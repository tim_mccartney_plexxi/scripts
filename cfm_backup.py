



import os
import sys
import requests



from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

#CFM_HOST_IP = '192.168.104.253'
CFM_HOST_IP = 'pinkman-connect.lab.plexxi.com'


#Create a class and the only variable we pass in is the host address

class Session:
    def __init__(self, host):
        self.host = host
        #self.token = None

# In this function call we get the token for the Connect server, nothing is passed in.
    def authenticate(self):
        headers = {
            'X-Auth-Username': 'admin',
            'X-Auth-Password': 'plexxi',
            'Content-Type': 'application/json'
        }
        r = requests.post('https://{}/api/v1/auth/token'.format(self.host), headers=headers, verify=False, timeout = 5.0)
        r.raise_for_status()
        res = r.json()
        #Not sure as to why we don't have "return here"
        self.token = res.get('result')

# This is the request to send using a method (GET, PUT etc) and a path eg. "nonCFM_session.send_request('GET', 'switches')"

    def send_request(self, method, path, json=None, params=None):
        headers = {
            'Authorization': self.token,
            'accept': 'application/json; version=1.0'
        }

        #Path to the end point
        url = 'https://{}/api/v1/{}'.format(self.host, path)        

        r = requests.request(method, url, json=json, headers=headers, verify=False)
        return r.json()

def scp_to_server():
    """ Securely copy the file to the server. """
    ssh_client = paramiko.SSHClient()
    ssh_client.load_system_host_keys()
    ssh_client.connect(CFM_HOST_IP, username="admin", password="plexxi")

    with scpclient.closing(scpclient.Write(ssh_client.get_transport(), "~/")) as scp:
        scp.send_file("LOCAL_FILENAME", remote_filename="REMOTE_FILENAME")
        
def db_backup():
    #Put the session for host and authenticate here so you dont need to repeat for every function call
    CFM_session = Session(CFM_HOST_IP)
    cfm_auth = CFM_session.authenticate()
    #See what backups are already on CFM, count, version name etc
    backup_response = CFM_session.send_request('GET', 'backups')
    #print backup_response
    try:
    #Ready to POST to create a new back up
        post_body = {}
        create_backup = CFM_session.send_request('POST', 'backups',json=post_body)
       #Print out the response to get the string containing the filename, which can be used later
        #print create_backup
        #TODO - Verify that backup has been created
        #TODO - Get filename from the json result
        create_backup_info = create_backup.get('result')
        backup_filename = create_backup_info.get('name')
        backup_checksum = create_backup_info.get('checksum')
        backup_version = create_backup_info.get('image_version')
        backup_url = create_backup_info.get('url')
    except AttributeError as error:
        sys.exit("Backup failed !!!!!!!  - You may have reached the limit of 20 ")
       

    print ( "\n\n\n\nNewly created backup file: {}. With checksum {}. Created with software version {}".format(backup_filename, backup_checksum, backup_version))
    backup_folder_list = backup_url.split('/')
    backup_folder = backup_folder_list[5]
    print ("Full path is /var/backups/{}/{} on {}").format(backup_folder, backup_filename, str(CFM_HOST_IP))

    return backup_filename,backup_folder







