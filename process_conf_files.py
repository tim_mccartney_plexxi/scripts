#!/usr/bin/python 

##########################################################################################
# Copyright 2017 Plexxi Inc. and its licensors.  All rights reserved.                    #
# Use and duplication of this software is subject to a separate license                  #
# agreement between the user and Plexxi or its licensor.                                 #
##########################################################################################


import os,sys,argparse,subprocess
import logging,cacheUTIL
logging.basicConfig(filename="/var/log/migration.log",level=logging.DEBUG)
logging.basicConfig(level=logging.WARNING, format="%(asctime)s - %(levelname)s - %(message)s")
logging.debug("DEBUG Logging activated.")

import switch_conf #class definition for switch configuration data



#############
# process_mlag_conf
#############


def process_mlag_conf(mlag_conf_filename):

    split_line = []
    port_list = []
    result_map = {}
    mlag_list = []
    shared_lag_name = ""
    local_lag_name = ""
    
    if (verbose): print("Processing %s." % (mlag_conf_filename))
    mlag_conf_file = open(mlag_conf_filename, "r")
    for line in mlag_conf_file:
        split_line = line.split()
        #print(split_line)
        # skip over first two table lines        
        if (len(split_line) > 0 and (split_line[0] != "")):
            # skip description lines, exit if we find the end "Plexxi"
            if (len(split_line) > 1):
                if ((split_line[1] == "Speed") or (split_line[1] == "-----")):
                    continue
                elif (line.find("Plexxi Switch") > -1 ):
                    break # signifies end of valid data

            if (split_line[0].find("lag") >= 0):

                # So if we're here we found a new lag name, accumulate what was found under last name then purge
                if (len(port_list) > 0):
                    result_map["shared_lag_name"] = shared_lag_name
                    result_map["local_lag_name"] = local_lag_name
                    result_map["port_list"] = port_list
                    mlag_list.append(result_map)            
                port_list = []
                result_map = {}
                    
                local_lag_name = split_line[0]
                if (verbose): sys.stdout.write("lag found %s\n" % local_lag_name)           
                #if (local_lag_name.find("*") >= 0):
                shared_lag_name = split_line[4]
                #sys.stdout.write("Mlag found %s" % shared_lag_name)
                for interface_i in xrange(5,len(split_line)):  # accumulate interfaces from line which does include other columns
                    interface_name = split_line[interface_i]
                    start_cut_pos = interface_name.find("(")
                    if (start_cut_pos >= 0):
                        interface_name = interface_name[0:start_cut_pos]
                    interface_name = interface_name.strip(",")
                    port_list.append(interface_name)
            elif (split_line[0].find("xp") >= 0 ):  # accumulates interface lines without other columns
                for interface_i in xrange(0,len(split_line)):
                    interface_name = split_line[interface_i]
                    start_cut_pos = interface_name.find("(")
                    if (start_cut_pos >= 0):
                        interface_name = interface_name[0:start_cut_pos]
                    interface_name = interface_name.strip(",")
                    port_list.append(interface_name)

    # to accumulate last mlag if we drop out of loop (likely)
                    
    if (len(port_list) > 0):
        result_map["shared_lag_name"] = shared_lag_name
        result_map["local_lag_name"] = local_lag_name
        result_map["port_list"] = port_list
        mlag_list.append(result_map)            
        port_list = [] #probably unneccessary
        result_map = {} #probably unneccessary
        
    mlag_conf_file.close()
    

    return mlag_list

#############
# process_mlag_conf END
#############



#############
# process_vlan_conf
#############


def process_vlan_conf(vlan_conf_filename):

    vlan = 0 # invalid I know.
    nativism = False # simply a guess.
    split_line = []
    port_list = []
    result_map = {}
    key_value = ""
    
    if (verbose): print("Processing %s." % (vlan_conf_filename))
    vlan_conf_file = open(vlan_conf_filename, "r")
    for line in vlan_conf_file:
        # We either have a new VLAN or this VLAN also has Native's on it
        if ((line.find("Native") > 0) or (line.find("Tagged") > 0)):
            if (line.find("Native") > 0): nativism = True
            if (line.find("Tagged") > 0): nativism = False
            if (not line[0].isspace()):
                split_line = line.split()
                vlan = int(split_line[0])
                del split_line[0] #removes VLAN which we already acquired
                del split_line[0] #removes tagged/native label
            else:
                split_line = line.split()
                del split_line[0] #just removes tagged/native label
        elif (line.find("Plexxi Switch") > -1 ):
            break # signifies end of valid data
        else:
            # vlan and native/tagged haven't changed so just take line as VLANs
            split_line = line.split()

            
        if (vlan != 0):
            if nativism:
                key_value = str(vlan) + "::Native"
            else:
                key_value = str(vlan) + "::Tagged"

        # find out if element is populated already and if not seed it, for all non-empty key_values
        if (key_value != ""):
            try:
                bobs_your_uncle = result_map[key_value]
            except KeyError:
                result_map[key_value] = []
                
        if (key_value != ""):
            port_list = []
            for item in split_line:
                #print("item %s" % item)

                start_cut_pos = item.find("(")
                if (start_cut_pos >= 0):
                    item = item[0:start_cut_pos]
                result_map[key_value].append(item.strip(","))
                #print("result_map %s" % result_map)
        
                start_cut_pos = 0
                
    vlan_conf_file.close()
    # dumps intermediary structure oriented(keyed on) to vlans, rather than ports/interfaces
    #dumpvlanmap(result_map)
    
    # So results map is all based on vlan and nativism because "show vlan" is setup that way,
    # but CFM is port oriented so we need to flip this dict so its port with lists of applicable vlans

    print("inside process vlan: %s" % result_map)
    
    new_port_map = {}
    for vlan_set in result_map:
        vlan_info = vlan_set.split("::")
        vlan_num = vlan_info[0]
        vlan_nativism = vlan_info[1]
        ports = result_map[vlan_set]
        for port in ports:
            if (vlan_nativism == "Native"):
                try:
                    new_port_map[port]["native"] = int(vlan_num)
                except KeyError:
                    new_port_map[port] = {}
                    new_port_map[port]["native"] = int(vlan_num)
            elif (vlan_nativism == "Tagged"):
                try:
                    new_port_map[port]["vlans"].append(int(vlan_num))
                except KeyError:
                    try:
                        new_port_map[port]["vlans"] = []
                    except KeyError:
                        new_port_map[port] = {}
                        new_port_map[port]["vlans"] = []
                    try:
                        new_port_map[port]["vlans"].append(int(vlan_num))
                    except ValueError:
                        print("error converting vlan, expecting int but got \"%s\"" % vlan_num)

    if (verbose): dumpnewportmap(new_port_map)
    
    return new_port_map

#############
# process_vlan_conf END
#############

#####
# dumpvlanmap
#####

def dumpvlanmap(vlan_map):
    for vlan in vlan_map:
        vlan_info = vlan.split("::")
        vlan_num = vlan_info[0]
        vlan_nativism = vlan_info[1]
        ports = vlan_map[vlan]
        print("VLAN # %s which is %s has these ports: %s" % (vlan_num,vlan_nativism,ports))




#####
# dumpvlanmap END
#####


#####
# dumpnewportmap
#####

def dumpnewportmap(new_port_map):

    for key_val in new_port_map:
        print("Interface %s has %s" % (key_val,new_port_map[key_val]))


#####
# dumpnewportmap END
#####



#############
# process_hostname
#############


def process_hostname(hostname_filename):

    if (verbose): print("Processing %s." % (hostname_filename))
    hostname_file = open(hostname_filename, "r")
    for line in hostname_file:
        if (line != ""):
            line = line.rstrip()
            hostname_file.close()
            print("Host is %s" % line)
            return line
    
    return ""

#############
# process_hostname END
#############

#############
# process_switchtype
#############


def process_switchtype(switchtype_filename):

    if (verbose): print("Processing %s." % (switchtype_filename))
    switchtype_file = open(switchtype_filename, "r")
    for line in switchtype_file:
        if (line != ""):
            line = line.rstrip()
            switchtype_file.close()
            print("Switch Type is %s" % line)
            return line
    
    return ""

#############
# process_switchtype END
#############


#############
# process_macaddr
#############


def process_macaddr(macaddr_filename):

    if (verbose): print("Processing %s." % (macaddr_filename))
    macaddr_file = open(macaddr_filename, "r")
    for line in macaddr_file:
        if (line != ""):
            line = line.rstrip()
            macaddr_file.close()
            print("MAC ADDR is %s" % line)
            return line
    
    return ""

#############
# process_macaddr END
#############


#############
# process_qsfp
#############


def process_qsfp(qsfp_filename):

    qsfp_map = {}
    if (verbose): print("Processing %s." % (qsfp_filename))
    qsfp_file = open(qsfp_filename, "r")
    for line in qsfp_file:
        if (("port" in line) and ("mode" in line)):
            chunk = line[line.find("port"):line.find("mode")]
            chunk = chunk.replace("port","")
            chunk = chunk.replace(".. ","..") # account for dumb formatting
            chunk = chunk.split()            
            range = chunk[0].split("..")

            # we are probably pre-5.0 and there is a range for the qsfp mode
            tmp_mode = chunk[1].split("x")
            new_chunk = [range, tmp_mode[0]]
            counter = int(range[0])
            if (len(range) < 2):
                print("Might not be pre-5.0 we are processing, exit.")
                exit(0)
            while (counter <= int(range[1])):
                qsfp_map[counter] = int(tmp_mode[0])
                counter = counter + 1
                    
    if (verbose): print(qsfp_map)
    
    return qsfp_map

#############
# process_qsfp END
#############

#############
# process_speed
#############


def process_speed(speed_filename):

    speed_map = {}
    if (verbose): print("Processing %s." % (speed_filename))
    speed_file = open(speed_filename, "r")
    for line in speed_file:
        if ("xp" in line):
            split_line = line.split()
            split_line_len = len(split_line)
            speed = split_line[split_line_len - 2] # count from back to avoid variable user label mis-parsing.
            interface = split_line[0]
            speed_map[interface] = speed
            
    if (verbose): print(speed_map)
    
    return speed_map

#############
# process_speed END
#############

#############
# process_adminstate
#############


def process_adminstate(adminstate_filename):

    adminstate_map = {}
    if (verbose): print("Processing %s." % (adminstate_filename))
    adminstate_file = open(adminstate_filename, "r")
    for line in adminstate_file:
        if ("xp" in line):
            split_line = line.split()
            split_line_len = len(split_line)
            adminstate = split_line[split_line_len - 5] # count from back to avoid variable user label mis-parsing.
            interface = split_line[0]
            if (adminstate == "up"):
                adminstate_map[interface] = "enabled"
            elif (adminstate == "down"):
                adminstate_map[interface] = "disabled"
                
    if (verbose): print(adminstate_map)
    
    return adminstate_map

#############
# process_adminstate END
#############



#############
# unpack_set 
#############

def unpack_set(targetFile):

    file_list = []
    print("Untar %s." % (targetFile))

    if (targetFile != ""):
        # make a tmp dir, extract to it
        commandX = "mkdir migrate_tmp; sudo /bin/tar -xvf " + targetFile + " -C ./migrate_tmp;"
    else:
        return ""      #This leaves little choice but to fail, due to lack of pyschic feature. 
    logging.debug("CommandX \"%s\"" % commandX)
    try: 
        raw_resultX = subprocess.check_output(commandX,stderr=subprocess.STDOUT, shell=True)
        tmp_file_list = os.listdir("./migrate_tmp")
        file_list = []
        for a_file in tmp_file_list:
            if ("tar" in a_file):
                file_list.append(a_file)
            
        logging.debug("*****")
        logging.debug(raw_resultX)
        logging.debug("*****")
    except subprocess.CalledProcessError:
        print("Error encountered untar: command \"%s\" --error-- \"%s\"" % (commandX,subprocess.STDOUT))
        return "" # reflects failure to apply, not sure if we want to bail out here.            

    
    return file_list

#############
# unpack END
#############

def unpack(targetFile):

    file_list = ""
    print("Untar %s." % (targetFile))

    if (targetFile != ""):
        # make a tmp dir, extract to it
        commandX = "cd migrate_tmp; sudo /bin/tar -xvf " + targetFile + "; cd ../"
    else:
        return ""      #This leaves little choice but to fail, due to lack of pyschic feature. 
    logging.debug("CommandX \"%s\"" % commandX)
    try: 
        raw_resultX = subprocess.check_output(commandX,stderr=subprocess.STDOUT, shell=True)
        file_list = os.listdir("./migrate_tmp")
        logging.debug("*****")
        logging.debug(raw_resultX)
        logging.debug("*****")
    except subprocess.CalledProcessError:
        print("Error encountered untar: command \"%s\" --error-- \"%s\"" % (commandX,subprocess.STDOUT))
        return "" # reflects failure to apply, not sure if we want to bail out here.            

    
    return file_list

#############
# unpack END
#############


#parse the command line args

# while defaults are provided in argparse, if there is no arg, no default seems to be applied so define here.


parser = argparse.ArgumentParser(description='Process Configuration Files 4.1 CLI -> intermediate python')
parser.add_argument('-v','--verbose', action="store_true", dest="verbose", default=False, help='Verbose mode outputs more to screen. Default is simple view.')
parser.add_argument('-c','--configure', dest='theChosenOnes', default="TBD", help='Too early in the morning for specifics.')
parser.add_argument('-t','--target', dest = 'theTargets', help='A switch IP address to go talk to and gain information about fabric.')
parser.add_argument('-f','--filetarget', dest = 'thefileTarget', default="", help='A file to process.')
parser.add_argument('-o','--override', dest='theOverrides', default="TBD", help='Too early in the morning for specifics.')
args = parser.parse_args()

thefileTarget = args.thefileTarget
verbose = args.verbose
files_to_process = unpack_set(thefileTarget)

print("Will now process these files %s" % files_to_process)

switch_configurations = []

for a_file in files_to_process:
    if (verbose): print("*********************")
    unpacked_files = unpack(a_file)
    if (verbose): print("unpacked %s" % unpacked_files)

    new_conf = switch_conf.switch_conf()
    this_host = ""
    this_vlan_map = {} # an "xp" interface name map of vlan assignments (pre-4.1 format)
    new_vlan_map = {} # an "xp" interface name map of vlan assignments (pre-4.1 format)
    this_mlag_list = [] # Data for an MLAG list [] of these {"local_lag_name":"", "shared_lag_name":"", "port_list":[]}
    this_qsfp_map = {} # used to map to new ids
    this_speed_map = {} # used to map to qsfp modes, and ultimately set speeds.
    new_speed_map = {} # used to map to qsfp modes, and ultimately set speeds.
    this_adminstate_map = {} # used to map to qsfp modes, and ultimately set adminstates.
    this_old2new = {}
    new_vlan_map = {} # a "display-id" interface name map of vlan assignments (post 5.0 format)

    for b_file in unpacked_files:
        if (verbose): print("-----------------------------------")
        b_file = "./migrate_tmp/" + b_file        
        
        if ("migrate_hostname" in b_file):            
            this_host = process_hostname(b_file)
            new_conf.setHOSTNAME(this_host)
        if ("migrate_switch_type" in b_file):            
            this_host = process_switchtype(b_file)
            new_conf.setSWITCHTYPE(this_host)
        if ("migrate_mac_base" in b_file):            
            this_host = process_macaddr(b_file)
            new_conf.setMACADDR(this_host)
        if ("migrate_mlag_conf" in b_file):            
            this_mlag_list = process_mlag_conf(b_file)
            print("this_mlag_list %s" % this_mlag_list)
            new_conf.setMLAGLIST(this_mlag_list)
        if ("migrate_vlan_conf" in b_file):            
            this_vlan_map = process_vlan_conf(b_file)
            print("this_vlan_map %s" % this_vlan_map)
            new_conf.setOLDVLANMAP(this_vlan_map)
        if ("migrate_qsfp" in b_file):           
            this_qsfp_map = process_qsfp(b_file)
            new_conf.setQSFPMAP(this_qsfp_map)
        if ("migrate_interface_conf" in b_file):           
            this_speed_map = process_speed(b_file)
            new_conf.setOLDSPEEDMAP(this_speed_map)
        if ("migrate_interface_conf" in b_file):           
            this_adminstate_map = process_adminstate(b_file)
            new_conf.setOLDADMINSTATEMAP(this_adminstate_map)


        if ((len(this_qsfp_map) > 0) and (len(this_speed_map) > 0) and (len(new_speed_map) == 0)):
            if (verbose): print("process new port ids here.")

            for interface in this_speed_map:
                simp_int = interface.replace("xp","")
                simp_int = int(simp_int)
                final_int = ""

                if (new_conf.getSWITCHTYPE() == "PX-S3EQ"):
                    # 3EQ
                    try:
                        mode = this_qsfp_map[simp_int]
                        if (mode == 1):
                            final_int = str(((simp_int - 1) / 4) + 1) # 1X has no "."-dot subtext like sfp
                        if (mode == 2):
                            postfix = ((simp_int - 1) % 2) + 1
                            final_int = str(((simp_int - 1)/ 4) + 1) + "." + str(postfix)  # 2X add .1 .2, logic to come.                       
                        if (mode == 4):
                            postfix = ((simp_int -1) % 4) + 1
                            final_int = str(((simp_int - 1)/ 4) + 1) + "." + str(postfix)  # 4X add .1 .2 .3 or .4, logic to come.
                    except KeyError:
                        if (str(simp_int) == "129"):
                            final_int = "33" # due to SFP after 32 QSFPs in 3eq
                        else:
                            # more than likely sfp's in a line, i.e. 2e
                            final_int = str(simp_int) # no "."-dot subtext for sfp, no qsfp mode implis sfp
                    # END 3EQ

                if (new_conf.getSWITCHTYPE() == "PX-S2E"):
                    # 2E
                    try:
                        mode = this_qsfp_map[simp_int]
                        simp_int = simp_int + 144 # to compensate for math in system with 48 sfps vs 32 qsfps (48 * 4 - 48, since they were counted once)
                        if (mode == 1):
                            final_int = str(((simp_int - 1) / 4) + 1) # 1X has no "."-dot subtext like sfp
                        if (mode == 2):
                            postfix = ((simp_int - 1) % 2) + 1
                            final_int = str(((simp_int - 1)/ 4) + 1) + "." + str(postfix)  # 2X add .1 .2, logic to come.                       
                        if (mode == 4):
                            postfix = ((simp_int -1) % 4) + 1
                            final_int = str(((simp_int - 1)/ 4) + 1) + "." + str(postfix)  # 4X add .1 .2 .3 or .4, logic to come. 
                    except KeyError:
                        # more than likely sfp's in a line, i.e. 2e
                        final_int = str(simp_int) # no "."-dot subtext for sfp, no qsfp mode implis sfp
                    # END 2E


                        
                this_old2new[interface] = final_int
                this_old2new[final_int] = interface
                
            new_conf.setOLD2NEW(this_old2new)
        
            
        if (verbose): print("-----------------------------------")
    if (verbose): print("*********************")
    # this point in the loop has processed everything for a host, append that to the list we'll hang on to.
    switch_configurations.append(new_conf)

# Dump the output for proofing, if in verbose mode.
for switch in switch_configurations:
    print("*************************  I have switch %s with base MAC %s *************************  " % (switch.getHOSTNAME(),switch.getMACADDR()))
    if (verbose):
        # tmp_old2new = switch.getOLD2NEW()
        # for element in sorted(tmp_old2new):
        #     print("======> Old2New %s => %s" % (element,tmp_old2new[element]))
        print("======> Old2New %s" % switch.getOLD2NEW())
        print("======> Oldvlanmap %s" % switch.getOLDVLANMAP())
    switch.remap4newint()
    print("\n======> qsfpmap")
    for item in switch.getQSFPMAP(): sys.stdout.write("%s:%s " % (item,switch.getQSFPMAP()[item])) 
    print("\n======> vlanmap ")
    for item in switch.getVLANMAP(): sys.stdout.write("%s:%s " % (item,switch.getVLANMAP()[item]))
    print("\n======> vlanmlagmap ")
    for item in switch.getMLAGVLANMAP(): sys.stdout.write("%s:%s " % (item,switch.getMLAGVLANMAP()[item]))
    print("\n======> mlaglist ")
    sys.stdout.write("%s" % (switch.getMLAGLIST())) 
    print("\n======> speedmap ")
    speeditemlist = []
    for item in switch.getSPEEDMAP(): speeditemlist.append(item)
    speeditemlist.sort()
    for item in speeditemlist: sys.stdout.write("%s:%s " % (item,switch.getSPEEDMAP()[item]))
    print("\n======> adminstatemap ")
    for item in switch.getADMINSTATEMAP(): sys.stdout.write("%s:%s " % (item,switch.getADMINSTATEMAP()[item])) 
    print("\n")
    
# save all that
cacheUTIL.data2file(switch_configurations)

    
exit(0)
