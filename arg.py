

#Look at this webpage also https://pymotw.com/2/argparse/

import argparse
parser = argparse.ArgumentParser()

parser.add_argument("--verbosity", "-v", help="increase output verbosity",action="store_true", dest="destverb")
parser.add_argument("--testing","-t", help="testing has help", action="store_true", dest="desttest")

args = parser.parse_args()

if args.destverb:
    print "Verbosity turned on"

if args.desttest:
    print "Testing turned on"

