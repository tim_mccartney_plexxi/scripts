import requests
import sys
import time
from pprint import pprint
from requests.packages.urllib3.exceptions import InsecureRequestWarning
from typing import List, Any

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

#Set the Connect IP address
nonCFM_HOST_IP = '172.18.20.14' # 3.1 Connect - NOT Control

class Session:
    def __init__(self, host):
        self.host = host
        #self.token = None

    def authenticate(self):
        """
        In this function call we get the token for the Connect server, nothing is passed in
        """
        headers = {
            'X-Auth-Username': 'admin',
            'X-Auth-Password': 'plexxi',
            'Content-Type': 'application/json'
        }
        r = requests.post('https://{}/api/v1/auth/token'.format(self.host), headers=headers, verify=False, timeout = 5.0)
        r.raise_for_status()
        res = r.json()
        #Not sure as to why we don't have "return here"
        self.token = res.get('result')

    def send_request(self, method, path, json=None, params=None):
        """
        This is the request to send using a method (GET, PUT etc)
        """
        headers = {
            'Authorization': self.token,
            'accept': 'application/json; version=1.0'
        }
        url = 'https://{}/api/v1/{}'.format(self.host, path)  #Path to the end point
        r = requests.request(method, url, json=json, headers=headers, verify=False)
        return r.json()

def get_switch_macs(response):
    """
    Get the switch MACs, and return a list of MACs
    """

    switch_macs = []
    for switch in response['result']:
        mac = switch.get('mac_address')
        switch_macs.append(mac)
    #return the variable switch_macs out from the function
    return switch_macs

# 

def get_fabric(response):
    """
    Get the fabric information- is whole, description, name, uuid, count
    return a string of fabric name and fabric description
    """

    fab_info = []
    for fabric in response['result']:
        fab_desc = fabric.get('description')
        fab_uuid = fabric.get('uuid')
        fab_name = fabric.get('name')
        

    return fab_name, fab_desc

def get_switch_name(response):
    """
    Get the switch name, and return a list of switch names
    """

    switch_name = []
    for switch in response['result']:
        name = switch.get('name')
        switch_name.append(name)
    return switch_name

def get_switch_model(response):
    """Get the switch model, and return a list of switch models"""

    switch_model = []
    for switch in response['result']:
        model = switch.get('model')
        switch_model.append(model)
    return switch_model


def get_lag_loop(response):

    """
    Get the lag information, return the lag description, name and speed
    """

    lag_info = []
    for lag in response['result']:
        lag_port_desc = lag.get('description')
        lag_port_name = lag.get('name')
        lag_speed = lag.get('speed')
        #print '{:>5} {:5<}'.format(lag_port_name, lag_port_desc)
    return lag_port_desc, lag_port_name, lag_speed


def get_switch_uuid(response):
    """
    Get the switch UUID, and return a list of them
    :rtype: object
    """

    switch_uuid = []
    for switch in response['result']:
        sw_uuid = switch.get('uuid')
        switch_uuid.append(sw_uuid)
    return switch_uuid


def get_switch_ip(response):
    """
    Get the switch IPs and return a list of them
    """

    switch_ip = []
    for switch in response['result']:
        sw_ip = switch.get('ip_address')
        switch_ip.append(sw_ip)
    return switch_ip


def get_ports_for_switch(sw_uuid):  
    """
    For each of the switch UUIDs, this function is being used to extract the port information and print it
    nicely to screen, it returns a list of port information for each switch
    """

    nonCFM_session = Session(nonCFM_HOST_IP)  # address or hostname of CFM
    nonCFM_session.authenticate()

    switch_list = []  # type: List[Any]
    outer_x = 0

    for uuid in sw_uuid:
        outer_x = outer_x + 1
        port_response = nonCFM_session.send_request('GET', 'ports?switches=' + uuid)
        switch_list.append(port_response)

    #now iterate through each one of the dicts and pull back a list of values from result, this should be one list (sw_info) per switch length of ports(72)
        mid_x = 0

    for sw_dict in switch_list:
        mid_x = mid_x + 1
        sw_info= sw_dict.get('result')
        print str(len(sw_info)) + " - Length of sw_info"

        print '\n'
        print '{:^18} {:^5} {:^10} {:^15} {:^15} {:^8} {:^6} {:^5} {:^15} {:^15}'.format('SW Name', 'Label', 'Admin', 'Desc',
                                                                                         'Name', 'Access', 'Native', 'Link',
                                                                                         'Current Speed', 'Vlan')
        print '\n'
        print "Mid loop " + str(mid_x)

        #iterate thru each element of the list (will get you data for each of the ports)
        inner_x = 0

        for port in sw_info:
            inner_x = inner_x + 1
            port_label = port.get('port_label')
            port_admin = port.get('admin_state')
            switch_name = port.get('switch_name')
            port_name = port.get('name')
            port_desc = port.get('description')
            port_type = port.get('access_port')
            port_native_vlan = port.get('native_vlan')
            port_link_state = port.get('link_state')
            port_speed = port.get('speed')
            port_current_speed = port_speed.get('current')
            port_vlan = port.get('vlans')
            print '{:^18} {:^5} {:^10} {:^15} {:^15} {:^8} {:^6} {:^5} {:^15} {:^15}'.format(switch_name, port_label, port_admin,
                                                                            port_desc, port_name, port_type, port_native_vlan,
                                                                            port_link_state, port_current_speed, port_vlan)
            # print "Inner loop= " + str(inner_x)
            # print switch_name
    # print "Outer loop= " + str(outer_x)
    return switch_list 
  

def get_connect_switch(sw_uuid):
    """
    For Connect: get a list of all the switches and data from call for access ports and return as a list
    containing a dict per switch, and also a list of port count per switch
    """

    nonCFM_session = Session(nonCFM_HOST_IP)  # address or hostname of CFM
    nonCFM_session.authenticate()
    
    nonCFM_switch_list_access = []
    nonCFM_port_count_list = []

    for switch in sw_uuid:
        port_response = nonCFM_session.send_request('GET', 'ports?switches=' + switch +'&type=access') #For ACCESS PORTS only
        nonCFM_switch_list_access.append(port_response)
        con_port_count = len(port_response.get('result'))
        nonCFM_port_count_list.append(con_port_count)
    print "This is the port count list for Connect\n"
   
    return nonCFM_switch_list_access, nonCFM_port_count_list


def strip_switch_data(switch_list): # paul k wrote this
    """
    Takes the list of all switches from get_connect switch, creates a reduced list of name, uuid, and label
    """
    switch_count = len(switch_list) 
    # "sw" is a dictionary of length 3 ie - count, result, time
    
    new_dict = {} # create a new dictionary 
    
    for sw in switch_list:
        sw_info= sw.get('result') # creates a list of items from a dict. Is a list of length 48 (number of access ports per switch)
        switch_name = sw_info[0].get('switch_name') # take the first element of the list of 48 and get the switch name from it
        new_dict[switch_name] = [] # Assign switchname  eg. mydict[key] = "value" . This creates a placeholder list, so rupert [], and then drop into the port loop to populate, then bob[] etc
        # the Key is the switchname, and the value is a list of dictionaries for each port and contains port label and uuid

       

        for port in sw_info: # 0-47 for each switch
            port_info = {
                'label': int(port.get('port_label')), #need to int, as CFM portlabel is unicode, and sort fails
                'uuid': port.get('uuid'),
                'name': port.get('name'),
                'description': port.get('description')
                }
            new_dict[switch_name].append(port_info)
            
        
    return new_dict


def order_dict_on_label(new_dict):
    """"
    This function takes the dictionary of switches and then sorts each by their port label and rolls back up into a dict
    """
    
    ordered_port_dict = {}

    for name in new_dict:
        ordered_port_dict[name] = sorted(new_dict[name], key=lambda k: k['label'])
        # pprint (ordered_port_dict)
    return ordered_port_dict


def get_fab_resp(): #TODO Tidy this up, there is no need for sw_response, lag_repsonse
    
    nonCFM_session = Session(nonCFM_HOST_IP) 
    nonCFM_session.authenticate()
    
    sw_response = nonCFM_session.send_request('GET', 'switches') 
    lag_response = nonCFM_session.send_request('GET', 'lags?port_type=access&type=provisioned' )
    fab_response = nonCFM_session.send_request('GET', 'fabrics')
    fab_count = fab_response['count']
    return fab_count


def main():
    #Put the session for host and authenticate here so you dont need to repeat for every function call
    nonCFM_session = Session(nonCFM_HOST_IP) 
    nonCFM_session.authenticate()
    sw_response = nonCFM_session.send_request('GET', 'switches') 
    lag_response = nonCFM_session.send_request('GET', 'lags?port_type=access&type=provisioned' )
    switch_count = sw_response['count']
    print "The total number of switches in the fabric is: " + str(switch_count)
    # print get_switch_macs(sw_response)
    # print get_switch_name(sw_response)
    # print get_switch_model(sw_response)
    print get_switch_uuid(sw_response)
    print get_switch_ip(sw_response)
    switch_uuid = get_switch_uuid(sw_response) #Pull back the switch uuid than can be used an passed to GET for Port UUID information
    get_ports_for_switch(switch_uuid)

  #TODO- Someting to look at below with two switch_list assignments###############
    # switch_list, nonCFM_port_count_list = get_connect_switch(switch_uuid)
    # strip_switch_data(switch_list)
    # new_dict = strip_switch_data(switch_list)
    # order_dict_on_label(new_dict)

    # connect_ports = order_dict_on_label(new_dict)
    # pprint(connect_ports)



if __name__ == '__main__':
    main()

