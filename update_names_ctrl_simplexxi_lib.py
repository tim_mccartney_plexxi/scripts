##########################################################################
#
# Copyright (c) 2016, Plexxi Inc. and its licensors.
#
# All rights reserved.
#
# Use and duplication of this software is subject to a separate license
# agreement between the user and Plexxi or its licensor.
#
##########################################################################

import os
import re
import sys


dirname = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(1, os.path.join(dirname, '../'))

from plexxi.core.api.binding  import *
from plexxi.core.api.util import *
from plexxi.core.api.util.LineSpeed import LineSpeed
from plexxi.core.api.uuid import PlexxiUuid
from plexxi.core.api.session import *

class Update_Names_Oninit():
    """Action responsible for enable ports commands."""

    def __init__(self, switchDataSet):
        print(switchDataSet)
        self.CTRLstring = switchDataSet[0]
        self.PORTSUPstring =  switchDataSet[1] 
        self.NTPstring = switchDataSet[2]
        self.DNSstring = switchDataSet[3]
        self.SNMPstring =  switchDataSet[4]
        self.TZstring = switchDataSet[5]
        self.HostADDRstring = switchDataSet[6]


    ################
    # validTargets, validates MAC addresses for targets
    # Also corrects for baseMAC hostMAC differences and interpets user input of mgmtMAC as baseMAC (essential for contact)
    # Later any mgmtMAC required for actual config will be acquired via contact with host via baseMAC
    ################

    def validTargets(self,rawTargets):

        Targets = [] 
        rawTargets = rawTargets.split(",")

        for Target in rawTargets:
            if (self.validMAC(Target)):
                Targets.append(Target.upper())   # we want everything easily comparable later, rather than every comp being saddled with ".upper()"
            else:
                print("Invalid MAC detected \"%s\" in target list. Abort!" % Target)
                sys.exit(1)

        #Any 7F or FF will be replaced with its lower order cousin (00 or 80 respectively) for contact as user MAY have entered baseMAC or mgmtMAC
        newTargets = []
        for Target in Targets:
            highMAC = Target[0:15]
            print("highMAC %s" % highMAC)
            lowMAC = Target[15:17]
            print("lowMAC %s" % lowMAC)
            if (lowMAC == "7F"):
                lowMAC = "00"
            if (lowMAC == "FF"):
                lowMAC = "80"
            print("lowMAC %s" % lowMAC)
            newMAC = str(highMAC) + str(lowMAC)
            print("newMAC %s" % newMAC)
            newTargets.append(newMAC)

        return newTargets # if we made it out of the loop everyone passes

    ################
    # validTargets END
    ################

    ################
    # validMAC, validates MAC addresses
    ################

    def validMAC(self,MAC):

        MAC = MAC.split(":")
        if (len(MAC) == 6):
            for x in xrange(6):
                #print(MAC[x])
                if not re.match("[0-9a-fA-F]",MAC[x]):
                    return "" # nothing means false
        else:
            return "" # nothing means false

        return "true" # if we made it out of the loop everyone passes

    ################
    # validMAC END
    ################

        
    def run(self):
        """Entry point for configuration of customer
        
        Arguments:
            message (str): The show command.
        """
        job = Job.create(name="Updating Switch Names to Hostnames",
                         description="MACs are hard to read, names are prefered, make it so!")
        job.begin()

        # gotta start somewhere
        index = 0

        while (index < len(self.HostADDRstring)):

            thisHOST = self.HostADDRstring[index]
            thisHOSTMAC = self.validTargets(thisHOST[0])
            thisHOSTMAC = str(thisHOSTMAC[0])  #validTargets returns list, so you must strip that out and string-ify
            thisHOSTNAME = thisHOST[1]
            targetID = MacAddress()            
            targetID.setMacAddress(thisHOSTMAC)
            switch = PlexxiSwitch.getByStationMacAddress(targetID)            
            switchName = switch.getName()
            print("seeking %s, TargetID is %s, Switch is %s, aka %s" % (thisHOSTMAC,targetID,switch,switchName))
            switch.setName(thisHOSTNAME)
            switchName = switch.getName()
            print("name after set %s" % (switchName))
            index = index + 1
            
        job.commit()





