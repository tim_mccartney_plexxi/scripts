

#Look at this webpage also https://pymotw.com/2/argparse/

import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-a', help="Print out timeline for c2, cx, plexxi, plexxi-L3, messages", dest='all', action='store')
parser.add_argument('-c2cx', help="Print out timeline for c2, cx", dest='c2cx', action='store')
parser.add_argument('-c2mp', help="Print out timeline for c2, messages, plexxiL3", dest='c2mp', action='store')
parser.add_argument('-l', help="List of timelines", dest='list', action='store')


args = parser.parse_args()



