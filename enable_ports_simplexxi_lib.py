##########################################################################
#
# Copyright (c) 2016, Plexxi Inc. and its licensors.
#
# All rights reserved.
#
# Use and duplication of this software is subject to a separate license
# agreement between the user and Plexxi or its licensor.
#
##########################################################################

import os
import re
import sys


dirname = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(1, os.path.join(dirname, '../'))

from plexxi.core.api.binding  import *
from plexxi.core.api.util import *
from plexxi.core.api.util.LineSpeed import LineSpeed
from plexxi.core.api.uuid import PlexxiUuid
from plexxi.core.api.session import *

class Enable_Ports_Oninit():
    """Action responsible for enable ports commands."""

    def __init__(self, portsupDataSet):
        print(portsupDataSet)
        self.portsupData = portsupDataSet[0]
        self.nativeVLANData = portsupDataSet[1]
        self.VLANData = portsupDataSet[2]

    def run(self):
        """Entry point for configuration of customer
        
        Arguments:
            message (str): The show command.
        """
        job = Job.create(name="Enabling Switch Access Ports",
                description="Enabling Switch Access Ports")
        job.begin()

        # gotta start somewhere, seed lastMAC so loop goes
        index = 0

        while (index < len(self.portsupData)):

            # re-init port_list as we'll be a new switch at this point.
            port_list = []
            port_speed_list = []
            rawMAC = ""
            
            # emulated do-while loop to accumulate port list and speed list for same switch 
            onSameSwitch = True
            while (onSameSwitch and (index < len(self.portsupData))):

                portData = self.portsupData[index].split(" ")
                rawMAC = portData[0]
                port_list.append(portData[1])
                port_speed_list.append(portData[2])

                if ((index + 1) < len(self.portsupData)):
                    portDataTmp = self.portsupData[(index + 1)].split(" ")
                    nextMAC = portDataTmp[0]                
                    onSameSwitch = (nextMAC == rawMAC)
                    
                # don't advance index if MAC different, leave alone for next round
                # don't need to replace lastMAC with new/rawMAC if same already
                if onSameSwitch:
                    index = index + 1
                if ((not onSameSwitch) or (index == len(self.portsupData))):                    
                    print("port_list: %s" % port_list)
            
            #Attach to first desired MAC target, harvest ALL its available ports
            targetID = MacAddress()
            targetID.setMacAddress(rawMAC)
            switch = PlexxiSwitch.getByStationMacAddress(targetID)
            switchName = switch.getName()
            print("seeking %s, TargetID is %s, Switch is %s, aka %s" % (portData[0],targetID,switch,switchName))
            inPorts = switch.getAllAccessSwitchFabricInPorts()

            #Run through list of all available ports (which come in random order) and compare hwid with port_list for this switch
            # if we find it, we also get the index as "found"'s value, which we don't need for enabling, but we need for finding speed (maybe vlan)
            for inPort in inPorts:
                found = -1
                try:
                    found = port_list.index(str(inPort.getHwId()))
                except ValueError:
                    found = -1

                if (found < 0):
                    print("port #" + str(inPort.getHwId()) + " not found in port_list")
                else:
                    print("port #" + str(inPort.getHwId()) + " FOUND in port_list")


                if (found >= 0):
                    if not inPort.isAdminStateEnabled():
                        print("Enabling port: " + str(inPort.getUserLabel()) + " Speed WOULD be: " + str(port_speed_list[found]) + " Native VLAN ID " + str(self.nativeVLANData) + " VLAN IDS " + str(self.VLANData))
                        inPort.configureAdminStateEnabled(True)
                        lagInPort = inPort.getLagInPort()
                        if (port_speed_list[found] == "10G"):
                            lagInPort.setConfiguredLineSpeed(LineSpeed.SPEED10GBPS)
                        if (port_speed_list[found] == "1G"):
                            lagInPort.setConfiguredLineSpeed(LineSpeed.SPEED1GBPS)
                        if (port_speed_list[found] == "25G"):
                            lagInPort.setConfiguredLineSpeed(LineSpeed.SPEED25GBPS)
                        LAG = lagInPort.getLinkAggregationGroup()
                        if ((self.nativeVLANData[0] == "y") or (self.nativeVLANData[0] == "Y")):
                            nativeVLAN = self.nativeVLANData.split(" ")
                            nativeVLANID = nativeVLAN[1]
                            print(nativeVLANID)
                            LAG.setNativeVlanId(nativeVLANID)
                        if (len(self.VLANData) > 0):
                            VLANDataINT = []
                            for VLANID in  self.VLANData:
                                VLANDataINT.append(int(VLANID))
                            print VLANDataINT
                            LAG.setVlanIds(set(VLANDataINT))
                            
            index = index + 1
            
        job.commit()





