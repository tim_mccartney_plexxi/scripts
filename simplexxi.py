#!/usr/bin/python 

##########################################################################################
# Copyright 2017 Plexxi Inc. and its licensors.  All rights reserved.                    #
# Use and duplication of this software is subject to a separate license                  #
# agreement between the user and Plexxi or its licensor.                                 #
##########################################################################################


import os,sys,socket,re,datetime,subprocess,readline,argparse,shutil,time,pickle

px_utils_imported = 1
try:
    import px_utils
except:
    print("!!!! Cannot find px_utils, maybe we aren't on a genuine Plexxi Switch???")
    px_utils_imported = 0
    sys.exit(1)  # which means you'll never see the "might not be root proceed with caution" message but leaving in place in case we revert behavior

    
# Check that we are root.

if (px_utils_imported == 1):
    px_utils.require_su()
else:
    print("px_utils not imported!! Normally if not sudo/root, you should quit, because nothing you do truly be completed as non-root. Proceed accordingly. If you are root, or have sudo'd you can ignore this message.")


    
import logging
logging.basicConfig(filename="/var/log/px-setup.log",level=logging.DEBUG)
#logging.basicConfig(level=logging.DEBUG, format="%(asctime)s - %(levelname)s - %(message)s")
logging.basicConfig(level=logging.WARNING, format="%(asctime)s - %(levelname)s - %(message)s")
logging.debug("DEBUG Logging activated.")

def anyK2C():
    try:
        raw_input("Press ANY KEY to continue.")
    except KeyboardInterrupt:
        emergency_exit_warning = "\n\n!!! -- CTRL-C ? -- !!!\n   Exit all the way?"
        if doThis(emergency_exit_warning):
            sys.exit(1)




# Border Macros

def topBORDER():
    print("__________________________________________________________") 

def bottomBORDER():
    print("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
    #print("__________________________________________________________") 

def topLongBORDER():
    print("____________________________________________________________________________________________________________________________") 

def bottomLongBORDER():
    print("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")


def printWelcome():

    topLongBORDER()
    print("======================================================\n===========  Plexxi Switch Setup ============\n======================================================\n\n")
    print("\tThis utility will configure your switches around the fabric in one series of actions with minimal input.")
    topLongBORDER()
    print("Hints:")
    print("\t1) Simplify repetitive data entry by using \"UP arrow key\" to recall previously entered answers.")
    print("\t2) If you make a mistake, continue on; you will have unlimited chances to modify your entries before anything gets applied.")
    print("\t3) If you do want to start over, use \"CTRL+C\" to exit immediately with no further action taken.")
    topLongBORDER()
    print("\n\n\n")


##################################
# lstz, which was external and is used elsewhere in the system, so this must be internalized to avoid conflict
# Copyright 2013 Plexxi Inc. and its licensors.  All rights reserved.
# Use and duplication of this software is subject to a separate license
# agreement between the user and Plexxi or its licensor.
#
# Lightly modified to function as module and use in externaly python script
#
#################################

master_list = []
unwanted = ["localtime", "posixrules"]

def gen_list (pfx, srcdir):

   if not srcdir:
      srcdir = "/usr/share/zoneinfo"
   
   """Recursively (infix) generate list of files in a tree."""
   if os.path.exists(srcdir):
      files = os.listdir(srcdir)
   else:
      return ""
   nextdirs = []
   global master_list
   global unwanted
   for file in files:
      if os.path.isdir(srcdir+"/"+file):         
         nextdirs.append(file)
      elif file not in unwanted:
         if pfx:
#            if "posix" in srcdir:
            master_list.append(pfx+"/"+file)
         else:
#            if "posix" in srcdir:
            master_list.append(file)
   for d in nextdirs:
      if pfx:
         gen_list(pfx+"/"+d, srcdir+"/"+d)
      else:
         gen_list(d, srcdir+"/"+d)

def gen_list_all(PFX,SRCDIR):

   gen_list(PFX,SRCDIR)
   
   master_list.sort()

   logging.debug(master_list)
   
   return master_list

# example of calling this function
#gen_list_all("", tzdir)


##################################
# END lstz, which was external and is used elsewhere in the system, so this must be internalized to avoid conflict
#################################


#############
# doThis
#############

def doThis(whateverthisis):

    for i in xrange(100):
#        sys.stdout.write("%s (y/n)" % whateverthisis)
        prompt =  whateverthisis + " (y/n)"
        answer = raw_input(prompt)

        if ((answer == "y") or (answer == "Y")):
            return "true"
        else:
            if ((answer == "n") or (answer == "N")):
                return ""    #Because empty is "false"

    print("Sorry, I did not recognize that response.")
    return ""

#############
# doThis END
#############


################
# validAddr, validates IP address (1), will NOT handle comma seperated list
################

def validAddr(ip):

    pat = re.compile("^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$")
    #pat = re.compile("^[0-2][0-9][0-9]\.{3}[0-2][0-9][0-9]{3}$")
    
    inet = 4
    try:
        socket.inet_aton(ip)
        #print("good ipv4")
    except socket.error:
        try:
            inet = 6
            socket.inet_pton(socket.AF_INET6, ip)
        except socket.error:
            logging.debug("Bad address, non-numeric \"%s\"" % ip)
            return "" #I.e. bad
        #print("good ipv6")

    # apparently, strictly strings like "127.1" is valid according to inet_aton, I saw nah, so regex double check
    if ((inet == 4) and (pat.match(ip))):
        return "true" # if we made it out of the loop everyone passes
    else:
        if (inet == 6):
            return "true" # if we made it out of the loop everyone passes
        else:
            return ""

################
# validAddr END
################

################
# validAddrS, validates IP addresses (n), WILL handle comma seperated list
################

def validAddrS(ips):
    
    for ip in ips:
        if not validAddr(ip):
            return "" #I.e. bad
        
    return "true" # if we made it out of the loop everyone passes

################
# validAddrS END
################


################
# validMAC, validates MAC addresses
################

def validMAC(MAC):

    MAC = MAC.split(":")
    if (len(MAC) == 6):
        for x in xrange(6):
            #print(MAC[x])
            if not re.match("[0-9a-fA-F]",MAC[x]):
                return "" # nothing means false
    else:
        return "" # nothing means false
        
    return "true" # if we made it out of the loop everyone passes

################
# validMAC END
################


################
# validTargets, validates MAC addresses for targets
# Also corrects for baseMAC hostMAC differences and interpets user input of mgmtMAC as baseMAC (essential for contact)
# Later any mgmtMAC required for actual config will be acquired via contact with host via baseMAC
################

def validTargets(rawTargets):

    Targets = [] 
    rawTargets = rawTargets.split(",")

    for Target in rawTargets:
        if (validMAC(Target)):
            Targets.append(Target.upper())   # we want everything easily comparable later, rather than every comp being saddled with ".upper()"
        else:
            print("Invalid MAC detected \"%s\" in target list. Abort!" % Target)
            sys.exit(1)

    #Any 7F or FF will be replaced with its lower order cousin (00 or 80 respectively) for contact as user MAY have entered baseMAC or mgmtMAC
    newTargets = []
    for Target in Targets:
        highMAC = Target[0:15]
        print("highMAC %s" % highMAC)
        lowMAC = Target[15:17]
        print("lowMAC %s" % lowMAC)
        if (lowMAC == "7F"):
            lowMAC = "00"
        if (lowMAC == "FF"):
            lowMAC = "80"
        print("lowMAC %s" % lowMAC)
        newMAC = str(highMAC) + str(lowMAC)
        print("newMAC %s" % newMAC)
        newTargets.append(newMAC)
            
    return newTargets # if we made it out of the loop everyone passes

################
# validTargets END
################


################
# validFQDNs, validates FQDNs, as a comma delimited list
################

def validFQDNs(FQDNs):

    FQDNsS = FQDNs.split(",")
    for FQDN in FQDNsS:        
        if not validFQDN(FQDN):
            return "" # if anyone fails we all fail
    
    return "true" # if we made it out of the loop everyone passes

################
# validFQDN END
################


################
# validFQDN, validates FQDN
################

def validFQDN(FQDN):

    if not (re.match("(?=^.{4,253}$)(^((?!-)[a-zA-Z0-9-]{1,63}(?<!-)\.)+[a-zA-Z]{2,63}$)",FQDN)):
        return ""       
    return "true" # if we made it here, we're good

################
# validFQDN END
################

################
# validHOST, validates HOST
################

def validHOST(HOST):

    if not (re.match("[a-zA-Z0-9-]{1,63}$",HOST)):
        return ""       
    return "true" # if we made it here, we're good

################
# validHOST END
################


################
# uniqueENFORCE
# assuming a list of 3 element lists, ensure that element 1 (mac) is unique through entire list, same for 2 (hostname) & 3 (ip)
################

def uniqueENFORCE(list):

    for x in xrange(len(list)):
        for y in xrange(3) :  #  don't compare whole list because mask and gateway addr CAN be same, only MAC, host, IP are valid comps
            for X in xrange(len(list)):     
                if (list[x][y] == list[X][y]):
                    if (x != X): # can't self conflict
                        print("!!!You have a conflict!!!! \"%s\" conflicts with \"%s\"" % (list[x],list[X]))
                        print(" Your input will be purged for this section. Fix this, by reentering data, with unique addresses please. ")
                        nonanswer = raw_input("Enter any key to acknowledge you understand.")
                        return ""

    # if we made it here we didn't return on first fail, i.e. all good
    return "PASS " 

################
# uniqueENFORCE END
################

################
# ctrlCharStrip
################

def escape_ansi(line):
    ansi_escape = re.compile(r'(\x9B|\x1B\[)[0-?]*[ -\/]*[@-~]')
    return ansi_escape.sub('', line)

################
# ctrlCharStrip END
################


################
# collectPortMap
################

def collectPortMap(MAChost):

    portMAP = []
    
    commandY = "sudo /opt/plexxi/bin/plexxiRemoteCommand " + MAChost + " \'/bin/hostname; /opt/plexxi/bin/showLag;\'"                        
    logging.debug("CommandY \"%s\"" % commandY)
    try: 
        raw_resultY = subprocess.check_output(commandY,stderr=subprocess.STDOUT, shell=True)
        logging.debug("*****")
        logging.debug(raw_resultY)            
        logging.debug("*****")
        portList = raw_resultY.split("\n")
        
        for port in portList:
            if ((port.find("xp") >= 0) and (port.find("lag") >= 0) and (port.find("ring") < 0)  and (port.find("east") < 0)  and (port.find("west") < 0)):  # we want NO ring ports
                port = port.strip()
                port = port.split()
                portId = port[3]
                portId = portId.strip()
                portMAP.append(portId)
        
    except subprocess.CalledProcessError:
        print("Error encountered attempting to apply config hostname_%s_conf.pup: \"%s\"" % (MAC,subprocess.STDOUT))
        return "" # reflects failure to apply, not sure if we want to bail out here.            
                 
    return portMAP
    
################
# collectPortMap END
################


################
# collectPeerPortMap     !!!! we will do MATH and return values in "xp" +1 representation !!!!!
################

def collectPeerPortMap(MAChost):

    portMAP = []
    
    commandY = "sudo /opt/plexxi/bin/plexxiRemoteCommand " + MAChost + " \'/bin/hostname; /opt/plexxi/bin/showPeers;\'"                        
    logging.debug("CommandY \"%s\"" % commandY)
    try: 
        raw_resultY = subprocess.check_output(commandY,stderr=subprocess.STDOUT, shell=True)
        logging.debug("*****")
        logging.debug(raw_resultY)            
        logging.debug("*****")
        portList = raw_resultY.split("\n")
        
        for port in portList:
            pos1 = 0
            pos2 = 0
            if ((port.find("Local port swId") >= 0) and (port.find("(self)") >= 0)):
                pos1 = port.find("Local port swId ")
                pos1 = pos1 + len("Local port swId ")
                pos2 = port.find("<==")
                port = port[pos1:pos2]
                try:
                    portId = int(port)
                    portId = portId + 1    # !!!!!!  <----- We are ADDING 1 to make it like xp representation!!!!!!
                    portId = "xp" + str(portId)  # Now make a string and put xp on it, that ought to make it clear
                except:
                    print("error converting port to int in collectPeerPortMap")
                portMAP.append(portId)
        
    except subprocess.CalledProcessError:
        print("Error encountered attempting to apply config hostname_%s_conf.pup: \"%s\"" % (MAC,subprocess.STDOUT))
        return "" # reflects failure to apply, not sure if we want to bail out here.            
                 
    return portMAP
    
################
# collectPeerPortMap END
################

################
# collectXCVR
################

def collectXCVR(MAChost):

    xcvrMAP = []
    
    commandY = "sudo /opt/plexxi/bin/plexxiRemoteCommand " + MAChost + " \'/bin/hostname; /opt/plexxi/bin/cliShowXcvr;\'"                        
    logging.debug("CommandY \"%s\"" % commandY)
    try: 
        raw_resultY = subprocess.check_output(commandY,stderr=subprocess.STDOUT, shell=True)
        logging.debug("*****")
        logging.debug(raw_resultY)            
        logging.debug("*****")
        xcvrList = raw_resultY.split("\n")
        
        for xcvr in xcvrList:
            if ((xcvr.find("Transceiver#") >= 0) and (xcvr.find("Port") >= 0) and (xcvr.find("ring") < 0)  and (xcvr.find("East") < 0)  and (xcvr.find("West") < 0)):  # we want NO ring xcvrs
                xcvr = xcvr.strip()
                xcvr = xcvr.split()
                xcvrId = xcvr[0]
                xcvrId = xcvrId.replace("Transceiver#","")
                try:
                    xcvrId = int(xcvrId)
                    xcvrId = "xp" + str(xcvrId)
                except:
                    logging.debug("error converting xcvr %s" % xcvrId)
                
                xcvrMAP.append(xcvrId)
        
    except subprocess.CalledProcessError:
        print("Error encountered attempting to apply config hostname_%s_conf.pup: \"%s\"" % (MAC,subprocess.STDOUT))
        return "" # reflects failure to apply, not sure if we want to bail out here.            
                 
    return xcvrMAP
    
################
# collectXCVR END
################



#############
# confPORTSUP
#############

def confPORTSUP():

    portCONFstr = []
    speed = ""
    doNative = "n"
    vlanID = -69
    natVLANstring = doNative + " " + str(vlanID)
    vlanIDs = []
    
    # Consume PORTSUP config from user
    while (len(portCONFstr) < 3):
        # get speed
        while ((speed != "1G") and (speed != "10G") and (speed != "25G") and (speed != "auto")):
            print("You can statically select one speed for all ports, or use auto to try all speeds and get most ports up. (Fabric ports excluded automatically)")
            speed = raw_input("What speed for these ports (1G/10G/25G/auto)?: ")
            if ((speed != "1G") and (speed != "10G") and (speed != "25G") and (speed != "auto")):
                print("You entered: %s ... and thats not valid must be 1G,10G,25G or auto" % speed)
            else:
                print("You entered: %s" % speed)

        # Are we doing VLANs today?
        doVLAN = ""
        while ((doVLAN != "y") and (doVLAN != "Y") and (doVLAN != "n") and (doVLAN != "N")):
            doVLAN = raw_input("Do you want to configure a VLAN for these ports? (y/n): ")

        if ((doVLAN == "y") or (doVLAN == "Y")):            
            doNative = ""
            while ((doNative != "y") and (doNative != "Y") and (doNative != "n") and (doNative != "N")):
                doNative = raw_input("Do you want to configure a native VLAN for these ports? (y/n): ")

            if ((doNative == "y") or (doNative == "Y")):            
                vlanID = -69                            
                while ((vlanID < 1) or (vlanID > 4000)):
                    vlanID = raw_input("Enter NATIVE VLAN ID (1-4000): ")
                    try:
                        vlanID = int(vlanID)
                    except:
                        print("error with vlanID %s" % vlanID)
                        vlanID = -69

            natVLANstring = doNative + " " + str(vlanID)
            
            while (vlanID != ""):
                vlanID = -69                            
                while (((vlanID < 1) or (vlanID > 4000)) and (vlanID != "")):
                    vlanID = raw_input("Add tagged/non-native VLAN ID (1-4000) or just enter to end: ")
                    if (vlanID != ""):
                        try:
                            vlanID = int(vlanID)
                            if ((vlanID != "-69") and (vlanID >= 1) and (vlanID < 4001)):
                                vlanIDs.append(str(vlanID))                        
                        except:
                            print("error with vlanID %s" % vlanID)
                            vlanID = -69
                print("vlanIDs so far: %s" % vlanIDs)
                        
                        

        portCONFstr.append(speed)
        portCONFstr.append(natVLANstring)
        portCONFstr.append(vlanIDs)        
        #portCONFstr.append(doNative)

    return portCONFstr

#############
# confPORTSUP END
#############


#############
# discPORTSUP 
#############

def discPORTSUP(PORTSUPstring,targetList,OUTPUTDIR):

    # To print time estimate along the way, to break up the bordem.
    def print_time_remaining(num_ports):
        time_per_port = 1.123
        time_remaining = time_per_port * num_ports
        outstring = str(time_remaining)         
        return outstring

    
    PORTSUPmasterLIST = []

    # PORTSUP
    commandX = ""
    raw_resultX = ""
    raw_resultY = ""
    list25G = []
    list10G = []
    list1G = []
    listXG = []

    os.system("clear")
    portsupMESSAGE = "PORTSUP... discovery process will now proceed, it will take approximately " + str(print_time_remaining((len(targetList) * 64 * 3))) + " seconds to complete, please standby...\n" 
    sys.stdout.write(portsupMESSAGE)
    
    if (PORTSUPstring):

        # we are going to roll through all hosts in target list and accumulate all ports that will come up at configured (or in auto case any) speed
        # This will go into PORTSUPmasterLIST, then be appended with VLAN/NativeVLAN info, then written to portsup.dat data file for transport/execution on CTRL
        # transport/execution will be in apply data section, though likely outside other config application which is per host, this will only occur once to CTRL meritting its own function
        for target in targetList:

            MAChost = target.replace(":","")   # nix colons for ring command to specific member # we want this set unconditionally! 
            sys.stdout.write("modify and querying host ")
            sys.stdout.write(MAChost)
            sys.stdout.write("  ...")

            ####
            #   Because each switch in fabric MIGHT be different models and have a differing number of ports we must do some work here.
            #   Also each switch even if the same model might have a different configuration.
            #   So first we collect a portMAP (all ports on a system)
            #   Next we will use showPeers (a connected fabric being a pre-requisite for using px-setup/simplexxi.py;
            #       showPeers ports which are local will NOT be disabled/modified/enabled because it will cause issues using px-setup/simplexxi, its unnecessary and it won't help 

            # ALL ports filtering out anything with "ring" in description
            portMapALL = collectPortMap(MAChost)
            logging.debug("portMapALL (no ring/fabric): %s" % portMapALL)
            portMAP = []

            # Collect, parse, execute +1 math on peer ports
            peerPortMAP = collectPeerPortMap(MAChost)
            logging.debug("Peer port map %s" % peerPortMAP)

            # Collect actual inserted transceivers to greatly reduce ports necessary to attempt.
            XCVR = collectXCVR(MAChost)
            logging.debug("Peer port map %s" % XCVR)

            
            # Remove peer ports from all port map, so we don't hit them. An extra pre-caution against manipulating any fabric/ring ports
            for port in portMapALL:
                found = 0
                for searchport in peerPortMAP:
                    if (port.find(searchport) >= 0):
                        found = 1
                if (found == 0):
                    portMAP.append(port)
            logging.debug("port map %s" % portMAP)

                    
            #for peer in portMAP:
            #    print("Peer port %s" % peer)


            speed = PORTSUPstring[0]
            vlanNATIVE = PORTSUPstring[1]
            vlanIDs = PORTSUPstring[2]

            # !!! Set VLANS First !!!!

            """ 
            Now that what ports are known, we can set them.
            Set VLANs first because speed and state.
            Because setting vlans disables and resets to 10G. !!!
            """
            bulkClearVlanAll(MAChost,portMAP,vlanNATIVE) # Maybe we shouldn't clear, but maybe we should, perhaps later make optional.
            # Because we are clearing in all cases, we must establish/re-establish default native vlan 1 on all access ports.
            if (len(vlanIDs) == 0):            
                bulkSetVlanNativeAll(MAChost,portMAP,"y 1")
            else:
                bulkSetVlanNativeAll(MAChost,portMAP,vlanNATIVE)
                bulkSetVlanAll(MAChost,portMAP,vlanIDs)

                
            # we need to set ALL ports for intended speed, and then verify ALL ports, in order that they have some time to come up
            # waiting on each port is very time inefficient.        
            
            portSuccess = 0
            
            # Speed can be modified and take positive effect without disable/enable bounce.
            # So just bring everyone up
            setPortEnableAll(MAChost)

            time.sleep(5)

            # Only attempt ports enable/speed test on ports with xcvrs
            portMAP = XCVR
            
            # First whatever was asked for (test with 25G to test automatic)
            if (speed == "auto"):
                
                speed = "1G"

                
                portsupMESSAGE = "Approximately " + str(print_time_remaining((len(portMAP)*3))) + " seconds remaining for this host ..." 
                sys.stdout.write(portsupMESSAGE)

                print("\nAttempt to bring up ports to %s" % speed)

                if (len(portMAP) > 0):
                    setPortSpeedBulk(MAChost,portMAP,speed)

                time.sleep(5)

                states = verifyPortSpeedBulk(MAChost,port,speed)

                verified = ""
                newPortMAP = []
                list1G = []
                for port in portMAP:
                    for state in states:
                        if (state[0] == port): 
                            if (state[1] == "down"):                        
                                newPortMAP.append(port)
                                logging.debug("Port %s did not come up at this speed: %s. Add for retry." % (port,speed))
                            else:                        

                                logging.debug("port is %s at %s" % (port,speed))
                                list1G.append(port)                    

                speed = "10G"
                
                portsupMESSAGE = "Approximately " + str(print_time_remaining(len(newPortMAP))) + " seconds remaining for this host ..." 
                sys.stdout.write(portsupMESSAGE)

                print("\nAttempt to bring up ports to %s" % speed)
                if (len(newPortMAP) > 0):
                    setPortSpeedBulk(MAChost,newPortMAP,speed)

                    
                time.sleep(5)

                states = verifyPortSpeedBulk(MAChost,port,speed)
                
                verified = ""
                newERPortMAP = []
                list10G = []
                for port in newPortMAP:                        
                    for state in states:
                        if (state[0] == port):
                            if (state[1] == "down"):                        
                                newERPortMAP.append(port)
                                logging.debug("Port %s did not come up at this speed: %s. Add for retry." % (port,speed))
                            else:                        
                                logging.debug("port is %s at %s" % (port,speed))
                                list10G.append(port)                    

                portsupMESSAGE = "Approximately " + str(print_time_remaining(len(newERPortMAP))) + " seconds remaining for this host ..." 
                sys.stdout.write(portsupMESSAGE)

                                                        
                speed = "25G"
                print("\nAttempt to bring up ports to %s" % speed)
                if (len(newERPortMAP) > 0):
                    setPortSpeedBulk(MAChost,newERPortMAP,speed)
                
                time.sleep(5)

                states = verifyPortSpeedBulk(MAChost,port,speed)
                
                verified = ""
                newESTPortMAP = []
                list25G = []
                for port in newERPortMAP:                        
                    for state in states:
                        if (state[0] == port):
                            if (state[1] == "down"):                        
                                logging.debug("Port %s did not come up at this speed: %s. no more retry." % (port,speed))
                            else:                        
                                logging.debug("port is %s at %s" % (port,speed))
                                list25G.append(port)                    
                    
                sys.stdout.write("\n")

            else:      # this means specific port speed selected by user.

                print("\nAttempt to bring up ports to %s" % speed)                
                setPortSpeedBulk(MAChost,portMAP,speed)                    

                time.sleep(15)
                
                states = verifyPortSpeedBulk(MAChost,port,speed)
            
                verified = ""
                newPortMAP = []
                listXG = []
                for port in portMAP:                        
                    for state in states:
                        if (state[0] == port):
                            if (state[1] == "down"):                        
                                newPortMAP.append(port)                                
                                logging.debug("Port %s did not come up at this speed: %s. Add for retry." % (port,speed))
                            else:                        
                                logging.debug("port is %s at %s" % (port,speed))
                                listXG.append(port)                    

            for port in list1G:
                #pair = MAChost + " " + port + " 1G"
                pair = target + " " + port + " 1G"
                PORTSUPmasterLIST.append(pair)

            for port in list10G:
                #pair = MAChost + " " + port + " 10G"
                pair = target + " " + port + " 10G"
                PORTSUPmasterLIST.append(pair)

            for port in list25G:
                #pair = MAChost + " " + port + " 25G"
                pair = target + " " + port + " 25G"
                PORTSUPmasterLIST.append(pair)

            for port in listXG:
                #pair = MAChost + " " + port + " " + speed
                pair = target + " " + port + " " + speed
                PORTSUPmasterLIST.append(pair)

        # End of main macHostList loop

        # output complete data set for all switches discovered above
        print("\n============================================================")            
        logging.debug("ports up master list:")
        logging.debug(PORTSUPmasterLIST)
        logging.debug(": ports up master list END")

        # form final structure which includes VLAN info as well as all port info.
        PORTSUPmasterLISTinfo = [PORTSUPmasterLIST,vlanNATIVE,vlanIDs]

        tmpres = writePORTSUP(PORTSUPmasterLISTinfo,OUTPUTDIR)
        if (tmpres):
            sys.stdout.write("Generated portsup data cache.")
            logging.debug("Generated portsup data cache.")
            return PORTSUPmasterLISTinfo
        else:
            # errors generated by function itself, so no need to double print in else here.
            return ""
        print("============================================================")


#############
# discPORTSUP END
#############


#############
# confNTP 
#############

def confNTP():

    ips_formed = ""
    
    # Consume NTP config from user
    while (ips_formed == ""):    
        print("Enter NTP server address(es) example: \"10.11.12.13,99.88.77.66,ntp.yourdomain.com\"")
        ips = raw_input("==> ")
        print("You entered: %s" % ips)

        # Validate user input, or allow override in FQDN case
        while (ips.find(" ") > -1):
            ips = ips.replace(" ","")
        ips_formed = ips.split(",")
        allGood = validAddrS(ips_formed)

        if allGood == "":
            for fqdn in ips_formed:                
                if not validFQDN(fqdn) and not validAddr(fqdn):
                    print("%s could not be validated." % fqdn)
                    logging.debug("FQDNs could not be validated NTP section")
                    allGood = ""
                    ips_formed = ""
                else:
                    logging.debug("Possible FQDN detected")
        else:
            logging.debug("All good numerical IPs.")

    return ips_formed

#############
# confNTP END
#############

#############
# writeNTP 
#############

def writeNTP(ips_formed,OUTPUTDIR):

    # produce puppet output
    
    NTP_Config_String = "    class { '::ntp':\n"
    NTP_Config_String = NTP_Config_String +     "        servers        => ["

    # We insert servers here in a given format, single quote bound ips comma delimited inside square brackets

    num_of_ips = len(ips_formed)
    count = 1
    for ip in ips_formed:
        if count == num_of_ips:
            NTP_Config_String = NTP_Config_String + " '" + ip + "' "    #Because last one doesn't need/want a comma, just close single quote
        else:
            NTP_Config_String = NTP_Config_String + " '" + ip + "', "
        count = count + 1
             
    NTP_Config_String = NTP_Config_String +      "],\n"

    # puppet is erroring on "disable_auth" not supported until later versions of puppet
    # NTP_Config_String = NTP_Config_String +      "        disable_auth   => false,\n"
    NTP_Config_String = NTP_Config_String +      "        service_enable => true,\n"
    # No peers know at this point. OR perhaps later other switches are peers, need engineering direction
    # NTP_Config_String = NTP_Config_String +      "        peers          => [ '1.2.3.4' ],\n"
    NTP_Config_String = NTP_Config_String +      "    }\n"

    # push puppet output to file (for transport to switch, executed there)

    filename = OUTPUTDIR + "/ntp_conf.pup"
    ntp_conf = open(filename, 'w')    
    ntp_conf.write(NTP_Config_String)
    ntp_conf.close()

    return ips_formed
    
#############
# writeNTP END
#############

#############
# confCTRL 
#############

def confCTRL():

    allGood = ""
    
    # don't stop until satisfied input is good
    while (allGood == ""):
        # Consume CTRL config from user

        print("Enter Plexxi Control Server Address for example: \"10.20.30.40\"")
        ip = raw_input("==> ")
        print("You entered: %s" % ip)

        # Validate user input, or allow override in FQDN case

        allGood = validAddr(ip)

        if (allGood == ""):
            allGood = validFQDN(ip)
            logging.debug("Validate as FQDN \"%s\"" % ip)
            if (allGood == ""):
                print("FQDNs could not be validated CTRL section")
                logging.debug("FQDNs could not be validated CTRL section")
                allGood = ""
                ip = ""
            else:
                logging.debug("Possible FQDN detected")
        else:
            logging.debug("All good numerical IPs.")

    return ip

#############
# confCTRL END
#############

#############
# writeCTRL 
#############

def writeCTRL(ip,OUTPUTDIR):

    # At this point this is a no-op because it will be executed as a command on the switch shell.
    # applyData directly takes CTRL IP and uses command there is no preformatting etc or file to speak of.
    # However I have created this NULL func for consistency.

    return ip
    
#############
# writeCTRL END
#############


#############
# confDNS 
#############

def confDNS():

    # Consume DNS config from user
    domain = ""
    ips_formed = ""

    while (ips_formed == ""):
        print("Enter DNS Server Address(es) example: 10.11.12.13,99.88.77.66")
        ips = raw_input("==> ")
        print("You entered: %s" % ips)
        
        # Validate user input, or allow override in FQDN case
        while (ips.find(" ") > -1):
            ips = ips.replace(" ","")
        ips_formed = ips.split(",")
        allGood = validAddrS(ips_formed)

        if allGood == "":
            print("Input could not be validated as an IP Address nor a series of comma delimited IP Addresses. Please try again.")
            ips_formed = ""
        else:
            logging.debug("All good numerical IPs.")

    while (domain == ""):
        print("Enter (DNS) Domain Name for your Plexxi switches, single domain for \"domain\", comma delimited set for \"search path\"\n For example: profitdept.giantuscorp.com")
        domain = raw_input("==> ")
        print("You entered: %s" % domain)

        
        if (domain.find(",")):
            if not validFQDNs(domain):
                print("FQDNs could not be validated.")
                logging.debug("FQDNs could not be validated DNS section")
                domain = ""
            else:
                logging.debug("Possible FQDN(s) detected")
                
        else:        
            if not validFQDN(domain):
                print("FQDN could not be validated.")
                logging.debug("FQDN could not be validated DNS section")
                domain = ""
            else:
                logging.debug("Possible FQDN(s) detected")

    return [ips_formed,domain]

#############
# confDNS END
#############

#############
# writeDNS 
#############

def writeDNS(DNSinfo,OUTPUTDIR):

    ips_formed = DNSinfo[0]
    domain = DNSinfo[1]

    if (domain.find(",") < 0):
        # This must be "Search path" 
        DOMAIN = "1"
    else:
        # OK its domain
        DOMAIN = "0"
    # produce puppet output
    
    DNS_Config_String = "    class { 'resolv_conf':\n"
    DNS_Config_String = DNS_Config_String +     "        nameservers        => ["

    # We insert servers here in a given format, single quote bound ips comma delimited inside square brackets

    num_of_ips = len(ips_formed)
    count = 1
    for ip in ips_formed:
        if count == num_of_ips:
            DNS_Config_String = DNS_Config_String + " '" + ip + "' "    #Because last one doesn't need/want a comma, just close single quote
        else:
            DNS_Config_String = DNS_Config_String + " '" + ip + "', "
        count = count + 1
             
    DNS_Config_String = DNS_Config_String +      "],\n"

    if (DOMAIN == "1"):
        DNS_Config_String = DNS_Config_String +     "        domainname        => '"
        DNS_Config_String = DNS_Config_String + domain + "',\n"
    else:
        # i.e. implied search path
        DNS_Config_String = DNS_Config_String +     "        searchpath        => ["

        domains = domain.split(",")
        num_of_fqdns = len(domains)
        count = 1
        for fqdn in domains:
            if count == num_of_fqdns:
                DNS_Config_String = DNS_Config_String + " '" + fqdn + "' "    #Because last one doesn't need/want a comma, just close single quote
            else:
                DNS_Config_String = DNS_Config_String + " '" + fqdn + "', "
            count = count + 1
        
        DNS_Config_String = DNS_Config_String +  "],\n"
        

    DNS_Config_String = DNS_Config_String +      "    }\n"

    # push puppet output to file (for transport to switch, executed there)

    filename = OUTPUTDIR + "/dns_conf.pup"
    dns_conf = open(filename, 'w')    
    dns_conf.write(DNS_Config_String)
    dns_conf.close()

    return DNSinfo
    
#############
# writeDNS END
#############



#############
# confTIMEZONE
#############

def confTIMEZONE():

    tz_answer = -1
    tzdir = "/usr/share/zoneinfo/posix"    
    
    # Go get timezone possibilities
    
    TZs=gen_list_all("",tzdir)
    
    
    # Output timezones to user for selection.

    if not TZs:      # in case something fails down in gen_list_all (like directory can't be found)
        print("Something failed to get timeZones.")
        return ""
    
    print("Timezones...")
    while ((tz_answer < 0) or (tz_answer > len(TZs))):
        for x in xrange(len(TZs)):
            sys.stdout.write(str(x))
            sys.stdout.write(" => ")
            print(TZs[x])
            if (((x+1)%50) == 0):   # i.e. print 50 timezones and ask for choice or more.
                tz_answer = raw_input("\nChoose a timezone.\nAs text Example: UTC\nAs number Example: 601\n<enter> to display choices.\n==>> ")

            # give a go at exact text match just in case
            for y in xrange(len(TZs)):
                if (tz_answer == TZs[y]):
                    tz_answer = y

            # if exact text were found, it would be a valid int now, otherwise what user entered
            try:
                tz_answer=int(tz_answer)
                if ((tz_answer > 0) and (tz_answer < len(TZs))):
                    break
            except:
                tz_answer = -1
                print("Could not interpret timezone information entered.")

            
    return TZs[tz_answer]



#############
# confTIMEZONE END
#############


#############
# writeTZ
#############

# Lets put the valid date finally obtained into the file

def writeTZ(TZ_chosen,OUTPUTDIR):

    TZ_CONF_String = "class { 'timezone':\n"
    TZ_CONF_String = TZ_CONF_String + "\ttimezone => '"
    TZ_CONF_String = TZ_CONF_String + TZ_chosen
    TZ_CONF_String = TZ_CONF_String + "',\n}\n"

    
    # push puppet output to file (for transport to switch, executed there)

    filename = OUTPUTDIR + "/tz_conf.pup"
    tz_conf = open(filename, 'w')    
    tz_conf.write(TZ_CONF_String)
    tz_conf.close()



#############
# writeTZ END
#############

#############
# confSNMP 
#############

def confSNMP():

    # Consume SNMP config from user
    final_trap_hosts = []
    work2do = "y"
    SNMPstring = ""

    # Offer mostly default values
    ip="ALL"
    network="default"
    proto="udp"
    ro_commun="public"
    port=161    
    location = "unknown"
    
    print("Default recommended settings for SNMP are:\nAgent on \"ALL\" interfaces\nListen on \"ALL\" networks\nread-only community \"public\"\nprotocol \"udp\"\nport \"161\"\n")
    
    doEasy = "easyNeverGoodgoodNeverEasy"
    while ((doEasy != "y") and (doEasy != "Y") and (doEasy != "n") and (doEasy != "N")):
        doEasy = raw_input("Do you want use these defaults? (y/n): ")
        
    if ((doEasy == "y") or (doEasy == "Y")):
        print("Setting defaults where possible (Agent on \"ALL\" interfaces, Listen on \"ALL\" networks, read-only community \"public\", protocol \"udp\", port \"161\").\n")
        ip="ALL"
        network="default"
        proto="udp"
        ro_commun="public"
        port=161
    else:
        # this is doEasy = n or N, based on escaping above while answer harvester and not being y or Y
        print("Proceeding with detailed configuration.\n")

        # DETAILED CONFIG

        # force entry of udp or tcp
        proto="<unselected>"
        while ((proto != "udp") and (proto != "tcp")):
            print("Enter SNMP Protocol (udp/tcp) for example: \"udp\"")
            proto = raw_input("==> ")
            print("You entered: %s" % proto)

        # select listener ip
        # we currently do not want to allow restriction of snmp agent listener to specific addresses
        # because misconfiguration can interfere with proper function too much, this may come back at some point
        #
        # ip="<unselected>"
        # print("Enter Agent IP Address to listen for SNMP or \"ALL\": example 10.27.27.28, example: ALL")
        # ip = raw_input("==> ")
        # ip = ip.strip()
        # print("You entered: %s" % ip)
        # if (ip == "ALL"):
        #     print("Selected ALL interface/addresses for listener.")            
        # else:
        #     while ((ip != "ALL") and (validAddr(ip) == "")):
        #         print("Enter Agent IP Address to listen for SNMP or \"ALL\": example 10.27.27.28, example: ALL")
        #         ip = raw_input("==> ")
        #         ip = ip.strip()
        #         print("You entered: %s" % ip)
        #         if (ip == "ALL"):
        #             print("Selected ALL interface/addresses for listener.")            

        # select listener PORT #

        port="<unselected>"
        print("Enter Agent ip PORT number to listen for SNMP (default: 161) example: 6772")
        port = raw_input("==> ")
        port = port.strip()
        try:
            port = int(port)
        except:
            port = "<invalid integer>"
        print("You entered: %s" % port)

        while (port == "<invalid integer>"):
            print("Enter Agent ip PORT number to listen for SNMP (default: 161) example: 6772")
            port = raw_input("==> ")
            port = port.strip()
            try:
                port = int(port)
            except:
                port = "<invalid integer>"
            print("You entered: %s" % port)
            
        # select trap destinations/sinks

        network="<unselected>"
        net = "<unselected>"
        netmask = 0
        print("Enter RO (read-only) NETWORK Address to listen for SNMP or \"default\" (which means all): example 10.20.30.0/24, example: default")
        network = raw_input("==> ")
        print("You entered: %s" % network)
        network = network.strip()
        if (network.find("/") != -1):
            netsplit = network.split("/")            
            net = netsplit[0]
            try:
                netmask=int(netsplit[1])
            except:
                netmask=0

        if (network == "default"):
            print("default, i.e. all nets, selected")
        else:
            while ((validAddr(net) == "") or (netmask > 32) or (netmask < 1)):
                print("Enter RO (read-only) NETWORK Address to listen for SNMP or \"default\" (which means all): example 10.20.30.0/24, example: default")
                network = raw_input("==> ")
                print("You entered: %s" % network)
                network = network.strip()
                if (network.find("/") != -1):
                    netsplit = network.split("/")            
                    net = netsplit[0]
                try:
                    netmask=int(netsplit[1])
                except:
                    netmask=0

                if (network == "default"):
                    print("default, i.e. all nets, selected")
                    break # this is valid, bail, otherwise it has to be a specific ip/mask


        # Community
        ro_commun="<unselected>"
        while ((ro_commun == "<unselected>") or (ro_commun == "")):
            print("Enter SNMP Community (for read-only, there is no writing) for example: \"myVerySpecialCommunity\" example: \"public\"")
            ro_commun = raw_input("==> ")
            print("You entered: %s" % ro_commun)


        # DETAILED CONFIG
        
    # Location

    while ((location == "unknown") or (location == "")):
        print("Enter SNMP Location description for example: \"Main St. Downtown #1\"")
        location = raw_input("==> ")
        print("You entered: %s" % location)

    # Trap destination
        
    work2do = raw_input("Are there any Trap Destination/Sink Addresses to add? (y/n):")
    while ((work2do != "y") and (work2do != "Y") and (work2do != "n") and (work2do != "N")):            
        print("Sorry, I did not recognize that response.")
        work2do = raw_input("Are there any Trap Destination/Sink Addresses to add? (y/n):")

        
    while ((work2do == "y") or (work2do == "Y") and (work2do != "n") and (work2do != "N")):
        print("Enter the trap destinations (aka sink) where you wish to send traps (one at a time) Enter: %-20s" % ("IP ADDRESS"))
        print("Example:%-20s or %-20s\n" % ("10.1.25.33 public","anotheraddress.fqdn.net private123"))
        
        maybe_trapsink = raw_input("Enter:")
        maybe_hosts = ""
        maybe_community = ""
        
        logging.debug("You entered: %s" % maybe_trapsink)

        try:
            maybe_trapsink = maybe_trapsink.split(" ")            
            maybe_hosts = maybe_trapsink[0]
            maybe_hosts = maybe_hosts.strip()   # Should only be 1 arg entry, get whitespace gone
            maybe_community = maybe_trapsink[1]
        except:
            print("Trap destination/sink input error.")
            logging.debug("Trap destination/sink input error.")
            



        if (len(maybe_community) > 0):
            # Something to test 1 word answer for community.
            maybe_community = maybe_community
        else:
            maybe_community = "public"
            
        
        #  FQDN
        if validAddr(maybe_hosts):
            logging.debug("Valid Addr...")
        else:
            if validFQDN(maybe_hosts):
                logging.debug("Valid FQDN...")
                # Good FQDN, now ADDR
            else:
                print("This is not passing IP Address or FQDN validation, please re-enter the value.")
                maybe_hosts = ""
                work2do = "y"

        thisISright = ""
        if maybe_hosts: # if we ditched it, we won't ask if you wanna keep it
            while ((thisISright != "y") and (thisISright != "Y") and (thisISright != "n") and (thisISright != "N")):
                print("\nEntered: Host %-20s Community %-20s" % (maybe_hosts,maybe_community))
                thisISright = raw_input("CORRECT? (y/n): ")
                if (thisISright):
                    if ((thisISright == "y") or (thisISright == "Y")):
                        trap_string = maybe_hosts + " " + maybe_community
                        final_trap_hosts.append(trap_string)
                    else:
                        if ((thisISright != "n") and (thisISright != "N")):
                            maybe_hosts = ""
                            os.system("clear")



        if (len(final_trap_hosts) > 1):
            for x in xrange(len(final_trap_hosts)):
                print("Trap destination #%d = \"%s\"" % (x,final_trap_hosts[x]))
                
        work2do = raw_input("Are there more Trap Destination/Sink Addresses to add? (y/n):");
        while ((work2do != "y") and (work2do != "Y") and (work2do != "n") and (work2do != "N")):            
            print("Sorry, I did not recognize that response.")
            work2do = raw_input("Are there more Trap Destination/Sink Addresses to add? (y/n):");

    # Form SNMP "struct"

    if (ip == "ALL"):
        agentAddr = proto + ":" + str(port)
    else:
        agentAddr = proto + ":" + ip + ":" + str(port)

    SNMPstring = ""
    if ((ro_commun != "") and (network!="")):
        SNMPstring = [agentAddr, ro_commun, network, final_trap_hosts, location]        

            
    
    return SNMPstring
    
#############
# confSNMP END
#############

#############
# writeSNMP 
#############

def writeSNMP(SNMPstring,OUTPUTDIR):

    # produce puppet output
    # break out of SNMPstring
    #[agentAddr, ro_commun, network, final_trap_hosts]

    # BEGIN
    SNMP_Config_String = "class { 'snmp':\n"
    
    # Listener ADDR

    SNMP_Config_String = SNMP_Config_String +     "\tagentaddress => ['" + SNMPstring[0] + "', ],\n"

    # COMMUNITY
    SNMP_Config_String = SNMP_Config_String +     "\tro_community => '" + SNMPstring[1] +"',\n"

    # NETWORK
    SNMP_Config_String = SNMP_Config_String +     "\tro_network  => '" + SNMPstring[2] +"',\n"

    # LOCATION
    if (len(SNMPstring) > 4):
        location = SNMPstring[4]
        SNMP_Config_String = SNMP_Config_String +     "\tlocation  => '" + location  +"',\n"

    # AGENT
    SNMP_Config_String = SNMP_Config_String +     "\ttrap2_sink => ["

    for x in SNMPstring[3]:
        SNMP_Config_String = SNMP_Config_String + "'" + x + "',\n\t\t\t"

    SNMP_Config_String = SNMP_Config_String +  "\t\t],\n"

    # END
    SNMP_Config_String = SNMP_Config_String +  "}\n"
    

    filename = OUTPUTDIR + "/snmp_conf.pup"
    snmp_conf = open(filename, 'w')    
    snmp_conf.write(SNMP_Config_String)
    snmp_conf.close()

    return SNMP_Config_String
    
#############
# writeSNMP END
#############

#############
# printCensusShort
#############

def printCensusShort(censusDataShort):

    outstring = "MAC Address<-->Host Names :: "
    for unit in censusDataShort:
        outstring = outstring + unit[0] + "<-->" + unit[1] + "  "

    print(outstring)

#############
# END printCensusShort
#############


#############
# acquireMgmtMAC
#############
# Here we need to acquire the "mgmt MAC" as this is whats on the external label on the device,
# even though it is NOT how we reach the systems (which is based on BaseMac)

def acquireMgmtMAC(target):

    #target will be BaseMAC, we'll confirm we are on the right system and if so then acquire MgmtMAC and return it.

    MAChost = target.replace(":","")   # nix colons for ring command to specific member # we want this set unconditionally!
    
    logging.debug("Checking we are on correct host contacted by base mac and gathering mgmt mac from platinfo.json file.")
    commandX = "sudo /opt/plexxi/bin/plexxiRemoteCommand " + MAChost + " \'/bin/cat /etc/platinfo.json\'"                        
    logging.debug("CommandX \"%s\"" % commandX)
    try: 
        raw_resultX = subprocess.check_output(commandX,stderr=subprocess.STDOUT, shell=True)
        logging.debug("*****")
        logging.debug(raw_resultX)
        logging.debug("*****")
    except subprocess.CalledProcessError:
        print("Error encountered attempting to inspect /etc/platinfo.json for confirmation of\"%s\"" % (MAChost,subprocess.STDOUT))
        return "" # reflects failure to apply, not sure if we want to bail out here.            

    pos1 = 0
    # check for which host we're on
    if (raw_resultX):
        logging.debug(raw_resultX)                    
        pos1 = raw_resultX.find(target) # It will have :'s colons so use target rather than MAChost
        if (pos1 < 0):
            print("Error, somehow we are contacting host by MAC %s and cannot find Base MAC in output: %s! Abort for safety sake." % (target,raw_resultX))
            return ""

        pos2 = raw_resultX.find("\"MgmtMac\": ")
        if (pos2 > 0):
            acquiredMAC = raw_resultX[(pos2+11):(pos2+31)]   # out past '"MgmtMac": ' and all of quoted MAC "00:11:22:33:44:55"
            acquiredMAC = acquiredMAC.replace("\"","")  # ditch quotes
            acquiredMAC = acquiredMAC.replace(",","")  # ditch comma
            return acquiredMAC
        

#############
# END acquireMgmtMAC
#############


#############
# confHOSTADDR 
#############
# This function will ask for MAC->IPADDR->HOSTNAME in order to collect in organized manner
##################

def confHOSTADDR(censusData,targetList):

    final_hosts = []
    work2do = "y"
    # Consume HOSTADDR config from user

    # chop censusData down to only selected and further chopped to confirmed contact targets
    if (targetList != "all"):
        censusData = confirmContactTargets(targetList)
        censusDataShort = []
        for unit in censusData:
            mgmtMAC = acquireMgmtMAC(unit[0])
            logging.debug("BaseMAC %s is MgmtMAC %s" % (unit[0],mgmtMAC))
            #censusDataShort.append((unit[0],unit[1]))  # we are ditching 3/4 which is state/plexx-state which won't help much here and may confuse
            censusDataShort.append((mgmtMAC,unit[1]))  # we are ditching 3/4 which is are not useful to user and replacign 1 (index 0) with acquired Mgmt MAC which matches physical label
            
    errorBlock = ""
    censusIndex=0
    topBORDER()
    print("Plexxi has detected %d MAC addresses attached to this fabric." % (len(censusData)))
    printCensusShort(censusDataShort)
    topBORDER()
    print("You will need to associate Hostnames, IP Addresses, Netmask, and default gateways (0.0.0.0 if unconfigured) for them.\n")
    print("Enter as space/tab seperated values, in one line for one host. All fields are mandatory. <Enter> after gateway.\n")
        
    while ((work2do == "y") or (work2do == "Y") and (work2do != "n") and (work2do != "N")):

        # ALL Targets, therefore get use entire censusList
        # targetList should have been overriden with "all" content if all were anywhere in selection or nothing was selected

            
        # Still in "detected" MAC range? thus prompt accordingly
        if (censusIndex < len(censusData)):

            print("Hint: Remember \"up arrow\" to go to previous entries and make data entry simPlexxi!")

            print("\nExample:\nProvided %-21s: Enter: %-30s%-20s%-10s%-20s" % ("MAC ADDRESS","HOST NAME","IP ADDRESS","NETMASK","DEFAULT-GW-ADDRESS"))
            print("MAC #Z==> %-20s: Enter: %-30s%-20s%-10s%-20s\n" % ("00:01:02:03:04:05","myFavoriteSwitch1","10.1.25.33","16","10.1.0.1"))


            if (len(errorBlock) > 0):
                print("\n!!! Previous action had the following issue: %s !!!\n" % errorBlock)
                print("REMEMBER? -> Enter as space/tab seperated values, in one line for one host. All fields are mandatory. <Enter> after gateway.\n")

            prompt = "MAC #" + str(censusIndex) + "==> "
            sys.stdout.write(prompt)
            #sys.stdout.write(censusData[censusIndex][0])   # user will now see mgmt MAC rather than base MAC
            sys.stdout.write(censusDataShort[censusIndex][0])   # this will be the first sub element as well as second hostname acquired from censusInfo


            enterPrompt = "\n                               Enter:"
            pre_maybe_hosts = raw_input(enterPrompt)
            logging.debug("You entered: %s" % pre_maybe_hosts)

            # We're going to prepend this basically because all the processing existed before with MAC collected manually as first element
            #pre_maybe_hosts = censusData[censusIndex][0] + " " + pre_maybe_hosts
            # we want to show user mgmt MAC
            pre_maybe_hosts = censusDataShort[censusIndex][0] + " " + pre_maybe_hosts
            maybe_hosts = pre_maybe_hosts.split()  # probably replace with no args so all white space is the split TODO!!!
        # Beyond "detected" MAC range, therefore prompt for the MAC as well (legacy)
        else:
            print("\nEnter: %-20s%-20s%-20s%-10s%-20s" % ("MAC ADDRESS","HOST NAME","IP ADDRESS","NETMASK","DEFAULT-GW-ADDRESS"))
            print("Example:%-20s%-20s%-20s%-10s%-20s\n" % ("00:01:02:03:04:05","myFavoriteSwitch1","10.1.25.33","16","10.1.0.1"))

            if (len(errorBlock) > 0):
                print("\n!!! Previous action had the following issue: %s !!!\n" % errorBlock)

            enterPrompt = "Enter:"
            maybe_hosts = raw_input(enterPrompt)
            logging.debug("You entered: %s" % maybe_hosts)
            maybe_hosts = maybe_hosts.split()   # probably replace with no args so all white space is the split TODO!!!

        # reset errorBlock so that we get a "current" error rather than the last, or none rather than last
        errorBlock = ""
            
        # Lets parse what the user has given us, and ditch that line of interlinked config if anything in that line is bad
        # YES, its nested... because well it behaves well... authoring.... well harder, but thats what we do for YOU. :)
        if (len(maybe_hosts) > 4):
            if validMAC(maybe_hosts[0]):
                logging.debug("Valid MAC...")

                # Good MAC, now HOSTNAME
                if validHOST(maybe_hosts[1]):
                    logging.debug("Valid HOSTNAME...")

                    # Good HOSTNAME, now ADDR
                    if validAddr(maybe_hosts[2]):
                        logging.debug("Valid Addr...")

                        # Good host ADDR, now netmask
                        # make darn sure this netmask is an integer
                        try:
                            netmaskolla=int(maybe_hosts[3])
                        except:
                            netmaskolla=0

                        if ((netmaskolla > 1) and (netmaskolla < 32)):
                            logging.debug("Valid netmask...")

                            #Good netmask, now gateway ADDR
                            if validAddr(maybe_hosts[4]):
                                logging.debug("Valid Addr for gateway.\n")
                            else:
                                logging.debug("Bad Addr for gateway")
                                errorBlock = "Invalid Gateway Address detected \"" + maybe_hosts[4] +"\""
                                maybe_hosts = ""                    

                        else:
                            logging.debug("Bad netmask\n")
                            errorBlock = "Invalid netmask detected \"" + str(netmaskolla) + "\" should be 1-31"
                            maybe_hosts = ""                    

                    else:
                        logging.debug("Bad Addr\n")
                        errorBlock = "Invalid Address detected \"" + maybe_hosts[2] + "\""
                        maybe_hosts = ""                    
                else:
                    logging.debug("Bad HOSTNAME\n")
                    errorBlock = "Invalid HOSTNAME detected \"" + maybe_hosts[1] + "\""
                    maybe_hosts = ""
            else:
                logging.debug("Bad MAC\n")
                errorBlock = "Invalid MAC detected \"" + maybe_hosts[0] + "\""
                maybe_hosts = ""
        else:
            print("Insufficent information, Abort! (you can redo after data entry review, keep going)")
            errorBlock =  "Insuffient information provided, you are missing something"
            maybe_hosts = ""
            
        # Validate user input, or allow override in HOSTNAME case

        thisISright = ""

        if maybe_hosts: # if we ditched it, we won't ask if you wanna keep it
            while ((thisISright != "y") and (thisISright != "Y") and (thisISright != "n") and (thisISright != "N")):
                print("\nEntered: %-20s%-20s%-20s%-10s%-20s" % (maybe_hosts[0], maybe_hosts[1], maybe_hosts[2], int(maybe_hosts[3]), maybe_hosts[4]))
                thisISright = raw_input("CORRECT? (y/n): ")
                if (thisISright):
                    if ((thisISright == "y") or (thisISright == "Y")):
                        final_hosts.append(maybe_hosts)
                        censusIndex = censusIndex + 1   # if we are done and good, lets move on to next MAC <if over range, user will be prompted for manual entry if desired.
                    else:
                        if ((thisISright != "n") and (thisISright != "N")):
                            maybe_hosts = ""                            



        os.system("clear")

        print("Plexxi has detected %d MAC addresses attached to this fabric." % (len(censusData)))
        printCensusShort(censusDataShort)
        topBORDER()
        print("You will need to associate Hostnames, IP Addresses, Netmask, and default gateways (0.0.0.0 if unconfigured) for them.\n")
        print("Enter as space/tab seperated values, in one line for one host. All fields are mandatory. <Enter> after gateway.\n")

        if (final_hosts):

            printHOSTADDR(final_hosts)

            print("...")
            # Now adding more MACs only is asked if we are beyond known MACs parsed from censusInfo
            work2do = ""
            if (censusIndex >= len(censusData)):
                sys.stdout.write("You seem to have configured all your detected MAC addresses, so probably done.\n")
                sys.stdout.write("However if know there are powered down units you'd like to manually configure, we are flexxible like that. Most users answer \'n\'.\n")                
                work2do = raw_input("Are there more MAC-NAME-ADDRs to add? (y/n):");
                print("...")
                if ((work2do != "y") and (work2do != "Y") and (work2do != "n") and (work2do != "N")):
                    print("Sorry, I did not recognize that response.")        
            else:
                work2do = "y" # Because we know there are more detected MACs

                
    # force uniqueness
    if uniqueENFORCE(final_hosts):
        return final_hosts
    else:
        return ""
    
#############
# confHOSTADDR END
#############


#############
# writeHOSTADDR
#############

# produce puppet output which BETTER be unique at this point or boom boom

def writeHOSTADDR(macNAMEaddr,OUTPUTDIR):

    weMADEthis = []
    
    for block in macNAMEaddr:

        HOSTNAME_CONF_String = "class { 'hostname':\n"
        HOSTNAME_CONF_String = HOSTNAME_CONF_String + "\thostname => '"
        HOSTNAME_CONF_String = HOSTNAME_CONF_String + block[1]
        HOSTNAME_CONF_String = HOSTNAME_CONF_String + "',\n}\n"        
        
        ADDRNAME_CONF_String = "debnet::iface::static { 'mgmt':\n"
        ADDRNAME_CONF_String = ADDRNAME_CONF_String + "\taddress => '"
        ADDRNAME_CONF_String = ADDRNAME_CONF_String + block[2]
        ADDRNAME_CONF_String = ADDRNAME_CONF_String + "',\n"
        ADDRNAME_CONF_String = ADDRNAME_CONF_String + "\tnetmask => " # no single quotes for netmask
        ADDRNAME_CONF_String = ADDRNAME_CONF_String + block[3]
        ADDRNAME_CONF_String = ADDRNAME_CONF_String + ",\n" # no single quotes for netmask
        ADDRNAME_CONF_String = ADDRNAME_CONF_String + "\tgateway => '"
        ADDRNAME_CONF_String = ADDRNAME_CONF_String + block[4]
        ADDRNAME_CONF_String = ADDRNAME_CONF_String + "',\n"
        ADDRNAME_CONF_String = ADDRNAME_CONF_String + "}\n"


        # push puppet output to file (for transport to switch, executed there)


        hostfilename = OUTPUTDIR + "/hostname_" + block[0] + "_conf.pup"
        hostname_conf = open(hostfilename, 'w')    
        hostname_conf.write(HOSTNAME_CONF_String)
        hostname_conf.close()

        addrfilename = OUTPUTDIR + "/addrname_" + block[0] + "_conf.pup"
        addrname_conf = open(addrfilename, 'w')    
        addrname_conf.write(ADDRNAME_CONF_String)
        addrname_conf.close()

        weMADEthis.append([hostfilename,addrfilename])

    return weMADEthis

#############
# writeHOSTADDR END
#############


#############
# mkOUTDIR , also cleans up older than 20 backups
#############

def mkOUTDIR(backupDir):

    numBackups = int(20)
    
    if not os.path.exists(backupDir):
        try:
            os.mkdir(backupDir)
            logging.debug("Created directory \"%s\" to warehouse backups\n" % backupDir)
        except OSError as e:
            print("Error encountered \"%s\", attempting to create output directory \"%s\", you will need to correct this. Exiting...\n\n\n" % (e.strerror, outputDirectory))
            logging.debug("Error encountered \"%s\", attempting to create output directory \"%s\", you will need to correct this. Exiting...\n\n\n" % (e.strerror, outputDirectory))
            sys.exit(1)
    else:
        logging.debug("%s, exists" % backupDir)

        
    removeFiles = True
    
    while (removeFiles):

        # acquire new list of files (as it will change if we remove things, and we MAY have to remove more than one in certain cases of manual copying, etc.)
        backupFiles = os.listdir(backupDir)
        backupFiles_w_path = []
        for file in backupFiles:        
            backupFile_w_path = backupDir + "/" + file
            backupFiles_w_path.append(backupFile_w_path)
        backupFiles_w_path.sort(key=os.path.getmtime)
        logging.debug(backupFiles)

        if (len(backupFiles) > numBackups):
            removeFiles = True
        else:
            removeFiles = False

        if (removeFiles):
            if (len(backupFiles_w_path) > 0):
                print("File %s" % backupFiles_w_path[0])
                if os.path.exists(backupFiles_w_path[0]):
                    try:
                        shutil.rmtree(backupFiles_w_path[0])
                    except OSError as e:
                        print("Error removing old backup directory %s with error \"%s\"" % (backupFiles_w_path[0],e))
                        logging.debug("Error removing old backup directory %s with error \"%s\"" % (backupFiles_w_path[0],e))        
            else:                
                logging.debug("No backups detected")
        else:            
            logging.debug("No excessive backups found, therefore removing nothing.")
            
    # Make a directory for the output, if it doesn't exist (tell user so), report errors if any
    outputDirectory = backupDir + "/" + datetime.datetime.now().strftime("%Y-%m-%d_%H-%M")            
    
    if not os.path.exists(outputDirectory):
        try:
            os.mkdir(outputDirectory)
            logging.debug("Created directory \"%s\" to warehouse output\n" % outputDirectory)
        except OSError as e:
            print("Error encountered \"%s\", attempting to create output directory \"%s\", you will need to correct this. Exiting...\n\n\n" % (e.strerror, outputDirectory))
            logging.debug("Error encountered \"%s\", attempting to create output directory \"%s\", you will need to correct this. Exiting...\n\n\n" % (e.strerror, outputDirectory))            
            sys.exit(1)
    else:
        logging.debug("Directory \"%s\" exists already, hmmmm, might want to clean it out first or wait a minute and re-run\n" % outputDirectory)

    return outputDirectory


#############
# mkOUTDIR END
#############

#############
# cpALL
#############
def cpALL(fromDIR,toDIR):
    
    if (fromDIR == toDIR):
        print("Same directory forgo copying.")
        return "success"    
    commandX = "cp -Rp " + fromDIR + "/* " + toDIR
    logging.debug("CommandX \"%s\"" % commandX)
    try: 
        raw_resultX = subprocess.check_output(commandX,stderr=subprocess.STDOUT, shell=True)
        logging.debug("*****")
        logging.debug(raw_resultX)
        logging.debug("*****")
    except subprocess.CalledProcessError:
        print("Error encountered attempting to copy files: command \"%s\" --error-- \"%s\"" % (commandX,subprocess.STDOUT))
        return "" # reflects failure to apply, not sure if we want to bail out here.            

    
    return "success"

#############
# cpALL END
#############


#############
# findLastDIR , finds last (5? 1?) directory(ies?) to allow user to recover cached data from previous run (really important for portsup)
#############

def findLastDIR(backupDir):

    numBackups = int(20)
    
    if not os.path.exists(backupDir):
        try:
            os.mkdir(backupDir)
            logging.debug("Created directory \"%s\" to warehouse backups\n" % backupDir)
        except OSError as e:
            logging.debug("Could not find previous configuration data directory...this MIGHT be fine.\n\n\n" % (e.strerror, outputDirectory))
            return "" # as in I could find Z-ear-oh!
    else:
        logging.debug("%s, exists" % backupDir)
        
    backupFiles = os.listdir(backupDir)
    backupFiles_w_path = []
    for file in backupFiles:
        backupFile_w_path = backupDir + "/" + file
        backupFiles_w_path.append(backupFile_w_path)
    backupFiles_w_path.sort(key=os.path.getmtime)

    if (len(backupFiles_w_path) > 0):
        print("Lastest backup file is %s" % backupFiles_w_path[len(backupFiles_w_path)-1])
        return backupFiles_w_path[len(backupFiles_w_path)-1]
    else:
        logging.debug("Could not find previous configuration data files...this MIGHT be fine.\n\n\n")
        return "" # as in I could find Z-ear-oh!

#############
# findLastDIR END
#############





#############
# printPORTSUPinfo
#############

def printPORTSUPinfo(masterPORTSUPinfo):
    #topBORDER()
    if (len(masterPORTSUPinfo) < 3):        
        return ""

    portsList = masterPORTSUPinfo[0]
    if (len(portsList) == 0):
        return ""

    tempPort = portsList[0].split(" ")
    lastMAC = tempPort[0]    
    sys.stdout.write("Automatic Port Discovery:\n\tMAC: %s Ports/Speeds:\t" % lastMAC)
    
    for port in portsList:
        portSet = port.split(" ")
        if (portSet[0] != lastMAC):
            lastMAC = portSet[0]
            sys.stdout.write("\n\tMAC: %s Ports/Speeds:\t" % lastMAC)
        sys.stdout.write("%s/%s\t" % (portSet[1],portSet[2]))
    print(" ")
    #bottomBORDER()

#############
# printPORTSUPinfo END
#############

#############
# printPORTSUP
#############

def printPORTSUP(PORTSUPstring):

    if (len(PORTSUPstring) < 2):
        return ""
    
    print("Switch Ports Speed Setting:\t%s" % PORTSUPstring[0])
    nativeVLAN = PORTSUPstring[1].split(" ")
    if ((nativeVLAN[0] == "y") or (nativeVLAN[0] == "Y")):
        nativeVLANID = nativeVLAN[1]
    else:
        nativeVLANID = "none"
    print("\t\tVLAN native:\t%s" % nativeVLANID)
    print("\t\tVLAN non-native IDs:\t%s" % PORTSUPstring[2])


#############
# printPORTSUP END
#############


#############
# writePORTSUP
#############

def writePORTSUP(portsLISTinfo,OUTPUTDIR):

    # first we strip off the "xp" from port number so this can be digested by existing CTRL end script
    if (len(portsLISTinfo) == 3):
        portsLIST = portsLISTinfo[0] # the port info
        nativeVLANinfo = portsLISTinfo[1] # the (native vlan boolean, native vlan id) pair
        allVLANinfo = portsLISTinfo[2] # the non-native vlan ids in a list
    else:
        logging.debug("writePORTSUP: Insufficient args in portsLISTinfo, return.")
        return ""  # no valid key variable, exit

    newportsLIST = []
    for port in portsLIST:
        port = port.split(" ")
        macADDR = port[0]
        portNUM = port[1][2:]
        speed = port[2]
        portINFO = macADDR + " " + portNUM + " " + speed
        newportsLIST.append(portINFO)

    newportsLIST.sort()
  
    newportsLISTinfo = [newportsLIST, nativeVLANinfo, allVLANinfo]
    
    
    try:
        portsupDataFileName = OUTPUTDIR + "/portsup.dat"
        portsupDataFile = open(portsupDataFileName, 'wb')
        pickle.dump(newportsLISTinfo,portsupDataFile)
        portsupDataFile.close()
    except:
        print("Error writing portsup data to data file for CTRL.")
        logging.debug("Error writing portsup data to data file for CTRL.")
        return ""
    # This will need to be run on CTRL to ensure we have the right python running with bindings etc. --->>    ". /opt/rh/python27/enable"

    
    return 1

#############
# writePORTSUP END
#############

#############
# writeSWITCH
#############
# Format should be kept straight in what is written into writeSWITCH as writeSWITCH([CTRLstring,PORTSUPstring,NTPstring,DNSstring,SNMPstring,TZstring,HostADDRstring,masterPORTSUPinfo],OUTPUTDIR)
def writeSWITCH(localSWITCHinfo,OUTPUTDIR):
  
    newSWITCHinfo = localSWITCHinfo
        
    try:
        switchDataFileName = OUTPUTDIR + "/switch.dat"
        switchDataFile = open(switchDataFileName, 'wb')
        pickle.dump(newSWITCHinfo,switchDataFile)
        switchDataFile.close()
    except:
        print("Error writing switch data to data file for CTRL.")
        logging.debug("Error writing switch data to data file for CTRL.")
        return ""

    
    return 1

#############
# writeSWITCH END
#############


#############
# readCache
#############

def readCache(masterSWITCHinfo,masterPORTSUPinfo,lastDir):

    masterPORTSUPinfo = readPORTSUP(masterPORTSUPinfo, lastDir)
    masterSWITCHinfo = readSWITCH(masterSWITCHinfo, lastDir)
    return [masterSWITCHinfo,masterPORTSUPinfo]

#############
# readCache END
#############

#############
# readSWITCH
#############

def readSWITCH(localSWITCHinfo,INPUTDIR):
    
    try:
        switchDataFileName = INPUTDIR + "/switch.dat"
        switchDataFile = open(switchDataFileName, 'rb')
        localSWITCHinfo = pickle.load(switchDataFile)
        switchDataFile.close()
    except:
        print("Error reading switch data file.")
        return ""

    return localSWITCHinfo

#############
# readSWITCH END
#############


#############
# readPORTSUP
#############

def readPORTSUP(portsLISTinfo,INPUTDIR):
    
    try:
        portsupDataFileName = INPUTDIR + "/portsup.dat"
        portsupDataFile = open(portsupDataFileName, 'rb')
        portsLISTinfo = pickle.load(portsupDataFile)
        portsupDataFile.close()
    except:
        print("Error reading portsup data file.")
        return ""

    return portsLISTinfo    

#############
# readPORTSUP END
#############


#############
# scpRECONCILE 
#############


def scpRECONCILE(CTRLstring,OUTPUTDIR):

        print("scp (secure copy) portsup data file and required files to Plexxi Control.")

        portsupDataFileName = OUTPUTDIR + "/portsup.dat"
        switchDataFileName = OUTPUTDIR + "/switch.dat"

        scriptFiles = "/opt/plexxi/PlexxiSupport/installTools/simplexxi/reconcile_ctrl_simplexxi.py "
        scriptFiles = scriptFiles + " /opt/plexxi/PlexxiSupport/installTools/simplexxi/enable_ports_simplexxi_lib.py "
        scriptFiles = scriptFiles + " /opt/plexxi/PlexxiSupport/installTools/simplexxi/update_names_ctrl_simplexxi_lib.py "
        filesToTransport = portsupDataFileName + " " + scriptFiles
        filesToTransport = switchDataFileName + " " + filesToTransport 

        userName = "plexxi"

        # We should be passing in CTRLstring always in this case, BUT, sometimes it looks like we don't so doublecheck we have a sensible value
        if (CTRLstring != ""): # if we have no CTRLstring it may have been unconfigured, so don't use that!
            commandX = "sudo /usr/bin/scp " + " " + filesToTransport + userName + "@" + CTRLstring + ":/tmp"
        else:
            if (CTRLstringFOUND != ""):
                CTRLstring = CTRLstringFOUND
                commandX = "sudo /usr/bin/scp " + " " + filesToTransport + userName + "@" + CTRLstring + ":/tmp"
            else:
                return ""      #This leaves little choice but to fail, due to lack of pyschic feature. 
        logging.debug("CommandX \"%s\"" % commandX)
        try: 
            raw_resultX = subprocess.check_output(commandX,stderr=subprocess.STDOUT, shell=True)
            logging.debug("*****")
            logging.debug(raw_resultX)
            logging.debug("*****")
        except subprocess.CalledProcessError:
            print("Error encountered attempting to transfer files: command \"%s\" --error-- \"%s\"" % (commandX,subprocess.STDOUT))
            return "" # reflects failure to apply, not sure if we want to bail out here.            

    
        return "success"

#############
# scpRECONCILE END
#############

#############
# execRECONCILEatCTRL 
#############

def execRECONCILEatCTRL(CTRLstring):

    print("Remotely executing CTRL script on CTRL to reconcile SWITCH and CONTROLLER configurations.")

    userName = "plexxi"

    # We should be passing in CTRLstring always in this case, BUT, sometimes it looks like we don't so doublecheck we have a sensible value
    
    if (CTRLstring != ""): # if we have no CTRLstring it may have been unconfigured, so don't use that!        
        commandY = "/tmp/reconcile_ctrl_simplexxi.py"
        hoststring = userName + "@" + CTRLstring
    else:
        if (CTRLstringFOUND != ""):
            commandY = "/tmp/reconcile_ctrl_simplexxi.py"
            hoststring = userName + "@" + CTRLstringFOUND
        else:
            return ""      #This leaves little choice but to fail, due to lack of pyschic feature. 

    logging.debug("CommandY \"%s\"" % commandY)
    
    ssh = subprocess.Popen(["/usr/bin/ssh", "%s" % hoststring, commandY],shell=False,stdout=subprocess.PIPE,stderr=subprocess.PIPE)

    result = ssh.stdout.readlines()
    if (result == []):
        error = ssh.stderr.readlines()
        print("ERROR detected: %s" % error)
        return ""
    # if we get here result is good, return that.
    return result

#############
# execRECONCILEatCTRL END
#############


#############
# printALL 
#############

def printALL(CTRLstring,PORTSUPstring,NTPstring,DNSstring,SNMPstring,TZstring,HostADDRstring,masterPORTSUPinfo):

        ######### REPORT AND repeat? ###########
        os.system("clear")
        print("**********************************************************************************")
        print("**************  These details will BE UNIQUE per switch.   ***********************")
        print("**********************************************************************************\n")
        
        if (HostADDRstring):
            printHOSTADDR(HostADDRstring)

        if (PORTSUPstring):
            printPORTSUP(PORTSUPstring)

        if (masterPORTSUPinfo):
            printPORTSUPinfo(masterPORTSUPinfo)

        print("\n\n**********************************************************************************")
        print("**************  These details will apply to ALL switches   ************************")
        print("***********************************************************************************\n")

        topLongBORDER()
        
        if(CTRLstring):
            printCTRL(CTRLstring)

        if (NTPstring):
            printNTP(NTPstring)

        if (DNSstring):
            printDNS(DNSstring)

        if (SNMPstring):
            printSNMP(SNMPstring)

        if (TZstring):
            printTZ(TZstring)

        bottomLongBORDER()


#############
# printALL END 
#############


#############
# printNTP 
#############

def printNTP(NTPstring):
    #topBORDER()
    print("NTP Address(s):\t\t%s" % NTPstring)
    #bottomBORDER()

#############
# printNTP END
#############

#############
# printSNMP 
#############

def printSNMP(SNMPstring):
    #topBORDER()
    # SNMPstring =>> [agentAddr (proto:IP:161 or proto:161) , ro_commun (string), network (ip/mask or Default), final_trap_hosts (list of IPs)]
    #[agentAddr, ro_commun, network, final_trap_hosts]
    print("SNMP Configuration:")
    print("\tAgent addresss:\t\t%s" % SNMPstring[0])
    print("\tCommunity:\t\t%s" % SNMPstring[1])
    print("\tNetwork (\"default\" means \"all\") :\t\t%s" % SNMPstring[2])
    print("\tTrap Destination/sinks:\t%s" % SNMPstring[3])
    if (len(SNMPstring) > 4):
        print("\tLocation:\t%s" % SNMPstring[4])

    #bottomBORDER()

#############1
# printSNMP END
#############


#############
# printTZ 
#############


def printTZ(TZstring):
    #topBORDER()
    print("Time Zone:\t\t%s" % TZstring)
    #bottomBORDER()
    
#############
# printTZ END
#############

#############
# printDNS 
#############


def printDNS(DNSstring):
    #topBORDER()
    print("DNS IP Address:\t\t%s" % DNSstring[0])
    print("DNS domain:\t\t%s" % DNSstring[1]) 
    #bottomBORDER()
    
#############
# printDNS END
#############


#############
# printCTRL 
#############

def printCTRL(CTRLstring):
    #topBORDER()
    print("Control IP Address:\t\t%s" % CTRLstring) 
    #bottomBORDER()

    
#############
# printCTRL END
#############

#############
# printHOSTADDR 
#############

def printHOSTADDR(HostADDRstring):


    topLongBORDER()
    print("|\t\t%-20s%-20s%-20s%-10s%-20s\t|" % ("MAC Addr","Hostname","IP Addr","Netmask","Default Gateway"))

    for x in xrange(len(HostADDRstring)):
        sys.stdout.write("| MAC #")
        print("%d\t%-20s%-20s%-20s%-10s%-20s\t|" % (x,HostADDRstring[x][0],HostADDRstring[x][1],HostADDRstring[x][2],HostADDRstring[x][3],HostADDRstring[x][4]))

    bottomLongBORDER()
        
#############
# printHOSTADDR END
#############
 


###########
# collectData -- collects user input through questioning and generates puppet manifests
###########


def collectData(theChosenOnes,work2do,OUTPUTDIR,CTRLstring,PORTSUPstring,NTPstring,DNSstring,SNMPstring,TZstring,HostADDRstring,masterPORTSUPinfo,censusData,targetList):

    while ((work2do != "y") and (work2do != "Y")):

        ####### COLLECT ########
        # We'll check "theChosenOnes" and prompt only for the chosen, OR "all" (which should be default)

        # Control IP
        # The current controller IP address should be harvested from censusInfo, so basically its SHOULD always be a choice of:
        # 1) no action, reconfigures ctrl ip (no op on switch)
        # 2) reconfiguration, reconfigures ctrl ip on switch
        # so there is really no "configure" unless we for some strange reason cannot harvest a CTRL IP

        if ((theChosenOnes.find("ctrl") >= 0) or (theChosenOnes.find("all") >= 0)):
        
            #os.system("clear") # leave welcome screen up for a tic for reading if desired.

            if (CTRLstring):
                printCTRL(CTRLstring)
                ctrlprompt = "RE-configure Control Address? "
            else:
                ctrlprompt = "Configure Control Address? "

            if doThis(ctrlprompt):
                logging.debug("Proceed with CTRL...")
                CTRLstring = confCTRL()
            else:
                logging.debug("Skip CTRL...")
            
        # NTP

        if ((theChosenOnes.find("ntp") >= 0) or (theChosenOnes.find("all") >= 0)):
            os.system("clear")

            if (NTPstring):
                printNTP(NTPstring)
                ntpprompt = "RE-configure NTP manually? "
            else:
                ntpprompt = "Configure NTP manually? "

            if doThis(ntpprompt):
                logging.debug("Proceed with NTP...")
                NTPstring = confNTP()
            else:
                logging.debug("Skip NTP...")

            
        # TimeZone
        if ((theChosenOnes.find("tz") >= 0) or (theChosenOnes.find("all") >= 0)):
            os.system("clear")
            if (TZstring):
                printTZ(TZstring)
                tzprompt = "RE-configure Time Zone? "
            else:
                tzprompt = "Configure Time Zone? "

            if doThis(tzprompt):
                logging.debug("Proceed with Time Zone...")
                TZstring = confTIMEZONE()
            else:
                logging.debug("Skip Time Zone...")

            
        # DNS
        if ((theChosenOnes.find("dns") >= 0) or (theChosenOnes.find("all") >= 0)):
            os.system("clear")
            if (DNSstring):
                printDNS(DNSstring)
                dnsprompt="RE-configure DNS manually?"
            else:
                dnsprompt="Configure DNS manually?"

            if doThis(dnsprompt):
                logging.debug("Proceed with DNS...")
                DNSstring = confDNS()
            else:
                logging.debug("Skip DNS...")

        # SNMP
        if ((theChosenOnes.find("snmp") >= 0) or (theChosenOnes.find("all") >= 0)):
            os.system("clear")
            if (SNMPstring):
                printSNMP(SNMPstring)
                snmpprompt="RE-configure SNMP manually?"
            else:
                snmpprompt="Configure SNMP manually?"

            if doThis(snmpprompt):
                logging.debug("Proceed with SNMP...")
                SNMPstring = confSNMP()
            else:
                logging.debug("Skip SNMP...")


        # IP Addresses & Hostnames
        if ((theChosenOnes.find("hostaddr") >= 0) or (theChosenOnes.find("all") >= 0)):
            #print("censusData %s " %censusData)
            os.system("clear")
            if (HostADDRstring):
                printHOSTADDR(HostADDRstring)
                hostprompt = "RE-configure Switch Management IP addresses and hostnames?"
            else:
                hostprompt = "Configure Switch Management IP addresses and hostnames?"

            if doThis(hostprompt):
                logging.debug("Proceed with IPs and names...")
                HostADDRstring =  confHOSTADDR(censusData,targetList)
            else:
                logging.debug("Skip IPs and names...")


        # PORTSUP  --- enable all ports with basic config (non-persistant)

        if ((theChosenOnes.find("portsup") >= 0) or (theChosenOnes.find("all") >= 0)):
            os.system("clear")

            printPORTSUP(PORTSUPstring)
            
            if (len(masterPORTSUPinfo) == 3):
                printPORTSUPinfo(masterPORTSUPinfo)
                portsupprompt = "Re-Configure and enable ports up for discovery?\n(Action may require several minutes!!)\n"
            else:
                portsupprompt = "Configure and enable ports up for discovery?\n(Action may require several minutes!!)\n"

            if doThis(portsupprompt):
                logging.debug("Proceed with Enable Ports Config...")
                PORTSUPstring = confPORTSUP()
                masterPORTSUPinfo = discPORTSUP(PORTSUPstring,theTargets,OUTPUTDIR)
            else:
                logging.debug("Skip Enable Ports Config...")

        printALL(CTRLstring,PORTSUPstring,NTPstring,DNSstring,SNMPstring,TZstring,HostADDRstring,masterPORTSUPinfo)
                
        #### repeat? ####

        work2do = "YES?"

        while ((work2do != "y") and (work2do != "Y") and (work2do != "n") and (work2do != "N") and (work2do != "q") and (work2do != "Q")):
            work2do = raw_input("REVIEW information carefully.\n(\"y\" will save this information)\n(\"n\" will allow for corrections before saving)\n(\"q\" will abandon changes made this round)\n (y/n/q): ");
            if ((work2do == "q") or  (work2do == "Q")):
                return "" #sys.exit(0) # QUITTER! but exit with "0" non error as this is user initiated exit
            if ((work2do != "y") and (work2do != "Y") and (work2do != "n") and (work2do != "N") and (work2do != "q") and (work2do != "Q")):
                sys.stdout.write("What? %s is not (y/n/q)" % work2do)            
        os.system("clear")

        # END of input while loop

    # We should be good and confirmed at this point, let write to puppet files (which will be transported to switch and applied seperately    
    
    sys.stdout.write("\nAs you command!\nCaching switch data entries for later...")
    logging.debug("Caching switch data entries for later...")

    if(writeSWITCH([CTRLstring,PORTSUPstring,NTPstring,DNSstring,SNMPstring,TZstring,HostADDRstring,masterPORTSUPinfo],OUTPUTDIR)):
        sys.stdout.write("Generated Switch data cache.")
        logging.debug("Generated Switch data cache.")
        # errors generated from function itself.

    sys.stdout.write("\nGenerating puppet files which are host specific...")
    files = ""
    if HostADDRstring:
        sys.stdout.write("Generating hostname_MAC#_conf.pup & addrname_MAC#_conf.pup for each MAC#...")
        files = writeHOSTADDR(HostADDRstring,OUTPUTDIR)    
    sys.stdout.write("done.\n")
    if files:
        for file in files:
            print("%s AND %s generated.\n" % (file[0],file[1]))

    sys.stdout.write("Generating puppet files which are universal...")
    # CTRL and PORTSUP config works differently than puppet driven and so this is a no-op but included for consistency
    if CTRLstring:
        sys.stdout.write("Generating CTRL config...pass trough...")
        writeCTRL(CTRLstring,OUTPUTDIR)
    #if PORTSUPstring:
    #    sys.stdout.write("This happens during collection.")
    # These really write files.
    if NTPstring:
        sys.stdout.write("Generating ntp_conf.pup...")
        writeNTP(NTPstring,OUTPUTDIR)
    if DNSstring:
        sys.stdout.write("Generating dns_conf.pup...")
        writeDNS(DNSstring,OUTPUTDIR)
    if SNMPstring:
        sys.stdout.write("Generating snmp_conf.pup...")
        writeSNMP(SNMPstring,OUTPUTDIR)
    if TZstring:
        sys.stdout.write("Generating tz_conf.pup...")
        writeTZ(TZstring,OUTPUTDIR)
    sys.stdout.write("done.\n")

    logging.debug("See output directory:  %s" % OUTPUTDIR)

    return (work2do,OUTPUTDIR,CTRLstring,PORTSUPstring,NTPstring,DNSstring,SNMPstring,TZstring,HostADDRstring,masterPORTSUPinfo,censusData)
    
###########
# collectData End
###########


###########
# packageData - tar it up  !(gzip it up) seems excessive to compress/decompress
###########

def packageData(OUTPUTDIR):

    if not os.path.exists(OUTPUTDIR):
        print("Cannot find the output directory \"%s\"" % OUTPUTDIR)
        print("This means we can proceed no further, sorry, try again perhaps.")
        #sys.exit(1)    # don't need to exit here any more due to menu drive
        return ""
    else:
        # Directory exists lets roll

        package1name = "/tmp/ring_conf_pkg.tar"
        command1 = "tar -Pcvf " + package1name + " " + OUTPUTDIR 
        print("COMMAND %s" % command1)
        print("Tarring up %s contents to %s" % (OUTPUTDIR, package1name))
          
        try:
            os.system(command1)
        except OSError as e:
            print("Error encountered \"%s\", attempting to create\"%s\". Exiting...\n\n\n" % (e.strerror, outputDirectory))            
            sys.exit(1)
            
        command2 = "cp " + package1name + " " + OUTPUTDIR 
        print("COMMAND %s" % command2)
        print("Copying up %s package to %s directory" % (OUTPUTDIR, package1name))
          
        try:
            os.system(command2)
        except OSError as e:
            print("Error encountered copying \"%s\",  to directory\"%s\". Exiting...\n\n\n" % (e.strerror, outputDirectory))

            sys.exit(1)

    return package1name

###########
# packageData End
###########


###########
# distributeDataTargeted -- Sends data ONLY to the list of targets rather than via ring command.
###########

def distributeDataTargeted(packageName,destDir,hostList):

    ##### Copy  package of puppet files around ring/fabric.
    print("PACKAGE NAME !! %s " % packageName)
    if not os.path.exists(packageName):
        print("Cannot find the package directory \"%s\"" %  packageName)
        print("This means we can proceed no further, sorry, try again perhaps.")
        #sys.exit(1) #not necessary to exit due to menu drive
        return ""
    else:
        # Directory exists lets roll
        for mac in hostList:
            MACHOST = mac.replace(":","")
            commandX = "sudo /opt/plexxi/bin/pcp " + packageName + " " + MACHOST + ":/tmp/ring_conf_pkg.tar"
            commandY = "sudo /opt/plexxi/bin/plexxiRemoteCommand " + MACHOST + " \'/bin/tar -xvf /tmp/ring_conf_pkg.tar'"

            logging.debug("CommandX \"%s\"" % commandX)
            print("Transporting configuration files to \"%s\"" % MACHOST)
            try: 
                raw_resultX = subprocess.check_output(commandX,stderr=subprocess.STDOUT, shell=True)
                logging.debug("*****")
                logging.debug(raw_resultX)
                logging.debug("*****")
            except subprocess.CalledProcessError:
                print("Error encountered attempting to distribute configuration to host \"%s\", error \"%s\"" % (MACHOST,subprocess.STDOUT))
                return "" # reflects failure to apply, not sure if we want to bail out here.

            logging.debug("CommandY \"%s\"" % commandY)
            try: 
                raw_resultY = subprocess.check_output(commandY,stderr=subprocess.STDOUT, shell=True)
                logging.debug("*****")
                logging.debug(raw_resultY)
                logging.debug("*****")
            except subprocess.CalledProcessError:
                print("Error encountered attempting to unpack configuration on host \"%s\", error \"%s\"" % (MACHOST,subprocess.STDOUT))
                return "" # reflects failure to apply, not sure if we want to bail out here.                
            
    return "success" # I guess since command did not "bomb" out by pcp doesn't seem to ever give an error either... hmmm


###########
# distributeDataTargeted End
###########


###########
# verifyDistTargeted
###########


def verifyDistTargeted(packageName,destDir,hostList):

    allGood = "True"
    
    print("Confirm delivery of whole config package.")
    command1 = "/usr/bin/cksum " + packageName
    try: 
        raw_result1 = subprocess.check_output(command1,stderr=subprocess.STDOUT, shell=True)
        logging.debug("*****")
        logging.debug(raw_result1)
        logging.debug("*****")
    except subprocess.CalledProcessError:
        print("Error encountered attempting to run cksum: \"%s\"" % subprocess.STDOUT)
        print("!!!! You might not have a package to distribute or you removed it? !!!!")
        return "" # reflects failure to verify
    
    logging.debug("\"%s\"" % raw_result1)
    result1 = raw_result1.split()
    SUM = result1[0]
    SIZE = result1[1]
    print("Local SUM is %s, SIZE of %s" % (result1[0],result1[1]))


    ##### Loop through hosts and check each  ##########

    for mac in hostList:
        MACHOST = mac.replace(":","")

        command2 = "sudo /opt/plexxi/bin/plexxiRemoteCommand " + MACHOST + " \'/bin/hostname; /usr/bin/cksum /tmp/ring_conf_pkg.tar'"

        logging.debug("Command2 \"%s\"" % command2)
        try: 
            raw_result2 = subprocess.check_output(command2,stderr=subprocess.STDOUT, shell=True)
            logging.debug("*****")
            logging.debug(raw_result2)
            logging.debug("*****")
        except subprocess.CalledProcessError:
            print("Error encountered attempting to run cksum: \"%s\"" % subprocess.STDOUT)
            print("!!!! You might not have a package to distribute or you removed it? !!!!")
            return "" # reflects failure to verify

        outlines = raw_result2.split("\n")
        for line in outlines:
            if (line.find(packageName) >= 0):
                choppedline = line.split()
                if (line.find("Output") == -1):
                    logging.debug("This  %s" % choppedline)
                    if ((choppedline[0] != SUM) or (choppedline[1] != SIZE)):
                        print("ERROR SIZES don't match! Possible ring copy error, examine manually!")
                        print("Local SUM %s, SIZE %s" % (SUM,SIZE))
                        print("This member SUM %s, SIZE %s" % (choppedline[0],choppedline[1]))
                        allGood = "False"
    
    
    return allGood
    


###########
# verifyDistTargeted End
###########

###########
# confirmContactTargets -- for user specified hosts, we check against censusInfo and warn user if MACs are not present. -- key beafore users wastes time entering for wrong mac in the end. -- also recommend just before any trigger pulls JUST in case anything changes while user enter's data ... slowly... even leaving a screen overnight! expect the unexpected, human thread safe?
###########

def confirmContactTargets(hostList):

    censusDataNOW = collectCensusData()                       
    hostCensusDataNOW = censusDataNOW[0]
    resultsList = []
    
    # first things first, form resultsList from data which exists in both hostList and hostCensusDataNOW
    for host in hostList:
        for x in xrange(len(hostCensusDataNOW)):  # run through rawHostAddrString (we only really care for first index, host-mac)
            if (hostCensusDataNOW[x][0].upper() == host.upper()):
                logging.debug("Mac match %s from censusDataNOW" % hostCensusDataNOW[x][0])
                resultsList.append(hostCensusDataNOW[x])
                # else we simply abandon it
                
    return resultsList

###########
# confirmContactTargets END
###########



###########
# applyDataTargted --- i.e. this will only be sent to hosts listed in hostList we need census data in this case for mac list to configure, when macs are NOT being configure (otherwise they'd be in HostADDRstring)
###########


def applyDataTargeted(packageName,destDir,CTRLstring,PORTSUPstring,NTPstring,DNSstring,SNMPstring,TZstring,HostADDRstring,targetList):

    reachableNOW = confirmContactTargets(targetList)

    if (len(reachableNOW) < len(targetList)):
        print("NOT all targets reachable!! Something must have changed in fabric. I can make a best effort......")
        
    print("We will now apply configurations, by type to these hosts %s:" % targetList)

            
    # How we run through targetList for ALL items which were configured during input phase
    
    ## This will need some serious review as what is the "MAC" is open to interpetation, for our purposes, right now, MAC is "MacBase", lowest number that could be owned by host.
    ### some hardware models may use this as the mgmt MAC, others will use the opposite end of the range (+128)

    # This counter tracks the same elemnt of targetList (which had better be in the same order or nothing will get done.)
    # targetList contains list of "base macs"
    # HostADDRstring contains list of "mgmt macs/label macs" sometimes same, sometimes +7F ; + ip/mask/gw
    # hostCounter = 0 
    # if (not HostADDRstring):
    #     HostADDRstring = []
    #     logging.debug("Resetting HostADDRstring as not configured, acquire mgmt macs automatically")
    #     print("Resetting HostADDRstring as not configured, acquire mgmt macs automatically")
    #     for target in targetList:
    #         mgmtMAC = acquireMgmtMAC(target)
    #         HostADDRstring.append([mgmtMAC,"unknown"])
    #         logging.debug("BaseMAC %s is MgmtMAC %s" % (target,mgmtMAC))


    
    # Aqcuire MAC from /etc/platinfo.json and confirm we are targetting the correct hose before execution.    
    for target in targetList:

        thisHostADDRstring = acquireMgmtMAC(target)
#        hostCounter = hostCounter + 1 #tidy
        
        MAChost = target.replace(":","")   # nix colons for ring command to specific member # we want this set unconditionally!         
        
        # Lets check some things, are we contacting the host we expect?
        # Is the matching host config file there for hostname AND interfaces?                
        print("Checking we are on correct host")
        commandX = "sudo /opt/plexxi/bin/plexxiRemoteCommand " + MAChost + " \'/bin/cat /etc/platinfo.json\'"                        
        logging.debug("CommandX \"%s\"" % commandX)
        try: 
            raw_resultX = subprocess.check_output(commandX,stderr=subprocess.STDOUT, shell=True)
            logging.debug("*****")
            logging.debug(raw_resultX)
            logging.debug("*****")
        except subprocess.CalledProcessError:
            print("Error encountered attempting to inspect /etc/platinfo.json for confirmation of\"%s\"" % (MAChost,subprocess.STDOUT))
            return "" # reflects failure to apply, not sure if we want to bail out here.            

        pos1 = 0
        # check for which host we're on
        if (raw_resultX):
            logging.debug(raw_resultX)                    
            pos1 = raw_resultX.find(target) # It will have :'s colons so use target rather than MAChost
            if (pos1 < 0):
                print("Error, somehow we are contacting host by MAC %s and cannot find Base MAC in output: %s! Abort settings for this mac for safety sake." % (target,raw_resultX))
                return ""

        # # If we have a HostADDRstring, we should only hit each element once
        # for hostset in HostADDRstring:        
        #     logging.debug("hostset[0] %s, MAChost %s" % (hostset[0],MAChost))            
        #     tempMAC = hostset[0].replace(":","")
        #     if (MAChost == tempMAC):
        #         thisHostADDRstring.append(hostset)
        #         # else we don't add to list

        ## UNTAR, required for all ###


        sys.stdout.write("UNPACK: untarring...")
        command1 = "sudo /opt/plexxi/bin/plexxiRemoteCommand " + MAChost + " \'/bin/hostname; cd /tmp; /bin/tar -Pxvf /tmp/ring_conf_pkg.tar'"
        logging.debug("Command1 \"%s\"" % command1)
        try: 
            raw_result1 = subprocess.check_output(command1,stderr=subprocess.STDOUT, shell=True)
            logging.debug("*****")
            logging.debug(raw_result1)
            logging.debug("*****")
        except subprocess.CalledProcessError:
            print("Error encountered attempting to run cksum: \"%s\"" % subprocess.STDOUT)
            print("!!!! You might not have a package to distribute or you removed it? !!!!")
            return "" # reflects failure to verify

        sys.stdout.write("done.\n")


        subPackageName = packageName.replace("ring_conf_pkg","")
        subPackageName = subPackageName.replace(".tar","")
        logging.debug("subpackagename = %s" % subPackageName)

        # host
        commandX = ""
        raw_resultX = ""
        raw_resultY = ""

        if (thisHostADDRstring):
            sys.stdout.write("Hostname...")

            #MAC = target
            MAC = thisHostADDRstring
            
            commandY = "sudo /opt/plexxi/bin/plexxiRemoteCommand " + MAChost + " \'/bin/hostname; ls " + subPackageName + "/hostname_" + MAC + "_conf.pup\'"                        
            logging.debug("CommandY \"%s\"" % commandY)
            try: 
                raw_resultY = subprocess.check_output(commandY,stderr=subprocess.STDOUT, shell=True)
                logging.debug("*****")
                logging.debug(raw_resultY)
                logging.debug("*****")
            except subprocess.CalledProcessError:
                print("Error encountered attempting to apply config hostname_%s_conf.pup: \"%s\"" % (MAC,subprocess.STDOUT))
                return "" # reflects failure to apply, not sure if we want to bail out here.            

            pos2 = 0
            # check for necessary puppet manifest
            if (raw_resultY):
                logging.debug(raw_resultY)
                pos2 = raw_resultY.find(MAC)                    

            #If we pass the checks above we apply the configuration
            # THIS!!! is where the action happens ladies and gentlemen, children of all ages!
            if (pos2 > 0):
                sys.stdout.write("applying hostname configuration to ")
                sys.stdout.write(thisHostADDRstring)
                sys.stdout.write("  ...")

                # seems a little sleep is required before autoSwitchAlias can be run
                commandH = "sudo /opt/plexxi/bin/plexxiRemoteCommand " + MAChost + " \'/bin/hostname; /usr/bin/puppet apply " + OUTPUTDIR + "/hostname_" + MAC + "_conf.pup; /opt/plexxi/bin/autoSwitchAlias\'"
                logging.debug("CommandH \"%s\"" % commandH)
                try: 
                    raw_resultH = subprocess.check_output(commandH,stderr=subprocess.STDOUT, shell=True)
                    logging.debug("*****")
                    logging.debug(raw_resultH)
                    logging.debug("*****")
                except subprocess.CalledProcessError:
                    print("Error encountered attempting to apply config hostname_%s_conf.pup: \"%s\"" % (MAC,subprocess.STDOUT))
                    return "" # reflects failure to apply, not sure if we want to bail out here.            

                sys.stdout.write("applying mgmt interface configuration to ")
                sys.stdout.write(thisHostADDRstring)
                sys.stdout.write("  ...")

                commandH = "sudo /opt/plexxi/bin/plexxiRemoteCommand " + MAChost + " \'/bin/hostname; /usr/bin/puppet apply " + OUTPUTDIR + "/addrname_" + MAC + "_conf.pup\'"
                logging.debug("CommandH \"%s\"" % commandH)
                try: 
                    raw_resultH = subprocess.check_output(commandH,stderr=subprocess.STDOUT, shell=True)
                    logging.debug("*****")
                    logging.debug(raw_resultH)
                    logging.debug("*****")
                except subprocess.CalledProcessError:
                    print("Error encountered attempting to apply config addrname_%s_conf.pup: \"%s\"" % (MAC,subprocess.STDOUT))
                    return "" # reflects failure to apply, not sure if we want to bail out here.            
        else:
            print("No Hosts found to configure")

        ## GENERAL aka HOST NON-SPECIFIC SECTION, though applied to target set only ###

        # NTP
        commandX = ""
        raw_resultX = ""
        sys.stdout.write("NTP...")

        if (NTPstring):
            sys.stdout.write("applying configuration to ...")
            sys.stdout.write(thisHostADDRstring)
            sys.stdout.write("  ...")


            commandX = "sudo /opt/plexxi/bin/plexxiRemoteCommand " + MAChost + " \'/bin/hostname; /usr/bin/puppet apply "+ OUTPUTDIR + "/" + "/ntp_conf.pup\'"
            logging.debug("CommandX \"%s\"" % commandX)
            try: 
                raw_resultX = subprocess.check_output(commandX,stderr=subprocess.STDOUT, shell=True)
                logging.debug("*****")
                logging.debug(raw_resultX)
                logging.debug("*****")
            except subprocess.CalledProcessError:
                print("Error encountered attempting to apply config ntp_conf.pup: \"%s\"" % subprocess.STDOUT)
                return "" # reflects failure to apply, not sure if we want to bail out here.            

            sys.stdout.write("done.\n")
        else:        
            sys.stdout.write("seems you are not altering NTP configuration...done.\n")


        # Timezone
        commandX = ""
        raw_resultX = ""
        sys.stdout.write("Time Zone...")

        if (TZstring):
            sys.stdout.write("applying configuration to ...")
            sys.stdout.write(thisHostADDRstring)
            sys.stdout.write("  ...")


            commandX = "sudo /opt/plexxi/bin/plexxiRemoteCommand " + MAChost + " \'/bin/hostname; /usr/bin/puppet apply " + OUTPUTDIR + "/tz_conf.pup\'"
            logging.debug("CommandX \"%s\"" % commandX)
            try: 
                raw_resultX = subprocess.check_output(commandX,stderr=subprocess.STDOUT, shell=True)
                logging.debug("*****")
                logging.debug(raw_resultX)
                logging.debug("*****")
            except subprocess.CalledProcessError:
                print("Error encountered attempting to apply config tz_conf.pup: \"%s\"" % subprocess.STDOUT)
                return "" # reflects failure to apply, not sure if we want to bail out here.

            sys.stdout.write("done.\n")
        else:        
            sys.stdout.write("seems you are not altering Timezone configuration...done.\n")

        # DNS
        commandX = ""
        raw_resultX = ""
        sys.stdout.write("DNS...")

        if (DNSstring):
            sys.stdout.write("applying configuration to ...")
            sys.stdout.write(thisHostADDRstring)
            sys.stdout.write("  ...")

            commandX = "sudo /opt/plexxi/bin/plexxiRemoteCommand " + MAChost + " \'/bin/hostname; /usr/bin/puppet apply " + OUTPUTDIR + "/dns_conf.pup\'"
            logging.debug("CommandX \"%s\"" % commandX)
            try: 
                raw_resultX = subprocess.check_output(commandX,stderr=subprocess.STDOUT, shell=True)
                logging.debug("*****")
                logging.debug(raw_resultX)
                logging.debug("*****")
            except subprocess.CalledProcessError:
                print("Error encountered attempting to apply config dns_conf.pup: \"%s\"" % subprocess.STDOUT)
                return "" # reflects failure to apply, not sure if we want to bail out here.            

            sys.stdout.write("done.\n")
        else:        
            sys.stdout.write("seems you are not altering DNS configuration...done.\n")

        # SNMP
        commandX = ""
        raw_resultX = ""
        sys.stdout.write("SNMP...")

        if (SNMPstring):
            sys.stdout.write("applying configuration to ...")
            sys.stdout.write(thisHostADDRstring)
            sys.stdout.write("  ...")



            commandX = "sudo /opt/plexxi/bin/plexxiRemoteCommand " + MAChost + " \'/bin/hostname; /usr/bin/puppet apply "+ OUTPUTDIR + "/snmp_conf.pup\'"
            logging.debug("CommandX \"%s\"" % commandX)
            try: 
                raw_resultX = subprocess.check_output(commandX,stderr=subprocess.STDOUT, shell=True)
                logging.debug("*****")
                logging.debug(raw_resultX)
                logging.debug("*****")
            except subprocess.CalledProcessError:
                print("Error encountered attempting to apply config snmp_conf.pup: \"%s\"" % subprocess.STDOUT)
                return "" # reflects failure to apply, not sure if we want to bail out here.            

            sys.stdout.write("done.\n")
        else:        
            sys.stdout.write("seems you are not altering SNMP configuration...done.\n")


        # Here we will configure the controller and immediately after contact control and permenatize any port configurations IF they exist.
            
        # CTRL
        commandX = ""
        raw_resultX = ""
        raw_resultY = ""
        sys.stdout.write("CTRL...")
        
        
        if (CTRLstring):
            sys.stdout.write("applying configuration to ...")
            sys.stdout.write(MAChost)
            sys.stdout.write("  ...")        
            
            raw_resultY = ""
            commandY = "sudo /opt/plexxi/bin/plexxiRemoteCommand " + MAChost + " \'/bin/hostname; /opt/plexxi/bin/setC3Controller " + CTRLstring + "\'"                        
            logging.debug("CommandX \"%s\"" % commandY)
            try: 
                raw_resultY = subprocess.check_output(commandY,stderr=subprocess.STDOUT, shell=True)
                logging.debug("*****")
                logging.debug(raw_resultY)
                logging.debug("*****")
            except subprocess.CalledProcessError:
                print("Error encountered attempting to set controller IP address: \"%s\"" % subprocess.STDOUT)
                return "" # reflects failure to apply, not sure if we want to bail out here.            

            sys.stdout.write("done.\n")
            CTRLstring = "" #Once we've set it, on one system, we do not want to set it again, or other issues may ensue

            
        else:        
            sys.stdout.write("Plexxi Control Server Address... will NOT be altered, probably because its the same as presently configured.\n")     
                        
        
    else:
        # we are here if host could not be confirmed to be the correct target
        if (pos1 < 0):
            print("Could not confirm appropriate system reached.")

    return "Success"

###########
# applyDataTargeted End
###########

###########
# applyDataCTRL
# --- Some things must be applied at CTRL (once available) for instance the portsup.dat file must be utilized there to make permenant any changes to port configuration due to portsup feature.
###########


def applyDataCTRL(CTRLstring,PORTSUPstring,OUTPUTDIR):


    # RECONCILE
    sys.stdout.write("RECONCILE...")

    if (PORTSUPstring):

        tmpres = scpRECONCILE(CTRLstring,OUTPUTDIR)
        if (tmpres):
            print("Success!")
        else:
            print("Error!")
        print("============================================================")

        tmpres = execRECONCILEatCTRL(CTRLstring)
        if (tmpres):
            print("Success!")
        else:
            print("Error!")
        print("============================================================")            

    else:        
        sys.stdout.write("Plexxi Port States... will NOT be altered.\n")     

                    
    return "Success"

###########
# applyDataCTRL End
###########

###########
# unlockXCVR
##########

def unlockXCVR():

    sys.stdout.write("Unlocking Transceivers...")

    raw_resultX = ""
    raw_resultY = ""

    commandX = "sudo /opt/plexxi/bin/pcp /opt/plexxi/PlexxiSupport/installTools/simplexxi/allowed_xcvrs.xml ring:/boot/allowed_xcvrs.xml"
    commandY = "sudo /opt/plexxi/bin/plexxiRemoteCommand ring \'/usr/bin/px-xcvr refresh;\'"
    
    logging.debug("CommandX \"%s\"" % commandX)
    try: 
        raw_resultX = subprocess.check_output(commandX,stderr=subprocess.STDOUT, shell=True)
        logging.debug("*****")
        logging.debug(raw_resultX)
        logging.debug("*****")
    except subprocess.CalledProcessError:
        print("Error encountered attempting to copy xcvr override: \"%s\"" % subprocess.STDOUT)
        return "" # reflects failure to apply, not sure if we want to bail out here.            

    logging.debug("CommandY \"%s\"" % commandY)
    try: 
        raw_resultY = subprocess.check_output(commandY,stderr=subprocess.STDOUT, shell=True)
        logging.debug("*****")
        logging.debug(raw_resultY)
        logging.debug("*****")
    except subprocess.CalledProcessError:
        print("Error encountered attempting to allow xcvr override: \"%s\"" % subprocess.STDOUT)
        return "" # reflects failure to apply, not sure if we want to bail out here.            

    sys.stdout.write("done.\n")
        
###########
# unlockXCVR End
###########

###########
# loopDetectON
##########

def loopDetectON():

    sys.stdout.write("Enabling Bridge Loop Detection Protections...")
    
    raw_resultY = ""

    commandY = "sudo /opt/plexxi/bin/cliLoopDetection enable ring"
    
    logging.debug("CommandY \"%s\"" % commandY)
    try: 
        raw_resultY = subprocess.check_output(commandY,stderr=subprocess.STDOUT, shell=True)
        logging.debug("*****")
        logging.debug(raw_resultY)
        logging.debug("*****")
    except subprocess.CalledProcessError:
        print("Error encountered attempting to enable bridge loop detection ring/fabric wide: \"%s\"" % subprocess.STDOUT)
        return "" # reflects failure to apply, not sure if we want to bail out here.            

    sys.stdout.write("done.\n")
        
###########
# loopDetectON End
###########


###########
# setPortEnableAll
##########

def setPortEnableAll(MAChost):

    raw_resultY = ""

    commandY = "sudo /opt/plexxi/bin/plexxiRemoteCommand " + MAChost + " \'/opt/plexxi/bin/setPortAdmin enable_all ; \'"
    
    logging.debug("CommandX \"%s\"" % commandY)
    try: 
        raw_resultY = subprocess.check_output(commandY,stderr=subprocess.STDOUT, shell=True)
        logging.debug("*****")
        logging.debug(raw_resultY)
        logging.debug("*****")
    except subprocess.CalledProcessError:
        print("Error encountered attempting to set bring ports to enabled state: \"%s\"" % subprocess.STDOUT)
        return "" # reflects failure to apply, not sure if we want to bail out here.            

    

    
###########
# setPortEnableAll End
###########


###########
# setPortSpeed
###########

## Assumes user of enable all for speed.

def setPortSpeed(MAChost,port,speed):

    raw_resultY = ""
    #    IF not enabled all
    #     commandY = "sudo /opt/plexxi/bin/plexxiRemoteCommand " + MAChost + " \'/bin/hostname; /opt/plexxi/bin/setPortAdmin " + port + " disabled; /opt/plexxi/bin/setPortAdmin " + port + " " + speed + "; /opt/plexxi/bin/setPortAdmin " + port + "enable; \'"

    commandY = "sudo /opt/plexxi/bin/plexxiRemoteCommand " + MAChost + " \'/opt/plexxi/bin/setPortAdmin " + port + " " + speed + "; \'"
    
    logging.debug("CommandX \"%s\"" % commandY)
    try: 
        raw_resultY = subprocess.check_output(commandY,stderr=subprocess.STDOUT, shell=True)
        logging.debug("*****")
        logging.debug(raw_resultY)
        logging.debug("*****")
    except subprocess.CalledProcessError:
        print("Error encountered attempting to set port speed: \"%s\"" % subprocess.STDOUT)
        return "" # reflects failure to apply, not sure if we want to bail out here.            

    logging.debug("%s set to %s... " % (port,speed))

    
###########
# setPortSpeed End
###########


###########
# bulkClearVlanAll
###########

def bulkClearVlanAll(MAChost,portList,vlanNATIVE):

    raw_resultY = ""

    # Form command !!!!! plexxiRemoteCommand is sensitive to inputs, do NOT add extra spaces. Test raw output on switch!!!!! see log
    commandY = "sudo /opt/plexxi/bin/plexxiRemoteCommand " + MAChost + " \'"
    portString = ""
    index = 0

    for port in portList:
        index = index + 1

        try:
            port = int(port.replace("xp",""))

            if (index < len(portList)):
                portString = portString + str(port) + ","
            else:
                portString = portString + str(port)
        except:
            logging.debug("Error in portList!")
            return ""

    vlanNATIVEstr = vlanNATIVE.split(" ")
        
    commandY = commandY + "/opt/plexxi/bin/manageVlans clear vlan " + portString  + ";/opt/plexxi/bin/manageVlans clear native vlan "
    commandY = commandY + "\'"
    
    logging.debug("CommandY \"%s\"" % commandY)
    try: 
        raw_resultY = subprocess.check_output(commandY,stderr=subprocess.STDOUT, shell=True)
        logging.debug("*****")
        logging.debug(raw_resultY)
        logging.debug("*****")
    except subprocess.CalledProcessError:
        print("Error encountered attempting to set port speed: \"%s\"" % subprocess.STDOUT)
        return "" # reflects failure to apply, not sure if we want to bail out here.            


    

###########
# bulkClearVlanAll End
###########

###########
# bulkSetVlanNativeAll
###########

def bulkSetVlanNativeAll(MAChost,portList,vlanNATIVE):

    raw_resultY = ""

    # Form command !!!!! plexxiRemoteCommand is sensitive to inputs, do NOT add extra spaces. Test raw output on switch!!!!! see log
    commandY = "sudo /opt/plexxi/bin/plexxiRemoteCommand " + MAChost + " \'"
    portString = ""
    index = 0

    for port in portList:
        index = index + 1

        try:
            port = int(port.replace("xp",""))

            if (index < len(portList)):
                portString = portString + str(port) + ","
            else:
                portString = portString + str(port)
        except:
            logging.debug("Error in portList!")
            return ""

    vlanNATIVEstr = vlanNATIVE.split(" ")
        
    commandY = commandY + "/opt/plexxi/bin/manageVlans add native vlan " + portString + " " + vlanNATIVEstr[1] + " ;"
    commandY = commandY + "\'"
    
    logging.debug("CommandY \"%s\"" % commandY)
    try: 
        raw_resultY = subprocess.check_output(commandY,stderr=subprocess.STDOUT, shell=True)
        logging.debug("*****")
        logging.debug(raw_resultY)
        logging.debug("*****")
    except subprocess.CalledProcessError:
        print("Error encountered attempting to set port speed: \"%s\"" % subprocess.STDOUT)
        return "" # reflects failure to apply, not sure if we want to bail out here.            


    

###########
# bulkSetVlanNativeAll End
###########


###########
# bulkSetVlanAll
###########

def bulkSetVlanAll(MAChost,portList,vlanIDs):

    raw_resultY = ""

    # Form command !!!!! plexxiRemoteCommand is sensitive to inputs, do NOT add extra spaces. Test raw output on switch!!!!! see log
    commandY = "sudo /opt/plexxi/bin/plexxiRemoteCommand " + MAChost + " \'"
    portString = ""
    index = 0
    for port in portList:
        index = index + 1

        try:
            port = int(port.replace("xp",""))

            if (index < len(portList)):
                portString = portString + str(port) + ","
            else:
                portString = portString + str(port)
        except:
            logging.debug("Error in portList!")
            return ""
        
    vlanString = ""
    index = 0
    for vlan in vlanIDs:
        index = index + 1
        if (index < len(vlanIDs)):
            vlanString = vlanString + vlan + ","
        else:
            vlanString = vlanString + vlan

            
    commandY = commandY + "/opt/plexxi/bin/manageVlans add vlan " + portString + " " + vlanString + " ;"
    commandY = commandY + "\'"
    
    logging.debug("CommandY \"%s\"" % commandY)
    try: 
        raw_resultY = subprocess.check_output(commandY,stderr=subprocess.STDOUT, shell=True)
        logging.debug("*****")
        logging.debug(raw_resultY)
        logging.debug("*****")
    except subprocess.CalledProcessError:
        print("Error encountered attempting to set port speed: \"%s\"" % subprocess.STDOUT)
        return "" # reflects failure to apply, not sure if we want to bail out here.            




    
###########
# bulkSetVlanAll End
###########


###########
# setPortSpeedBulk
###########

## Assumes user of enable all for speed.

def setPortSpeedBulk(MAChost,portList,speed):

    raw_resultY = ""

    # Form command !!!!! plexxiRemoteCommand is sensitive to inputs, do NOT add extra spaces. Test raw output on switch!!!!! see log
    commandY = "sudo /opt/plexxi/bin/plexxiRemoteCommand " + MAChost + " \'"
    for port in portList:
        commandY = commandY + "/opt/plexxi/bin/setPortAdmin " + port + " " + speed + ";"
    commandY = commandY + "\'"
    
    logging.debug("CommandX \"%s\"" % commandY)
    try: 
        raw_resultY = subprocess.check_output(commandY,stderr=subprocess.STDOUT, shell=True)
        logging.debug("*****")
        logging.debug(raw_resultY)
        logging.debug("*****")
    except subprocess.CalledProcessError:
        print("Error encountered attempting to set port speed: \"%s\"" % subprocess.STDOUT)
        return "" # reflects failure to apply, not sure if we want to bail out here.            

    logging.debug("%s set to %s... " % (port,speed))

    
###########
# setPortSpeedBulk End
###########


###########
# setPortSpeed
###########

## Does not Assume user enabled.

def setPortSpeedEnable(MAChost,port,speed):

    raw_resultY = ""
    #    IF not enabled all
    commandY = "sudo /opt/plexxi/bin/plexxiRemoteCommand " + MAChost + " \'/opt/plexxi/bin/setPortAdmin " + port + " " + speed + "; /opt/plexxi/bin/setPortAdmin " + port + "enable; \'"
    
    logging.debug("CommandX \"%s\"" % commandY)
    try: 
        raw_resultY = subprocess.check_output(commandY,stderr=subprocess.STDOUT, shell=True)
        logging.debug("*****")
        logging.debug(raw_resultY)
        logging.debug("*****")
    except subprocess.CalledProcessError:
        print("Error encountered attempting to set port speed: \"%s\"" % subprocess.STDOUT)
        return "" # reflects failure to apply, not sure if we want to bail out here.            

    logging.debug("%s set to %s... " % (port,speed))

    
###########
# setPortSpeedEnable End
###########


###########
# setPortSpeedDot
###########

## Does not Assume user enabled.

def setPortSpeedEnableDot(MAChost,port,speed):

    sys.stdout.write(".")
    sys.stdout.flush() # Otherwise its a whole bunch of dots at once, no fun.
    return setPortSpeedEnable(MAChost,port,speed)
 
    
###########
# setPortSpeedEnableDot End
###########


###########
# setPortSpeedDot (and prints dot)
###########

def setPortSpeedDot(MAChost,port,speed):

    sys.stdout.write(".")
    sys.stdout.flush() # Otherwise its a whole bunch of dots at once, no fun.
    return setPortSpeed(MAChost,port,speed)
    
###########
# setPortSpeedDot End
###########


###########
# verifyPortSpeed
###########

def verifyPortSpeed(MAChost,port,speed):

    raw_resultY = ""
    commandY = "sudo /opt/plexxi/bin/plexxiRemoteCommand " + MAChost + " \'/bin/hostname; /bin/px-shell -e \"show interface " + port + "\"\'"                        
    logging.debug("CommandX \"%s\"" % commandY)
    
    try: 
        raw_resultY = subprocess.check_output(commandY,stderr=subprocess.STDOUT, shell=True)
        logging.debug("*****")
        logging.debug(raw_resultY)
        logging.debug("*****")

        pos1 = raw_resultY.find("Link: ")
        pos2 = raw_resultY.find("Bandwidth: ")
        pos2 = pos2 + len("Bandwidth: ") + 3     # reach out past "Bandwidth" to capture speed
        result = raw_resultY[pos1:pos2]

        posLink = result.find("Link:")
        posLinkEnd = 0
        linkState = ""
        if (posLink >= 0):
            posLink = posLink + len("Link: ")
            posLinkEnd = posLink + 2   #length of "up", else doesn't matter its not up
            linkState = result[posLink:posLinkEnd]

        posOper = result.find("Oper:")
        posOperEnd = 0
        operState = ""
        if (posOper >= 0):
            posOper = posOper + len("Oper: ")
            posOperEnd = posOper + 2   #length of "up", else doesn't matter its not up
            operState = result[posOper:posOperEnd]
                               
        posSTP = result.find("STP:")
        posSTPEnd = 0
        STPState = ""
        if (posSTP >= 0):
            posSTP = posSTP + len("STP: ")
            posSTPEnd = posSTP + 7   #length of "forward", else doesn't matter its not up
            STPState = result[posSTP:posSTPEnd]

        linkStatus = result

        if ((linkState == "up") and (operState == "up") and (STPState == "forward")):
            return("UP")
        else:
            return("DOWN")
        
        #print(result)
        #return(result)
        
    except subprocess.CalledProcessError:
        print("Error encountered attempting to set bring ports to enabled state: \"%s\"" % subprocess.STDOUT)
        return "" # reflects failure to apply, not sure if we want to bail out here.            

    
###########
# verifyPortSpeed End
###########


###########
# verifyPortSpeedBulk
###########

def verifyPortSpeedBulk(MAChost,portMAP,speed):

    state = "unknown"
    state_result = []
    raw_resultY = ""
    commandY = "sudo /opt/plexxi/bin/plexxiRemoteCommand " + MAChost + " \'/bin/hostname; /opt/plexxi/bin/showPort\'"                        
    logging.debug("CommandX \"%s\"" % commandY)
    
    try: 
        raw_resultY = subprocess.check_output(commandY,stderr=subprocess.STDOUT, shell=True)
        logging.debug("*****")
        logging.debug(raw_resultY)
        logging.debug("*****")

        for raw_line in raw_resultY.split("\n"):
            state = "unknown"
            interface_pos = raw_line.find("xp")
            if (interface_pos >= 0):
                interface = raw_line[interface_pos:(interface_pos + 4)]
                up_pos = raw_line.find("up")
                down_pos = raw_line.find("down")
                if ((up_pos > 0) and (down_pos < 0)):
                    state = "up"
                else:
                    state = "down"
                state_result.append([interface.strip(),state])

    except subprocess.CalledProcessError:
        print("Error encountered attempting to attempting to verify ports: \"%s\"" % subprocess.STDOUT)
        return "" # reflects failure to apply, not sure if we want to bail out here.            

    return state_result
    
###########
# verifyPortSpeedBulk End
###########


###########
# verifyFinalTargeted
###########


def verifyFinalTargeted(packageName,destDir,CTRLstring,PORTSUPstring,NTPstring,DNSstring,SNMPstring,TZstring,HostADDRstring,targetList):

    overAllSuccess = True
    
    reachableNOW = confirmContactTargets(targetList)

    if (len(reachableNOW) < len(targetList)):
        print("NOT all targets reachable!! Something must have changed in fabric. I can make a best effort......")
        
    print("We will now apply configurations, by type to these hosts %s:" % targetList)
            
    # How we run through targetList for ALL items which were configured during input phase
    
    ## This will need some serious review as what is the "MAC" is open to interpetation, for our purposes, right now, MAC is "MacBase", lowest number that could be owned by host.
    ### some hardware models may use this as the mgmt MAC, others will use the opposite end of the range (+128)



    
    # Aqcuire MAC from /etc/platinfo.json and confirm we are targetting the correct hose before execution.    
    for target in targetList:
        thisHostADDRstring = []

        MAChost = target.replace(":","")   # nix colons for ring command to specific member # we want this set unconditionally! 

        # Lets check some things, are we contacting the host we expect?
        # Is the matching host config file there for hostname AND interfaces?                
        print("Checking we are on correct host")
        commandX = "sudo /opt/plexxi/bin/plexxiRemoteCommand " + MAChost + " \'/bin/cat /etc/platinfo.json\'"                        
        logging.debug("CommandX \"%s\"" % commandX)
        try: 
            raw_resultX = subprocess.check_output(commandX,stderr=subprocess.STDOUT, shell=True)
            logging.debug("*****")
            logging.debug(raw_resultX)
            logging.debug("*****")
        except subprocess.CalledProcessError:
            print("Error encountered attempting to inspect /etc/platinfo.json for confirmation of\"%s\"" % (MAChost,subprocess.STDOUT))
            return "" # reflects failure to apply, not sure if we want to bail out here.            

        pos1 = 0
        # check for which host we're on
        if (raw_resultX):
            logging.debug(raw_resultX)                    
            pos1 = raw_resultX.find(target) # It will have :'s colons so use target rather than MAChost
            if (pos1 < 0):
                print("Error, somehow we are contacting host by MAC %s and cannot find Base MAC in output: %s! Abort settings for this mac for safety sake." % (target,raw_resultX))
                return ""

        # If we have a HostADDRstring, we should only hit each element once
        for hostset in HostADDRstring:        
            logging.debug("hostset[0] %s, MAChost %s" % (hostset[0],MAChost))            
            tempMAC = hostset[0].replace(":","")
            if (MAChost == tempMAC):
                thisHostADDRstring.append(hostset)
                # else we don't add to list

        if (thisHostADDRstring):
            #We have something so proceed.
            raw_resultV = ""
            commandV = "sudo /opt/plexxi/bin/plexxiRemoteCommand " + MAChost + " /bin/hostname;"
            logging.debug("commandV \"%s\"" % commandV)

            try: 
                raw_resultV = subprocess.check_output(commandV,stderr=subprocess.STDOUT, shell=True)
                logging.debug("*****")
                logging.debug(raw_resultV)
                logging.debug("*****")
            except subprocess.CalledProcessError:
                print("Error encountered running command %s : %s" % (commandV,subprocess.STDOUT))
                #return "" # reflects failure to apply, not sure if we want to bail out here.            
        
            if (raw_resultV.find(thisHostADDRstring[1]) < 0):
                print("!!! Unable to verify %s on %s !!!" % ('Hostname via hostname',MAChost))
                overallSuccess = ""
            else:
                print("success!")

        if (thisHostADDRstring):
            #We have something so proceed.
            raw_resultV = ""
            commandV = "sudo /opt/plexxi/bin/plexxiRemoteCommand " + MAChost + " /opt/plexxi/bin/censusInfo;"
            logging.debug("commandV \"%s\"" % commandV)

            try: 
                raw_resultV = subprocess.check_output(commandV,stderr=subprocess.STDOUT, shell=True)
                logging.debug("*****")
                logging.debug(raw_resultV)
                logging.debug("*****")
            except subprocess.CalledProcessError:
                print("Error encountered running command %s : %s" % (commandV,subprocess.STDOUT))
                #return "" # reflects failure to apply, not sure if we want to bail out here.            
        
            if (raw_resultV.find(thisHostADDRstring[1]) < 0):
                print("!!! Unable to verify %s on %s !!!" % ('Hostname via hostname',MAChost))
                overallSuccess = ""
            else:
                print("success!")

        if (CTRLstring):
            #We have something so proceed.
            raw_resultV = ""
            commandV = "sudo /opt/plexxi/bin/plexxiRemoteCommand " + MAChost + " /opt/plexxi/bin/censusInfo;"
            logging.debug("commandV \"%s\"" % commandV)

            try: 
                raw_resultV = subprocess.check_output(commandV,stderr=subprocess.STDOUT, shell=True)
                logging.debug("*****")
                logging.debug(raw_resultV)
                logging.debug("*****")
            except subprocess.CalledProcessError:
                print("Error encountered running command %s : %s" % (commandV,subprocess.STDOUT))
                #return "" # reflects failure to apply, not sure if we want to bail out here.            
        
            if (raw_resultV.find(CTRLstring) < 0):
                print("!!! Unable to verify %s on %s !!!" % ('Hostname via hostname',MAChost))
                overallSuccess = ""
            else:
                print("success!")

        for ntpsubstring in NTPstring:
            if (ntpsubstring):
                #We have something so proceed.
                raw_resultV = ""
                commandV = "sudo /opt/plexxi/bin/plexxiRemoteCommand " + MAChost + " /usr/bin/ntpq -pn;"
                logging.debug("commandV \"%s\"" % commandV)

                try: 
                    raw_resultV = subprocess.check_output(commandV,stderr=subprocess.STDOUT, shell=True)
                    logging.debug("*****")
                    logging.debug(raw_resultV)
                    logging.debug("*****")
                except subprocess.CalledProcessError:
                    print("Error encountered running command %s : %s" % (commandV,subprocess.STDOUT))
                    #return "" # reflects failure to apply, not sure if we want to bail out here.            

                if (raw_resultV.find(ntpsubstring) < 0):
                    print("!!! Unable to verify %s on %s !!!" % ('ntp config via ntpq',MAChost))
                    overallSuccess = ""
                else:
                    print("success!")



 
        sys.stdout.write("done.\n")
    else:
        if (pos1 < 0):
            print("Could not confirm appropriate system reached.")

    if (overAllSuccess):
        return "Success"
    else:
        return ""


###########
# verifyFinalTargeted End
###########



###########
# collectCensusData 
###########

def collectCensusData():

    command1 = "/opt/plexxi/bin/censusInfo"
    try: 
        raw_result1 = subprocess.check_output(command1,stderr=subprocess.STDOUT, shell=True)
        logging.debug("*****")
        logging.debug(raw_result1)
        logging.debug("*****")
    except subprocess.CalledProcessError:
        print("Error encountered attempting to run censusInfo: \"%s\"" % subprocess.STDOUT)
        print("!!!! cannot proceed without censusInfo !!!!")
        sys.exit(1)

    ############ Controller where are thou? ############
    tempCTRLip = ""
    posC1 = 0
    posC2 = 0
    searchCTRLstr = "C3Controller IP:"
    posC1 = raw_result1.find(searchCTRLstr)
    if (posC1 > 0):
        posC1 = (posC1 + len(searchCTRLstr))   # advance to actual IP
        posC2 = raw_result1.find("Ring",posC1)
    if ((posC1 > 0) and (posC2 > 0) and (posC2 > posC1)):
        tempCTRLip = raw_result1[posC1:posC2]
        tempCTRLip = tempCTRLip.strip()
        print("Controller IP address detected: \"%s\"" % tempCTRLip)

    logging.debug("regardless: %d,  %d Controller IP address detected: %s" % (posC1,posC2,tempCTRLip))

    ############ Now find hosts ################
    
    pos1 = raw_result1.find("Active List:")
    pos2 = raw_result1.find("Black List:")
    ring_members = raw_result1[pos1:pos2]
    
    pos1 = 0
    pos2 = 0
    pos1 = ring_members.find("{")
    pos2 = len(ring_members)
    ring_members = ring_members[pos1:pos2]
    ring_members = ring_members.strip()


    ring_members_lines = ring_members.split("\n")
    ring_members = []
    for member_line in ring_members_lines:
        templine = escape_ansi(member_line)
        templine = templine.replace("{","")
        templine = templine.replace("}","")
        templine = templine.replace("\"","")
        templine = templine.split(",")    
        ring_members.append(templine)
        
    return [ring_members,tempCTRLip]


###########
# collectCensusData End
###########

###########
# makeCache
###########

def makeCache(backupDir):

    lastDir = findLastDIR(backupDir)
    logging.debug("LAST DIR = %s" % lastDir)

    OUTPUTDIR = mkOUTDIR(backupDir)
    logging.debug("OUTPUTDIR = %s" % OUTPUTDIR)

    return [lastDir,OUTPUTDIR]

###########
# makeCache End
###########


###########
# wizardDrive
###########


def wizardDrive(theChosenOnes,work2do,OUTPUTDIR,CTRLstring,PORTSUPstring,NTPstring,DNSstring,SNMPstring,TZstring,HostADDRstring,masterPORTSUPinfo,hostCensusData,theTargets):
                
    # Collect for ONLY selected targets (which could be all, if it were set so, it would now be populated with a complete list from censusData)
    packageName = ""
    collected = ""
    collected = collectData(theChosenOnes,work2do,OUTPUTDIR,CTRLstring,PORTSUPstring,NTPstring,DNSstring,SNMPstring,TZstring,HostADDRstring,masterPORTSUPinfo,hostCensusData,theTargets)

    if (collected):
        work2do = collected[0] 
        OUTPUTDIR = collected[1]
        CTRLstring = collected[2]
        PORTSUPstring = collected[3]
        NTPstring = collected[4]
        DNSstring = collected[5]
        SNMPstring = collected[6]
        TZstring = collected[7]
        HostADDRstring = collected[8]
        masterPORTSUPinfo = collected[9]
        #  censusData = collected[9], this should not have changed, so we don't need to mess ourselves up here

        packageName = packageData(OUTPUTDIR)    #collected[1] should be OUTPUTDIR, see above

    ####### Targeted ONLY Targeted ONLY DISRIBUTE / VERIFY / CONFIGURE  ############

    distStatus = distributeDataTargeted(packageName,destDir, theTargets)  # we get distStatus, but due to error "free" pcp I think its false positive
    if (distStatus != "success"):
        print("WARNING this may not have made it to all targets!")

    # Verify delivery by checksum and if good, apply configuration.
    if (packageName):
        verifiedDist = verifyDistTargeted(packageName,destDir, theTargets)
    else:
        print("No valid package found.")
        return ""

    if (verifiedDist == "True"):
        print("Packages verified as good.")
        # If the controller ip address is the same as originally harvested, save us the labor / risk and leave alone, UNLESS we configured ports, then we want to make sure we set it again, and contact CTRL to synchronize configuration.
        if ((PORTSUPstring != "") and (CTRLstring == "")):
            CTRLstring == CTRLstringFOUND     #sometimes portsup is configured but CTRL isn't specifically configured, we'll use harvested value in that case.

        if ((CTRLstring == CTRLstringFOUND) and (PORTSUPstring == "")):
            CTRLstring = ""
            logging.debug("New Controller Address is the same as that detected originally, this value will be ignored.\nIf you desire to force the controller configuration, alter the IP to something else (0.0.0.0 for example) and then reconfigure to desired address.") 

        applyStatus = applyDataTargeted(packageName,destDir,CTRLstring,PORTSUPstring,NTPstring,DNSstring,SNMPstring,TZstring,HostADDRstring,theTargets)

        applyCTRLdataPrompt = "---- Reconcile with controller  ---- \n\tReconciliation is necessary to make permanent the following items configured in px-setup:\nAny port discovery based configuration \"portsup\".\nHostname of switches at controller matches switch configuration. \"hostaddr\|mgmt\""
        if doThis(applyCTRLdataPrompt):
            applyStatusCTRL = applyDataCTRL(CTRLstring,PORTSUPstring,OUTPUTDIR)        

            if (applyStatus and applyStatusCTRL):
                print("Successfully applied to switches and Controller")            
        else:
            print("NOT applying port configuration to Control. This should be done later when Controller is up and running via \"px-setup-finalize\"")

    else:
        print("Possible package transfer problem.")
        applyStatus = -1 # because we didn't apply it.

    return collected

###########
# wizardDrive END
###########




###########
# menuDrive
###########


def menuDrive(theChosenOnes,work2do,OUTPUTDIR,CTRLstring,PORTSUPstring,NTPstring,DNSstring,SNMPstring,TZstring,HostADDRstring,masterPORTSUPinfo,hostCensusData,theTargets):

    
    ### Menu Function Section

    def quitDisYo():        
        return("probablyOK")
    
    def wizard(theChosenOnes,work2do,OUTPUTDIR,CTRLstring,PORTSUPstring,NTPstring,DNSstring,SNMPstring,TZstring,HostADDRstring,masterPORTSUPinfo,hostCensusData,theTargets):
        collected = wizardDrive(theChosenOnes,work2do,OUTPUTDIR,CTRLstring,PORTSUPstring,NTPstring,DNSstring,SNMPstring,TZstring,HostADDRstring,masterPORTSUPinfo,hostCensusData,theTargets)
        if (collected):
            work2do = collected[0] 
            OUTPUTDIR = collected[1]
            CTRLstring = collected[2]
            PORTSUPstring = collected[3]
            NTPstring = collected[4]
            DNSstring = collected[5]
            SNMPstring = collected[6]
            TZstring = collected[7]
            HostADDRstring = collected[8]
            masterPORTSUPinfo = collected[9]
            #  censusData = collected[9], this should not have changed, so we don't need to mess ourselves up here

            packageName = packageData(OUTPUTDIR)    #collected[1] should be OUTPUTDIR, see above
            return packageName

    def acquire(theChosenOnes,work2do,OUTPUTDIR,CTRLstring,PORTSUPstring,NTPstring,DNSstring,SNMPstring,TZstring,HostADDRstring,masterPORTSUPinfo,hostCensusData,theTargets):
        collected = collectData(theChosenOnes,work2do,OUTPUTDIR,CTRLstring,PORTSUPstring,NTPstring,DNSstring,SNMPstring,TZstring,HostADDRstring,masterPORTSUPinfo,hostCensusData,theTargets)
        
        if (collected):
            work2do = collected[0] 
            OUTPUTDIR = collected[1]
            CTRLstring = collected[2]
            PORTSUPstring = collected[3]
            NTPstring = collected[4]
            DNSstring = collected[5]
            SNMPstring = collected[6]
            TZstring = collected[7]
            HostADDRstring = collected[8]
            masterPORTSUPinfo = collected[9]
            #  censusData = collected[9], this should not have changed, so we don't need to mess ourselves up here

            packageName = packageData(OUTPUTDIR)    #collected[1] should be OUTPUTDIR, see above


            # Copy round ring/fabric for redundancy and sychronization
            distStatus = distributeDataTargeted(packageName,destDir, theTargets)  # we get distStatus, but due to error "free" pcp I think its false positive
            if (distStatus != "success"):
                print("WARNING this may not have made it to all targets!")

            anyK2C()
                    
            return packageName

    def saveConf(theChosenOnes,work2do,OUTPUTDIR,CTRLstring,PORTSUPstring,NTPstring,DNSstring,SNMPstring,TZstring,HostADDRstring,masterPORTSUPinfo,hostCensusData,theTargets):

        print("masterPORTSUPinfo %s,OUTPUTDIR %s" % (masterPORTSUPinfo,OUTPUTDIR))
        
        tmpres = writePORTSUP(masterPORTSUPinfo,OUTPUTDIR)
        
        if (tmpres):
            sys.stdout.write("Generated portsup data cache.")
            logging.debug("Generated portsup data cache.")

        # We should be good and confirmed at this point, let write to puppet files (which will be transported to switch and applied seperately    

        
        sys.stdout.write("\nAs you command!\nCaching switch data entries for later...")
        logging.debug("Caching switch data entries for later...")

        if(writeSWITCH([CTRLstring,PORTSUPstring,NTPstring,DNSstring,SNMPstring,TZstring,HostADDRstring,masterPORTSUPinfo],OUTPUTDIR)):
            sys.stdout.write("Generated Switch data cache.")
            logging.debug("Generated Switch data cache.")
            # errors generated from function itself.

        sys.stdout.write("\nGenerating puppet files which are host specific...")
        files = ""
        if HostADDRstring:
            sys.stdout.write("Generating hostname_MAC#_conf.pup & addrname_MAC#_conf.pup for each MAC#...")
            files = writeHOSTADDR(HostADDRstring,OUTPUTDIR)    
        sys.stdout.write("done.\n")
        if files:
            for file in files:
                print("%s AND %s generated.\n" % (file[0],file[1]))

        sys.stdout.write("Generating puppet files which are universal...")
        # CTRL and PORTSUP config works differently than puppet driven and so this is a no-op but included for consistency
        if CTRLstring:
            sys.stdout.write("Generating CTRL config...pass trough...")
            writeCTRL(CTRLstring,OUTPUTDIR)
        #if PORTSUPstring:
        #    sys.stdout.write("This happens during collection.")
        # These really write files.
        if NTPstring:
            sys.stdout.write("Generating ntp_conf.pup...")
            writeNTP(NTPstring,OUTPUTDIR)
        if DNSstring:
            sys.stdout.write("Generating dns_conf.pup...")
            writeDNS(DNSstring,OUTPUTDIR)
        if SNMPstring:
            sys.stdout.write("Generating snmp_conf.pup...")
            writeSNMP(SNMPstring,OUTPUTDIR)
        if TZstring:
            sys.stdout.write("Generating tz_conf.pup...")
            writeTZ(TZstring,OUTPUTDIR)
        sys.stdout.write("done.\n")

        logging.debug("See output directory:  %s" % OUTPUTDIR)

        packageName = ""
        lastPackage = OUTPUTDIR + "/ring_conf_pkg.tar"
        if (os.path.exists(lastPackage)):
            packageName = lastPackage
            logging.debug("PackageName from menu drive acquisition = %s" % packageName)
        
        # Copy round ring/fabric for redundancy and sychronization
        distStatus = distributeDataTargeted(packageName,destDir, theTargets)  # we get distStatus, but due to error "free" pcp I think its false positive
        if (distStatus != "success"):
            print("WARNING this may not have made it to all targets!")

        anyK2C()
        
        return (work2do,OUTPUTDIR,CTRLstring,PORTSUPstring,NTPstring,DNSstring,SNMPstring,TZstring,HostADDRstring,masterPORTSUPinfo,censusData)


        
    def reconcile(theChosenOnes,work2do,OUTPUTDIR,CTRLstring,PORTSUPstring,NTPstring,DNSstring,SNMPstring,TZstring,HostADDRstring,masterPORTSUPinfo,hostCensusData,theTargets):
        applyCTRLdataPrompt = "---- Reconcile with controller  ---- \n\tReconciliation is necessary to make permanent the following items configured in px-setup:\nAny port discovery based configuration \"portsup\".\nHostname of switches at controller matches switch configuration. \"hostaddr\|mgmt\""
        applyStatusCTRL = ""
        if doThis(applyCTRLdataPrompt):
            applyStatusCTRL = applyDataCTRL(CTRLstring,PORTSUPstring,OUTPUTDIR)        
        
        if (applyStatusCTRL):
            print("Successfully reconciled Switch Configuration and Controller")
            anyK2C()

    def confSwitch(theChosenOnes,work2do,OUTPUTDIR,CTRLstring,PORTSUPstring,NTPstring,DNSstring,SNMPstring,TZstring,HostADDRstring,masterPORTSUPinfo,hostCensusData,theTargets):
        print("Bringing configuration to switches and applying...")

        packageName = ""
        lastPackage = OUTPUTDIR + "/ring_conf_pkg.tar"
        if (os.path.exists(lastPackage)):
            packageName = lastPackage
            logging.debug("PackageName from menu drive acquisition = %s" % packageName)

        
        # Verify delivery by checksum and if good, apply configuration.
        if (packageName):
            distStatus = distributeDataTargeted(packageName,destDir, theTargets)  # we get distStatus, but due to error "free" pcp I think its false positive
            if (distStatus != "success"):
                print("WARNING this may not have made it to all targets!")
            verifiedDist = verifyDistTargeted(packageName,destDir, theTargets)
        else:
            print("No valid package found. %s " % packageName)
            logging.debug("No valid package found. %s " % packageName)
            return ""

        if (verifiedDist == "True"):
            print("Packages verified as good.")
            # If the controller ip address is the same as originally harvested, save us the labor / risk and leave alone, UNLESS we configured ports, then we want to make sure we set it again, and contact CTRL to synchronize configuration.
            if ((PORTSUPstring != "") and (CTRLstring == "")):
                CTRLstring == CTRLstringFOUND     #sometimes portsup is configured but CTRL isn't specifically configured, we'll use harvested value in that case.

            if ((CTRLstring == CTRLstringFOUND) and (PORTSUPstring == "")):
                CTRLstring = ""
                logging.debug("New Controller Address is the same as that detected originally, this value will be ignored.\nIf you desire to force the controller configuration, alter the IP to something else (0.0.0.0 for example) and then reconfigure to desired address.") 

            applyStatus = applyDataTargeted(packageName,destDir,CTRLstring,PORTSUPstring,NTPstring,DNSstring,SNMPstring,TZstring,HostADDRstring,theTargets)

        else:
            print("Possible package transfer problem.")
            applyStatus = -1 # because we didn't apply it.

        if (applyStatus and applyStatusCTRL):
            print("Successfully applied to switches and Controller")            
            anyK2C()

            ####### END Targeted ONLY Targeted ONLY DISTRIBUTE / VERIFY / CONFIGURE  ##########




    ### End Menu Function Section ###

    menuOptions = { "w": wizard,"a":acquire,"b":confSwitch,"c":reconcile,"s":saveConf,"q":quitDisYo }

    mainMenu = "\t Please chose an action from the following options.\n\n\
    \tw:\tWizard (legacy mode menu options a,b,c sequentially)\n\
    \ta:\tAcquire configuration (Provided by user entry and discovery)\n\
    \tb:\tBring up configuration (Parameters to Switch and Apply at Switch)\n\
    \tc:\tComplete configuration (Reconcile Switch Configuration to Controller)\n\
    \ts:\tSave configuration to local cache (writes puppet files, switch and portsup data)\n\
    \tq:\tQuit."
    
    answer="introduction"
    prompt="==>"
    result = "unknown"
    
    while (answer != "q"):
        
        printALL(CTRLstring,PORTSUPstring,NTPstring,DNSstring,SNMPstring,TZstring,HostADDRstring,masterPORTSUPinfo)
        print("\n\n\n\n")
        topLongBORDER()        
        print(mainMenu)    
        topLongBORDER()
        answer = ""
        answer = raw_input(prompt)
        logging.debug("menu answer is %s" % answer)
        try:
            if (answer == "q"):
                result = menuOptions[answer]()
                return result
            else:
                result = menuOptions[answer](theChosenOnes,work2do,OUTPUTDIR,CTRLstring,PORTSUPstring,NTPstring,DNSstring,SNMPstring,TZstring,HostADDRstring,masterPORTSUPinfo,hostCensusData,theTargets)            
        except KeyError:
            print("unknown option \"%s\"" % answer)
            anyK2C()
        except Exception as e:
            print('unknown exception "%s"' % e)
            anyK2C()

        # re-Read cache    # just in case things change. seems intensive but less error prone.

        localSWITCHinfo = []
        
        cachedData = readCache(localSWITCHinfo,masterPORTSUPinfo,OUTPUTDIR)
        masterPORTSUPinfo = cachedData[1]
        #localSWITCHinfo should look like this: [CTRLstring,PORTSUPstring,NTPstring,DNSstring,SNMPstring,TZstring,HostADDRstring]
        localSWITCHinfo = cachedData[0]

        if (len(localSWITCHinfo) > 0):
            CTRLstring = localSWITCHinfo[0]
            PORTSUPstring = localSWITCHinfo[1]
            NTPstring = localSWITCHinfo[2]
            DNSstring = localSWITCHinfo[3]
            SNMPstring =  localSWITCHinfo[4]
            TZstring = localSWITCHinfo[5]
            HostADDRstring = localSWITCHinfo[6]
            packageName = packageData(OUTPUTDIR)    #REWRITE contents of new backdirectory if data is discovered so packageName is up to date.
        else:
            logging.debug("No valid cached data found.")
            
        #masterPORTSUPinfo = readPORTSUP(masterPORTSUPinfo, lastDir)
        if (len(masterPORTSUPinfo) == 3):
            logging.debug("Sucessfully loaded cached portsup discover data: %s" % masterPORTSUPinfo)
        else:
            logging.debug("Nothing valid loaded from cached portsup discover data.")

    
                    
    # End while of menuDrive
            
    return(result)


        
###########
# menuDrive END
###########



###########
# MAIN loop - Bukkiah Golden, blame that guy 
###########

#parse the command line args

# while defaults are provided in argparse, if there is no arg, no default seems to be applied so define here.


parser = argparse.ArgumentParser(description='Simplexxi, the Plexxi switch fabric configuration engine.')
parser.add_argument('-c','--configure', dest='theChosenOnes', default="ntp,tz,ctrl,dns,snmp,hostaddr", help='List of things to configure. Choices: ntp,tz,ctrl,dns,snmp,hostaddr,portsup,all Example: simplexxi -c ntp,tz')
parser.add_argument('-t','--target','--targets', dest='theTargets', default="all" , help='List of targets to configure. Specified by MAC address. Example: simplexxi -c all -t 01:02:03:f4:05:ab')
parser.add_argument('-o','--override', dest='theOverrides', default="all" , help='List of overrides, like override \'cache\'. Example: simplexxi -o cache')
args = parser.parse_args()

# Override options
theOverrides = args.theOverrides
# Override cache means do NOT use cached entries found in backup directories.
if (theOverrides.find("cache") >= 0):
    copyForward = False
else:
    copyForward = True

# Override cache means do NOT use cached entries found in backup directories.
if (theOverrides.find("xcvr") >= 0):
    print("Transceiver support override enabled. DO NOT DO THIS WITHOUT SUPPORT DIRECTION! Danger!!")
    # Proactively ensure all tranceivers xcvrs are unlocked.    
    unlockXCVR()
    print("Completed. Exiting.")
    sys.exit(1)
    

    
theChosenOnes  = args.theChosenOnes
if (theChosenOnes.find("all") >= 0):
    print("Configure All Config Items")
else:
    print("Configure Individual Config Items")

theTargets  = args.theTargets
if (theTargets.find("all") >= 0):
    print("Configure All Targets")
    theTargets = "all"        
else:
    theTargets = validTargets(theTargets)  #This will validate and return as a list, otherwise we'll quit out early if target MACs are bad.
    print("Configure Select Targets %s" % theTargets)
    # Because we are relying no user provided MACs, and if we haven't quit out in above validTargets() we'll also reach out to those MACs and save the user wasted time if they were valid but not present.
    confirmedTargets = confirmContactTargets(theTargets)
    if (len(confirmedTargets) < len(theTargets)):
        print("Could NOT confirm all targets presence on fabric, don't want to waste your time, quitting. Hint: Execute without specific MACs and I will choose all detectable MACs for you... if you wish it so. I cannot configure what is not there however... even if you wish it.")
        sys.exit(1)
                      
logging.debug("args.theTarget %s" % args.theTargets)
logging.debug("theTarget %s" % theTargets)

homeDir = os.environ['HOME']
if (homeDir.find("root") >= 0):
    backupDir = homeDir + "/px-setup-backups"
else:
    backupDir = "/root/px-setup-backups"
    
work2do = "YES!"
PORTSUPstring = ""
masterPORTSUPinfo = []
masterSWITCHinfo = []
NTPstring = ""
DNSstring = ""
SNMPstring = ""
TZstring = ""
HostADDRstring = ""
CTRLstring = ""
CTRLstringFOUND = ""
OUTPUTDIR = ""
applyStatus = ""
applyStatusCTRL = ""

destDir = "/tmp"
packageName = ""

####### Make a cache ######

cacheData = makeCache(backupDir)
lastDir = cacheData[0]
OUTPUTDIR = cacheData[1]


# Proactively and protectively turn on loop detection ring/fabric wide so we don't incidentally cause any loops during any phase, especially portsup discovery

loopDetectON()

# Possibly read a cache, default is read cache, but can be override by argument

if (copyForward):
    if (lastDir.find(backupDir) >= 0):
        cpALL(lastDir,OUTPUTDIR)
    else:
        result = lastDir.find(backupDir)
        logging.debug("Nothing to copy forward, starting from scratch: lastDir \"%s\" result: \"%s\"" % (lastDir,result))
        print("Nothing to copy forward, starting from scratch.")

    # find last package
    lastPackage = lastDir + "/ring_conf_pkg.tar"
    if (os.path.exists(lastPackage)):
        packageName = lastPackage
        logging.debug("PackageName from cache = %s" % packageName)

    # find last package
    lastPackage = lastDir + "/ring_conf_pkg.tar"
    if (os.path.exists(lastPackage)):
        packageName = lastPackage
        logging.debug("PackageName from cache = %s" % packageName)

    
    if (lastDir):
        cachedData = readCache(masterSWITCHinfo,masterPORTSUPinfo,lastDir)
        masterPORTSUPinfo = cachedData[1]
        #masterSWITCHinfo should look like this: [CTRLstring,PORTSUPstring,NTPstring,DNSstring,SNMPstring,TZstring,HostADDRstring]
        masterSWITCHinfo = cachedData[0]

        if (len(masterSWITCHinfo) > 0):
            CTRLstring = masterSWITCHinfo[0]
            PORTSUPstring = masterSWITCHinfo[1]
            NTPstring = masterSWITCHinfo[2]
            DNSstring = masterSWITCHinfo[3]
            SNMPstring =  masterSWITCHinfo[4]
            TZstring = masterSWITCHinfo[5]
            HostADDRstring = masterSWITCHinfo[6]
            packageName = packageData(OUTPUTDIR)    #REWRITE contents of new backdirectory if data is discovered so packageName is up to date.
        else:
            logging.debug("No valid cached data found.")
            
        #masterPORTSUPinfo = readPORTSUP(masterPORTSUPinfo, lastDir)
        if (len(masterPORTSUPinfo) == 3):
            logging.debug("Sucessfully loaded cached portsup discover data: %s" % masterPORTSUPinfo)
        else:
            logging.debug("Nothing valid loaded from cached portsup discover data.")
                

##### End cache 

##### Initial fluid state assignment ##########

censusData = collectCensusData()                       
 
hostCensusData = censusData[0]
CTRLstringFOUND = censusData[1]
CTRLstring = CTRLstringFOUND    


if (theTargets == "all"):
    theTargets = []
    # populate with censusData (since we just got it, should be no need to validate as its not user input)
    for host in censusData[0]:
        theTargets.append(host[0])

##### Initial fluid state assignment END ##########xs
        
# Hey there, how are you, we are this, enjoy.
printWelcome()

anyK2C()

######################
#  Menu Control loop #
######################

coolRunning = True

while (coolRunning):
    try:
        mainExitCondition = "unknown"
        mainExitCondition = menuDrive(theChosenOnes,work2do,OUTPUTDIR,CTRLstring,PORTSUPstring,NTPstring,DNSstring,SNMPstring,TZstring,HostADDRstring,masterPORTSUPinfo,hostCensusData,theTargets)
    except KeyboardInterrupt:
        # Catch CTRL-C
        emergency_exit_warning = "\n\n!!! -- CTRL-C ? -- !!!\n   Exit all the way?"
        if doThis(emergency_exit_warning):
            sys.exit(1)
        else:
            mainExitCondition = "cancelExit"
    if (mainExitCondition == "probablyOK"):
        if doThis("Really exit now?"):
            print("User exit. Have a nice day!")
            coolRunning = False        
        else:
            coolRunning = True
    if (mainExitCondition == "cancelExit"):
        print("Back to main menu.")
        coolRunning = True
    if (mainExitCondition == "unknown"):
        print("Unknown exit condition: %s" % mainExitCondition)
        coolRunning = False
        

        
        
sys.exit(1)

##########################
#  Menu Control loop END #
##########################



# Under development ---- TODO ---
#if (applyStatus > 0):
#    verifyFinalTargeted(packageName,destDir,CTRLstring,PORTSUPstring,NTPstring,DNSstring,SNMPstring,TZstring,HostADDRstring,theTargets)
# Under development ---- TODO ---

####### CONFIGURE TARGETS ENDS ############

###########
# MAIN End - Bukkiah Golden, blame that guy
###########

