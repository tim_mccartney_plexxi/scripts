import requests
import sys
import time
from pprint import pprint as pp
from requests.packages.urllib3.exceptions import InsecureRequestWarning

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

#Set the Connect IP address
nonCFM_HOST_IP = '192.168.104.151'
CFM_HOST_IP = '192.168.104.58'
#nonCFM_HOST_IP = '192.168.1.152'
#CFM_HOST_IP = '192.168.1.76'


#Create a class and the only variable we pass in is the host address

class Session:
    def __init__(self, host):
        self.host = host
        #self.token = None

# In this function call we get the token for the Connect server, nothing is passed in.
    def authenticate(self):
        headers = {
            'X-Auth-Username': 'admin',
            'X-Auth-Password': 'plexxi',
            'Content-Type': 'application/json'
        }
        r = requests.post('https://{}/api/v1/auth/token'.format(self.host), headers=headers, verify=False, timeout = 5.0)
        r.raise_for_status()
        res = r.json()
        #Not sure as to why we don't have "return here"
        self.token = res.get('result')

# This is the request to send using a method (GET, PUT etc) and a path eg. "nonCFM_session.send_request('GET', 'switches')"

    def send_request(self, method, path, json=None, params=None):
        headers = {
            'Authorization': self.token,
            'accept': 'application/json; version=1.0'
        }

        #Path to the end point
        url = 'https://{}/api/v1/{}'.format(self.host, path)        

        r = requests.request(method, url, json=json, headers=headers, verify=False)
        return r.json()
        
#pass in the json to this function
def get_switch_macs(response):
    print 'id of switch response ' + str(id(response))
    switch_macs = []
    for switch in response['result']:
        mac = switch.get('mac_address')
        switch_macs.append(mac)
    #return the variable switch_macs out from the function
    return switch_macs

# TODO Get the fabric information- is whole, description, name, uuid, count

def get_fabric(response):
    fab_info = []
    for fabric in response['result']:
        fab_desc = fabric.get('description')
        fab_uuid = fabric.get('uuid')
        fab_name = fabric.get('name')
        

    return fab_name, fab_desc

def get_switch_name(response):
    print 'id of switch response ' + str(id(response))
    switch_name = []
    for switch in response['result']:
        name = switch.get('name')
        switch_name.append(name)
    return switch_name

def get_switch_model(response):
    print 'id of switch response ' + str(id(response))
    switch_model = []
    for switch in response['result']:
        model = switch.get('model')
        switch_model.append(model)
    return switch_model

def get_lag_loop(response):
    lag_info = []
    for lag in response['result']:
        lag_port_desc = lag.get('description')
        lag_port_name = lag.get('name')
        lag_speed = lag.get('speed')
        #print '{:>5} {:5<}'.format(lag_port_name, lag_port_desc)
    return lag_port_desc, lag_port_name, lag_speed

def get_switch_uuid(response):
    print 'id of switch response ' + str(id(response))
    switch_uuid = []
    for switch in response['result']:
        sw_uuid = switch.get('uuid')
        switch_uuid.append(sw_uuid)
    return switch_uuid

def get_switch_ip(response):
    print 'id of switch response ' + str(id(response))
    switch_ip = []
    for switch in response['result']:
        sw_ip = switch.get('ip_address')
        switch_ip.append(sw_ip)
    return switch_ip


def get_ports_for_switch(sw_uuid):  
    nonCFM_session = Session(nonCFM_HOST_IP)  # address or hostname of CFM
    nonCFM_session.authenticate()

    switch_list = []
    for uuid in sw_uuid:
        port_response = nonCFM_session.send_request('GET', 'ports?switches=' + uuid)
        switch_list.append(port_response)
    # the list now has dicts inside it

    #now iterate through each one of the dicts and pull back a list of values from result, this should be one list (sw_info) per switch length of ports(72)
        for sw_dict in switch_list:
            sw_info= sw_dict.get('result')
            #print len(sw_info)
            #create a header for the table
            print '\n'
            print '{:^18} {:^5} {:^10} {:^15} {:^15} {:^8} {:^6} {:^5} {:^15} {:^15}'.format('SW Name', 'Label', 'Admin', 'Desc', 
                                                                                            'Name', 'Access', 'Native', 'Link',
                                                                                            'Current Speed', 'Vlan')
            print '\n'

            #iterate thru each element of the list (will get you data for each of the ports)
            for port in sw_info:
                port_label = port.get('port_label')
                port_admin = port.get('admin_state')
                switch_name = port.get('switch_name')
                port_name = port.get('name')
                port_desc = port.get('description') 
                port_type = port.get('access_port')
                port_native_vlan = port.get('native_vlan')
                port_link_state = port.get('link_state')
                port_speed = port.get('speed')
                port_current_speed = port_speed.get('current')
                port_vlan = port.get('vlans')
                print '{:^18} {:^5} {:^10} {:^15} {:^15} {:^8} {:^6} {:^5} {:^15} {:^15}'.format(switch_name, port_label, port_admin, 
                                                                            port_desc, port_name, port_type, port_native_vlan, 
                                                                            port_link_state, port_current_speed, port_vlan)
                  

    return switch_list 
  


# TODO create list of dictionaries that can be used to construct the PUT requests

# TODO Remember that there are some port mapping changes, no longer one to one with label

def get_fab_resp():
    nonCFM_session = Session(nonCFM_HOST_IP) 
    nonCFM_session.authenticate()
    
    sw_response = nonCFM_session.send_request('GET', 'switches') 
    lag_response = nonCFM_session.send_request('GET', 'lags?port_type=access&type=provisioned' )
    fab_response = nonCFM_session.send_request('GET', 'fabrics')
    fab_count = fab_response['count']
    return fab_count

def check_both_servers(session):
    #TODO code up a check to make sure that both Connect and CFM is online and CFM has no fabric configured (clean)
    
    nonCFM_fab = get_fab_resp() # Get the count of fabrics in Connect
    
    check_CFM_fab_resp = session.send_request('GET','fabrics')
    CFM_fab = check_CFM_fab_resp.get('count')
    
    
    if nonCFM_fab > 2: # Change this back to 1
        print ' ********** Error: Detected more than one fabric for Connect ' + str(nonCFM_HOST_IP) + ' *************'
        return False
    elif CFM_fab != 0: # Change back to 0 
        print ' ********* Error: Detected a fabric is already configured for CFM ' +str(CFM_HOST_IP) + ' ************'
        return False
    else: 
        print 'Good to start as you have one fabric in Connect and zero configured in CFM'
        return True

    

####################################################
###### From here down uses CFM      ################
####################################################

#Function to create a new fabric and return the response from CFM.
def add_fabric(session, switch, fabric_name, fabric_desc):
   
    provision_fabric = raw_input('Do you wish to create a fabric on CFM? : yes / no ')

    if provision_fabric == 'yes':
        
        post_body = {
        "host": switch,
        "name": fabric_name,
        "description": fabric_desc
        }
        response = session.send_request('POST', 'fabrics', json=post_body)
        print response.items()
        return response['result']
    
    elif provision_fabric == 'no' :      
        return 'Come back later'

# TODO call to make sure we have switches discovered in fabric - 

# TODO function to read back the UUIDs for switches

def main():
    #Put the session for host and authenticate here so you dont need to repeat for every function call
    nonCFM_session = Session(nonCFM_HOST_IP) 
    nonCFM_session.authenticate()
    CFM_session = Session(CFM_HOST_IP)
    cfm_auth = CFM_session.authenticate()

    #TODO code up a check to make sure that CFM is online and has no fabric configured (clean)
    #check_CFM_fab_resp = CFM_session.send_request('GET','fabrics')
    #CFM_fab_count = check_CFM_fab_resp.get('count')
    

    sw_response = nonCFM_session.send_request('GET', 'switches') 
    lag_response = nonCFM_session.send_request('GET', 'lags?port_type=access&type=provisioned' )
    fab_response = nonCFM_session.send_request('GET', 'fabrics')
    fab_count = fab_response['count']

    cfm_switch_response = CFM_session.send_request('GET', 'switches')
    

    #if check_both_servers(CFM_session) == False: # Check that there is only one fabric on Connect and zero on CFM
     #   exit()
    
    switch_count = sw_response['count']
    print switch_count
    print get_switch_macs(sw_response)
    print get_switch_name(sw_response)
    print get_switch_model(sw_response)
    print get_lag_loop(lag_response)
    print get_switch_uuid(sw_response)
    print get_switch_ip(sw_response)
    #Pull back the switch uuid than can be used an passed to GET for Port UUID information
    switch_uuid = get_switch_uuid(sw_response)
    get_ports_for_switch(switch_uuid)
  
###############################
#### Now do the CFM Posts #####
###############################

    #The switches have been ONIED to 5.0 and px-setup run on each switch, so that they have the correct IP and hostname, that matches the 4.1
    # configuration.


    #Get the switch IP address, name and description to pass for creating fabric function
    
    
    fabric_name, fabric_description =  get_fabric(fab_response)
    print fabric_name
    print fabric_description
    switch_for_fab = get_switch_ip(sw_response)[0] # Get the first IP address for a switch
    print add_fabric(CFM_session, switch_for_fab, fabric_name, fabric_description) # Create the fabric and return 'result'
    #sleep(120) # sleep for two minutes to allow the switches to be discovered and become healthy #TODO figure out how to do a sleep or be smarter
    

    # At this point the fabric should have been created and all switches discovered.
    # TODO pull back the switches names,UUID, MAC and IP address from CFM
    print get_switch_macs(cfm_switch_response)
    print get_switch_name(cfm_switch_response)
    print get_switch_uuid(cfm_switch_response)

    #TODO configure ports - access only? on a per switch basis, to do this you need to get the UUID that has been created in CFM....NOT.. reuse
    #  the UUID from Connect
    # 
     
  

if __name__ == '__main__':
    main()

