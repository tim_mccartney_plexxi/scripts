
"""This is a script to pull only the plexxi fabric information, and ignore any CFM"""

import requests
import sys
import time
from pprint import pprint as pp
from requests.packages.urllib3.exceptions import InsecureRequestWarning

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

#Set the Connect IP address
nonCFM_HOST_IP = '172.18.20.14'


#Create a class and the only variable we pass in is the host address

class Session:
    def __init__(self, host):
        self.host = host
        #self.token = None

# In this function call we get the token for the Connect server, nothing is passed in.
    def authenticate(self):
        headers = {
            'X-Auth-Username': 'admin',
            'X-Auth-Password': 'plexxi',
            'Content-Type': 'application/json'
        }
        r = requests.post('https://{}/api/v1/auth/token'.format(self.host), headers=headers, verify=False, timeout = 5.0)
        r.raise_for_status()
        res = r.json()
        #Not sure as to why we don't have "return here"
        self.token = res.get('result')

# This is the request to send using a method (GET, PUT etc) and a path eg. "nonCFM_session.send_request('GET', 'switches')"

    def send_request(self, method, path, json=None, params=None):
        headers = {
            'Authorization': self.token,
            'accept': 'application/json; version=1.0'
        }

        #Path to the end point
        url = 'https://{}/api/v1/{}'.format(self.host, path)        

        r = requests.request(method, url, json=json, headers=headers, verify=False)
        return r.json()
        
#pass in the json to this function
def get_switch_macs(response):
    """ Get the switch MACs, and return a list of MACs"""
    switch_macs = []
    for switch in response['result']:
        mac = switch.get('mac_address')
        switch_macs.append(mac)
    #return the variable switch_macs out from the function
    return switch_macs

# 

def get_fabric(response):
    """Get the fabric information- is whole, description, name, uuid, count
       return a string of fabric name and fabric description"""

    fab_info = []
    for fabric in response['result']:
        fab_desc = fabric.get('description')
        fab_uuid = fabric.get('uuid')
        fab_name = fabric.get('name')
        

    return fab_name, fab_desc

def get_switch_name(response):
    """Get the switch name, and return a list of switch names"""

    switch_name = []
    for switch in response['result']:
        name = switch.get('name')
        switch_name.append(name)
    return switch_name

def get_switch_model(response):
    """Get the switch model, and return a list of switch models"""

    switch_model = []
    for switch in response['result']:
        model = switch.get('model')
        switch_model.append(model)
    return switch_model

def get_lag_loop(response):
    """Get the lag information, return the lag description, name and speed"""

    lag_info = []
    for lag in response['result']:
        lag_port_desc = lag.get('description')
        lag_port_name = lag.get('name')
        lag_speed = lag.get('speed')
        #print '{:>5} {:5<}'.format(lag_port_name, lag_port_desc)
    return lag_port_desc, lag_port_name, lag_speed

def get_switch_uuid(response):
    """Get the switch UUID, and return a list of them"""

    switch_uuid = []
    for switch in response['result']:
        sw_uuid = switch.get('uuid')
        switch_uuid.append(sw_uuid)
    return switch_uuid

def get_switch_ip(response):
    """Get the switch IPs and return a list of them"""

    switch_ip = []
    for switch in response['result']:
        sw_ip = switch.get('ip_address')
        switch_ip.append(sw_ip)
    return switch_ip




def get_ports_for_switch(sw_uuid):  
    """For each of the switch UUIDs, this function is being used to extract the port information and print it
        nicely to screen, it returns a list of port information for each switch"""

    nonCFM_session = Session(nonCFM_HOST_IP)  # address or hostname of CFM
    nonCFM_session.authenticate()

    switch_list = []
    for uuid in sw_uuid:
        port_response = nonCFM_session.send_request('GET', 'ports?switches=' + uuid)
        switch_list.append(port_response)
    # the list now has dicts inside it

    #now iterate through each one of the dicts and pull back a list of values from result, this should be one list (sw_info) per switch length of ports(72)
        for sw_dict in switch_list:
            sw_info= sw_dict.get('result')
            #print len(sw_info)
            #create a header for the table
            print '\n'
            print '{:^18} {:^5} {:^10} {:^15} {:^15} {:^8} {:^6} {:^5} {:^15} {:^15}'.format('SW Name', 'Label', 'Admin', 'Desc', 
                                                                                            'Name', 'Access', 'Native', 'Link',
                                                                                            'Current Speed', 'Vlan')
            print '\n'

            #iterate thru each element of the list (will get you data for each of the ports)
            for port in sw_info:
                port_label = port.get('port_label')
                port_admin = port.get('admin_state')
                switch_name = port.get('switch_name')
                port_name = port.get('name')
                port_desc = port.get('description') 
                port_type = port.get('access_port')
                port_native_vlan = port.get('native_vlan')
                port_link_state = port.get('link_state')
                port_speed = port.get('speed')
                port_current_speed = port_speed.get('current')
                port_vlan = port.get('vlans')
                print '{:^18} {:^5} {:^10} {:^15} {:^15} {:^8} {:^6} {:^5} {:^15} {:^15}'.format(switch_name, port_label, port_admin, 
                                                                            port_desc, port_name, port_type, port_native_vlan, 
                                                                            port_link_state, port_current_speed, port_vlan)
                  

    return switch_list 
  
#TODO a function that splits and slices switch list dictionary - DONT THINK THIS IS MUCH USE
def switch_list_parser(list_switches_ports):
    print type(list_switches_ports)
    print len(list_switches_ports)
    print list_switches_ports

    for i in range (len(list_switches_ports)):
        print i

#TODO make a function that generates two lists, old and new for port uuids, and order so that index matches port label, do it only for access ports.
def get_connect_switch_port_data(sw_uuid):
    # type: (object) -> object

    nonCFM_session = Session(nonCFM_HOST_IP)  # address or hostname of CFM
    nonCFM_session.authenticate()
    
    nonCFM_switch_list = []
    for switch in sw_uuid:
        port_response = nonCFM_session.send_request('GET', 'ports?switches=' + switch +'&type=access') #For ACCESS PORTS only
        nonCFM_switch_list.append(port_response)
        print "This is length of nonCFM_switch_list " + str(len (nonCFM_switch_list))

        for sw_dict in nonCFM_switch_list:
            sw_info= sw_dict.get('result')
            #print "This is the length of sw_info: " + str(len(sw_info))

            #iterate thru each element of the list (will get you data for each of the ports)
            port_data_list = []
            for port in sw_info:
                port_label = port.get('port_label') #Port label is an "int"
                port_uuid = port.get('uuid')
                # print str(port_label)
                # port_data_list.append(port_label)
                # port_data_list.append(port_uuid)


def get_fab_resp(): #TODO Tidy this up, there is no need for sw_response, lag_repsonse
    
    nonCFM_session = Session(nonCFM_HOST_IP) 
    nonCFM_session.authenticate()
    
    sw_response = nonCFM_session.send_request('GET', 'switches') 
    lag_response = nonCFM_session.send_request('GET', 'lags?port_type=access&type=provisioned' )
    fab_response = nonCFM_session.send_request('GET', 'fabrics')
    fab_count = fab_response['count']
    return fab_count



# TODO function to read back the UUIDs for CFM switches

def main():
    #Put the session for host and authenticate here so you dont need to repeat for every function call
    nonCFM_session = Session(nonCFM_HOST_IP) 
    nonCFM_session.authenticate()
    sw_response = nonCFM_session.send_request('GET', 'switches') 
    lag_response = nonCFM_session.send_request('GET', 'lags?port_type=access&type=provisioned' )
    switch_count = sw_response['count']
    print switch_count
    print get_switch_macs(sw_response)
    print get_switch_name(sw_response)
    print get_switch_model(sw_response)
    #print get_lag_loop(lag_response) #TODO There is an error here when - local variable assigned logic
    print get_switch_uuid(sw_response)
    print get_switch_ip(sw_response)
    #Pull back the switch uuid than can be used an passed to GET for Port UUID information
    switch_uuid = get_switch_uuid(sw_response)
    get_ports_for_switch(switch_uuid)
    get_connect_switch_port_data(switch_uuid)


if __name__ == '__main__':
    main()

