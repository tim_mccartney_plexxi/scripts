#Import what we need. Add an ignore insecure (stops annoying prints)
import requests
import sys
import readline
from pprint import pprint
requests.packages.urllib3.disable_warnings()




#prompt for IP address of Connect
#test_ip = raw_input ('enter text')
#connect_IP = raw_input('Please enter the IP address for your Connect: ')
#base_url = ('https://' + connect_IP + '/api/v1')
#print base_url

# define the base URL for the API

base_url = "https://192.168.104.151/api/v1"
#base_url = "https://192.168.104.32/api/v1"
# Create a session to connect api
s = requests.session()
# Get an auth token via POST request to /auth/token resource. Give the headers as defined in api schema.
# Verify means dont verify ssl cert (its self signed)
auth_headers = {'X-Auth-Username': 'admin', 'X-Auth-Password': 'plexxi', 'Content-Type': 'application/json'}
resp = s.post(base_url + '/auth/token', headers=auth_headers, verify=False)

# optionally check if the request was successful
#if resp.status_code is 200:
#	...

# Extract the token from the json.
token = resp.json().get('result')

# Now add the authorization and other headers to the session. also persist not checking ssl
session_headers = {
	'Content-Type': 'application/json',
    'accept': 'application/json; version=1',
	'Authorization': token,
	'X-Auth-Refresh-Token': 'true'
}
s.headers.update(session_headers)
s.verify = False








# Now have an authenticated session to Connect API.
# Example GET /switches endpoint:
resp = s.get(base_url + '/switches')
#print resp.text
switches = resp.text
data = resp.json()
#pprint(data)
#print type(data)
#get switch model
switch_count = data['count']
#print switch_count
#print(type(switch_count))
#print('There are ' + str(switch_count) + ' switches in your fabric')


for switch in range(switch_count):
	#print '\nCurrent switch ', switch
	#print 'Current Switch :' + str(switch)
	
	#print type(swdata)
	#get switch mac_address
	mac_address = data['result'][switch]['mac_address']
	mac_minus_colon = mac_address.replace(":", "")
	stdoutOrigin = sys.stdout
	filename = "%s.txt" %str(mac_minus_colon)
	sys.stdout = open(filename, "w")
	#print "MAC of Switch : " + str(mac_address)
	switch_uuid = data['result'][switch]['uuid']
#	print ('Switch UUID is: ' + switch_uuid)
	swdata = data['result'][switch]['model']
	print swdata
	

	port_info = s.get(base_url + '/ports?switches=' + switch_uuid)
	port_dict= port_info.json()
	port_count = port_dict['count']
	#print ('Number of ports on this switch: ' + str (port_count))
	print '\n'
	print '{:>2} {:>40} {:>35} {:>30} {:>5} {:>10} {:>10} {:>15}' .format('MAC', 'Sw UUID', 'Port UUID','Port Label', 'PortSpeed', 'Native', 'VLANS', 'Admin State')
	for port in range(port_count):
		port_label= port_dict['result'][port]['port_label']
		port_speed= port_dict['result'][port]['speed']['current']
		native_vlan= port_dict['result'][port]['native_vlan']
		port_uuid= port_dict['result'][port]['uuid']
		vlan= port_dict['result'][port]['vlans']
		#print port_speed
		#print port_dict['result'][0]
		#print port
		#print (type(port_label))
		admin_state= port_dict['result'][port]['admin_state']
		#print ('Admin state: ' + admin_state +' for ' + 'Port Label: ' + str(port_label) + ' Switch UUID' + switch_uuid)
		#print '{:>5} {:>5} {:>40} {:>20} {:>10} {:>10} {:>10} {:>40}'.format(admin_state, port_label, 
		#											switch_uuid, mac_address, port_speed, native_vlan, vlan, port_uuid)
	

		print '{:>5} {:>40} {:>40} {:>5} {:>15} {:>10} {:>10} {:>15}'.format(mac_address, switch_uuid, port_uuid, port_label, port_speed, native_vlan, vlan, admin_state)

	
#Revert stdout back to screen
	sys.stdout.close()
	sys.stdout=stdoutOrigin



if __name__ == '__main__':
	print "test"

