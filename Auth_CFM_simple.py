import requests
import readline
from pprint import pprint
requests.packages.urllib3.disable_warnings()
# define the base URL for the API
base_url = "https://192.168.104.151/api/v1"
#base_url = "https://192.168.104.32/api/v1"

# Create a session to connect api
s = requests.session()

# Get an auth token via POST request to /auth/token resource. Give the headers as defined in api schema.
# Verify means dont verify ssl cert (its self signed)
auth_headers = {'X-Auth-Username': 'admin', 'X-Auth-Password': 'plexxi', 'Content-Type': 'application/json'}
resp = s.post(base_url + '/auth/token', headers=auth_headers, verify=False)

# Extract the token from the json.
token = resp.json().get('result')

# Now add the authorization and other headers to the session. also persist not checking ssl
session_headers = {
	'Content-Type': 'application/json',
    'accept': 'application/json; version=1',
	'Authorization': token,
	'X-Auth-Refresh-Token': 'true'
}
s.headers.update(session_headers)
s.verify = False

# Now have an authenticated session to Connect API.
# Example GET /switches endpoint:
resp = s.get(base_url + '/switches')
#print resp.text
switches = resp.text
data = resp.json()