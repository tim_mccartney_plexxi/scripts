#!/usr/bin/env python
import requests
import logging
import json
import multiprocessing
#import grequests
import time

requests.packages.urllib3.disable_warnings()
FORMAT= "%(asctime)-15s : %(message)s"

logger = logging.getLogger("connect_api")
connect_session = None
token = None
logging.basicConfig(filename='connect_api', level=logging.INFO, filemode='w', format=FORMAT)
# curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json'
# -d '{"username":"admin","password":"plexxi"}' 'https://localhost/api/v1/auth/token'
POST_REQUEST_HEADER = {'Content-Type':'application/json; charset=UTF-8', 'Accept': 'application/json'}
GET_REQUEST_HEADER = {'Accept': 'application/json version=1.0'}
DEFAULT_USER_NAME = 'admin'
DEFAULT_PASSWORD = 'plexxi'

def get_authorization_token(host='192.168.104.32', user_name=DEFAULT_USER_NAME,
                            password=DEFAULT_PASSWORD):
    url_path = 'https://{}/api/v1/auth/token'.format(host)
    auth_token = None
    global connect_session
    if not connect_session:
        logger.error('Nont connect session --Returning')
        return None
    logger.info('Auth endpoint')
    data = {'username': user_name, 'password': password}
    response = requests.post(url_path, headers=POST_REQUEST_HEADER, data=json.dumps(data), verify=False)
    logger.info('auth-token : %s',response.json()['result'])
    try:
        response.raise_for_status()
    except Exception as ex:
        logger.info('Response code : %s', response.status_code)
    if response:
        logger.info(response.json())
        auth_token =  response.json()['result']
    return auth_token

def setup_connection():
    global connect_session
    connect_session = requests.session()
    connect_session.headers.update(GET_REQUEST_HEADER)
    global token
    token = get_authorization_token()
    logger.info('Authorization token : %s', token)


def call_connect(host='192.168.104.32'):
    name = multiprocessing.current_process().name
    logger.info('call_connect_{}'.format(name))
    global connect_session
    if not connect_session:
        logger.error('Not connect session--Returning')
        return
    # url_path = 'https://{}/api/v1/users'.format(host)
    # url_path = 'https://{}/api/v1/audits'.format(host)
    url_path = 'https://{}/api/v1/nutanix/prisms'.format(host)
    # logger.info('calling url : %s', url_path)
    print('call connect')

    global token
    if token:
        GET_REQUEST_HEADER.update({'Authorization': 'Bearer {}'.format(token)})
        # logger.info(GET_REQUEST_HEADER)
        response = requests.get(url_path, headers=GET_REQUEST_HEADER, verify=False)
        print (response.text)
        print (' did we enter this')

        try:
            response.raise_for_status()
            logger.info('Response(%s) : %s', name, response.json())
        except Exception as ex:
            logger.info('Response code(%s) : %s',name, response.status_code)
    else:
        logger.info('No Token!!')




def exception_callback(request, exception):
    logger.info('Request(%s) failed with exception (%s)', request, exception)

def basic_call(host='192.168.104.32'):
    print('hello')
    r = requests.get('https://192.168.104.32/api/switches', verify = False)
    print (r.text)



if __name__ == '__main__':
    #logger.info('main method...')
    setup_connection()
    call_connect()
    basic_call()
    
    # throttle_requests(num_requests=1000000)
    # throttle_async_grequests(num_requests=3000)