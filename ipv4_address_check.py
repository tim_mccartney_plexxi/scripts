import socket


def is_valid_ipv4_address(address):
    try:
        socket.inet_pton(socket.AF_INET, address)
    except AttributeError:  # no inet_pton here, sorry
        print "no inet_pton"
        
        try:
            socket.inet_aton(address)
        except socket.error:
            return False
        return address.count('.') == 3
    except socket.error:  # not a valid address
        return False

    return True


if __name__ == "__main__":
    input_address = raw_input ("Please enter an IPv4 address\n")
    result = is_valid_ipv4_address(input_address)
    print result

