#Import what we need. Add an ignore insecure (stops annoying prints)
import requests
from pprint import pprint
requests.packages.urllib3.disable_warnings()

# define the base URL for the API
base_url = "https://192.168.104.32/api/v1"
# Create a session to connect api
s = requests.session()
# Get an auth token via POST request to /auth/token resource. Give the headers as defined in api schema.
# Verify means dont verify ssl cert (its self signed)
auth_headers = {'X-Auth-Username': 'admin', 'X-Auth-Password': 'plexxi', 'Content-Type': 'application/json'}
resp = s.post(base_url + '/auth/token', headers=auth_headers, verify=False)

# optionally check if the request was successful
#if resp.status_code is 200:
#	...

# Extract the token from the json.
token = resp.json().get('result')

# Now add the authorization and other headers to the session. also persist not checking ssl
session_headers = {
	'Content-Type': 'application/json',
    'accept': 'application/json; version=1',
	'Authorization': token,
	'X-Auth-Refresh-Token': 'true'
}
s.headers.update(session_headers)
s.verify = False

# Now have an authenticated session to Connect API.
# Example GET /switches endpoint:
resp = s.get(base_url + '/switches')
print resp.text
switches = resp.text
data = resp.json()
pprint(data)
#pprint(switches)
#print len(data.keys())
#print data.keys()
print '\n'
print '****************************************\n'
#get switch model
switch_count = data['count']
print switch_count
print(type(switch_count))
print('There are ' + str(switch_count) + ' switches in your fabric\n')


for switch in range(switch_count):
	print '\nCurrent switch ', switch

	swdata = data['result'][switch]['model']
	print swdata
	#get switch mac_address
	mac_address = data['result'][switch]['mac_address']
	print mac_address
	switch_uuid = data['result'][switch]['uuid']
	print ('Switch UUID is: ' + switch_uuid)
	

	port_info = s.get(base_url + '/ports?switches=' + switch_uuid)
	#pprint (port_info.json())
	port_dict= port_info.json()
	port_count = port_dict['count']
	print ('Number of ports on this switch: ' + str (port_count))
	for port in range(port_count):
		port_label= port_dict['result'][port]['port_label']
		#print port
		#print (type(port_label))
		admin_state= port_dict['result'][port]['admin_state']
		print ('Admin state: ' + admin_state +' for ' + 'Port Label: ' + str(port_label) + ' Switch UUID' + switch_uuid)
		#port_speed= port_dict['result'][port]['speed'][0]['current']
		#print (port_speed)
		#port_label= port_dict['result'][port]['port_label']
		#print ('Port label: ' , port_label)
		#print '\n'


#swdata = data['result'][0]['model']
#print swdata
#get switch mac_address
#mac_address = data['result'][0]['mac_address']
#print mac_address




