import os
import cfm_backup
import paramiko_scp_files


def main():
    bk_filename, bk_folder = cfm_backup.db_backup()
    print ("File name = {}, Folder = {} ".format (bk_filename, bk_folder)) 

if __name__ == "__main__":
    main()

