#!/usr/bin/env python
import datetime
import os
import subprocess
import argparse

#These are defining the colours to be used

CRED = '\033[91m'
CEND = '\033[0m'
CGREEN2 = '\33[92m'
CGREENBG2 = '\33[42m'
CBEIGE = '\33[36m'
CBLUE = '\33[104m'
CWHITE = '\33[107m'
CRED2 = '\33[101m'
CBLUE2 = '\33[94m'
CORANGE2 = '\33[93m'
CORANGE = '\33[45m'
CTORQ = '\33[46m'

#Location of the files for the manual cop an paste, specifying the full path
"""
	a1 = "/Users/shanemccaughan/Downloads/d93_switch166_12thAug-D93-56-09A-HP3032-166-2019-08-12-075206500146-sw-log-bundle/var/log/c2/messages.log"
	a2 = "/Users/shanemccaughan/Downloads/d93_switch166_12thAug-D93-56-09A-HP3032-166-2019-08-12-075206500146-sw-log-bundle/var/log/cxcontrol/messages.log"
	a3 = "/Users/shanemccaughan/Downloads/d93_switch166_12thAug-D93-56-09A-HP3032-166-2019-08-12-075206500146-sw-log-bundle/var/log/plexxi"
	a4 = "/Users/shanemccaughan/Downloads/d93_switch166_12thAug-D93-56-09A-HP3032-166-2019-08-12-075206500146-sw-log-bundle/var/log/plexxi-L3"
	a5 = "/Users/shanemccaughan/Downloads/d93_switch166_12thAug-D93-56-09A-HP3032-166-2019-08-12-075206500146-sw-log-bundle/var/log/messages"
	b1 = "/Users/shanemccaughan/Downloads/ice-sw-03-2019-08-28-112029696208-sw-log-bundle/var/log/c2/messages.log"
	b2 = "/Users/shanemccaughan/Downloads/ice-sw-03-2019-08-28-112029696208-sw-log-bundle/var/log/cxcontrol/messages.log"
	b3 = "/Users/shanemccaughan/Downloads/ice-sw-03-2019-08-28-112029696208-sw-log-bundle/var/log/plexxi"
	b4 = "/Users/shanemccaughan/Downloads/ice-sw-03-2019-08-28-112029696208-sw-log-bundle/var/log/plexxi-L3"
	b5 = "/Users/shanemccaughan/Downloads/ice-sw-03-2019-08-28-112029696208-sw-log-bundle/var/log/messages"
"""


time = datetime.datetime.now()
directory = "CombineSwitchesLogs_" + time.strftime("%d-%m-%y_%H:%M:%S")
parent_dir = "/tmp"
tempPath = os.path.join(parent_dir, directory)
all_tuple = []
c2_cx_tuple = []
c2_messages_plexxi_l3_tuple = []
test_tuple = []


def make_tmp_file():
    try:
        os.mkdir(tempPath)
    except OSError as error:
        print(error)
    print ("\n\n\nDirectory '%s' created" % directory)


def all_five(c2, cx, plexxi, plexxi_l3, messages, outfile=None):
    counter = 0
    make_tmp_file()
    write_file = open('/tmp/%s/ALL-TIMELINE.txt' % directory, 'w+')
    with open(c2) as myFile:
        data = myFile.readlines()

        for index, line in enumerate(data, 1):
            if line[0].isdigit():
                c2_date_info = line.split()[0:1]
                c2_date_string = ' '.join(c2_date_info)
                c2_time_info = line.split()[1:2]
                c2_time_string = ' '.join(c2_time_info)
                c2_string_dt = c2_date_string + ' ' + c2_time_string
                line = CBLUE + 'c2:' + CEND + ' ' + line
                c2_tuple = (c2_string_dt, line)
                all_tuple.append(c2_tuple)

    with open(cx) as myFile:
        data = myFile.readlines()
        for index, line in enumerate(data, 1):
            if line[0].isdigit():
                cx_date_info = line.split()[0:1]
                cx_date_string = ' '.join(cx_date_info)
                cx_time_info = line.split()[1:2]
                cx_time_string = ' '.join(cx_time_info)
                cx_string_dt = cx_date_string + ' ' + cx_time_string
                line = CGREENBG2 + 'cx:' + CEND + ' ' + line
                cx_tuple = (cx_string_dt, line)
                all_tuple.append(cx_tuple)

    with open(plexxi) as myFile:
        make_tmp_file()

        data = myFile.readlines()
        for index, line in enumerate(data, 1):
            year = '2019'
            plexxi_date_info = line.split()[:2]
            plexxi_date_info.insert(0, year)
            str_date_string = ' '.join(plexxi_date_info)
            plexxi_time_info = line.split()[2:3]
            plexxi_time_string = ' '.join(plexxi_time_info)
            plexxi_time_string = plexxi_time_string.replace(' ', '')

            if len(plexxi_time_string) == 8:
                date_time = str_date_string + ' ' + plexxi_time_string + '.000'
                plexxi_date_time = datetime.datetime.strptime(date_time, "%Y %b %d %H:%M:%S.%f")
                plexxi_string_dt = str(plexxi_date_time) + '.000'
                line = CRED2 + 'plexxi:' + CEND + ' ' + line
                plexxi_tuple = (plexxi_string_dt, line)
                all_tuple.append(plexxi_tuple)

            elif len(plexxi_time_string) == 12:
                date_time = str_date_string + ' ' + plexxi_time_string
                if plexxi_time_string.endswith('000'):
                    plexxi_date_time = datetime.datetime.strptime(date_time, "%Y %b %d %H:%M:%S.%f")
                    plexxi_string_dt = str(plexxi_date_time) + '.000'
                    line = CRED2 + 'plexxi:' + CEND + ' ' + line
                    plexxi_tuple = (plexxi_string_dt, line)
                    all_tuple.append(plexxi_tuple)

                else:
                    plexxi_date_time = datetime.datetime.strptime(date_time, "%Y %b %d %H:%M:%S.%f")
                    plexxi_string_dt = str(plexxi_date_time)
                    line = CRED2 + 'plexxi:' + CEND + ' ' + line
                    plexxi_tuple = (plexxi_string_dt, line)
                    all_tuple.append(plexxi_tuple)

    with open(plexxi_l3) as myFile:
        data = myFile.readlines()
        for index, line in enumerate(data, 1):
            year = '2019'
            plexxi_l3_date_info = line.split()[:2]
            plexxi_l3_date_info.insert(0, year)
            str_date_string = ' '.join(plexxi_l3_date_info)
            plexxi_l3_time_info = line.split()[2:3]
            plexxi_l3_time_string = ' '.join(plexxi_l3_time_info)
            plexxi_l3_time_string = plexxi_l3_time_string.replace(' ', '')
            date_time = str_date_string + ' ' + plexxi_l3_time_string + '.000'
            plexxi_l3_date_time = datetime.datetime.strptime(date_time, "%Y %b %d %H:%M:%S.%f")
            plexxi_l3_string_dt = str(plexxi_l3_date_time) + '.000'

            line = CORANGE + 'plexxiL3:' + CEND + ' ' + line
            plexxi_l3_tuple = (plexxi_l3_string_dt, line)
            all_tuple.append(plexxi_l3_tuple)

    with open(messages) as myFile:
        data = myFile.readlines()
        for index, line in enumerate(data, 1):
            year = '2019'
            messages_date_info = line.split()[:2]
            messages_date_info.insert(0, year)
            str_date_string = ' '.join(messages_date_info)
            messages_time_info = line.split()[2:3]
            messages_time_string = ' '.join(messages_time_info)
            messages_time_string = messages_time_string.replace(' ', '')
            date_time = str_date_string + ' ' + messages_time_string + '.000'
            messages_date_time = datetime.datetime.strptime(date_time, "%Y %b %d %H:%M:%S.%f")
            messages_string_dt = str(messages_date_time) + '.000'

            line = CTORQ + 'Messages:' + CEND + ' ' + line
            messages_tuple = (messages_string_dt, line)
            all_tuple.append(messages_tuple)

    sorted_list = sorted(all_tuple, key=lambda x: datetime.datetime.strptime(x[0], '%Y-%m-%d %H:%M:%S.%f'))

    for date, lines in sorted_list:
        print CORANGE2 + str(counter) + ': ' + CEND + lines
        write_file.write(CORANGE2 + str(counter) + ': ' + CEND + lines + '\n')
        counter = counter + 1

    counter = 0

    if outfile:
        yes = open(outfile, 'w+')
        for date, lines in sorted_list:
            yes.write(CORANGE2 + str(counter) + ': ' + CEND + lines + '\n')
            counter = counter + 1

    del all_tuple[:]

# all_five(a1, a2, a3, a4, a5)
# all_five(b1, b2, b3, b4, b5)


def c2_messages_plexxi_l3(c2, messages, plexxi_l3, outfile=None):
    counter = 0
    make_tmp_file()
    write_file = open('/tmp/%s/c2_messages_plexxi.txt' % directory, 'w+')

    with open(c2) as myFile:
        data = myFile.readlines()
        for index, line in enumerate(data, 1):
            if line[0].isdigit():
                c2_date_info = line.split()[0:1]
                c2_date_string = ' '.join(c2_date_info)
                c2_time_info = line.split()[1:2]
                c2_time_string = ' '.join(c2_time_info)
                c2_string_dt = c2_date_string + ' ' + c2_time_string

                line = CBLUE + 'c2:' + CEND + ' ' + line
                c2_tuple = (c2_string_dt, line)
                c2_messages_plexxi_l3_tuple.append(c2_tuple)

    with open(messages) as myFile:
        data = myFile.readlines()
        for index, line in enumerate(data, 1):
            year = '2019'
            messages_date_info = line.split()[:2]
            messages_date_info.insert(0, year)
            str_date_string = ' '.join(messages_date_info)
            messages_time_info = line.split()[2:3]
            messages_time_string = ' '.join(messages_time_info)
            messages_time_string = messages_time_string.replace(' ', '')
            date_time = str_date_string + ' ' + messages_time_string + '.000'
            messages_date_time = datetime.datetime.strptime(date_time, "%Y %b %d %H:%M:%S.%f")
            messages_string_dt = str(messages_date_time) + '.000'

            line = CTORQ + 'Messages:' + CEND + ' ' + line
            messages_tuple = (messages_string_dt, line)
            c2_messages_plexxi_l3_tuple.append(messages_tuple)

    with open(plexxi_l3) as myFile:
        data = myFile.readlines()
        for index, line in enumerate(data, 1):
            year = '2019'
            plexxi_l3_date_info = line.split()[:2]
            plexxi_l3_date_info.insert(0, year)
            str_date_string = ' '.join(plexxi_l3_date_info)
            plexxi_l3_time_info = line.split()[2:3]
            plexxi_l3_time_string = ' '.join(plexxi_l3_time_info)
            plexxi_l3_time_string = plexxi_l3_time_string.replace(' ', '')
            date_time = str_date_string + ' ' + plexxi_l3_time_string + '.000'
            plexxi_l3_date_time = datetime.datetime.strptime(date_time, "%Y %b %d %H:%M:%S.%f")
            plexxi_l3_string_dt = str(plexxi_l3_date_time) + '.000'

            line = CORANGE + 'plexxiL3:' + CEND + ' ' + line
            plexxi_l3_tuple = (plexxi_l3_string_dt, line)
            c2_messages_plexxi_l3_tuple.append(plexxi_l3_tuple)

    sorted_list = sorted(c2_messages_plexxi_l3_tuple, key=lambda x: datetime.datetime.strptime(x[0], '%Y-%m-%d %H:%M:%S.%f'))

    for date, lines in sorted_list:
        print CORANGE2 + str(counter) + ': ' + CEND + lines
        write_file.write(CORANGE2 + str(counter) + ': ' + CEND + lines + '\n')
        counter = counter + 1

    counter = 0
    if outfile:
        yes = open(outfile, 'w+')
        for date, lines in sorted_list:
            yes.write(CORANGE2 + str(counter) + ': ' + CEND + lines + '\n')
            counter = counter + 1

    del c2_messages_plexxi_l3_tuple[:]


# c2_messages_plexxi_l3(a1, a5, a4)
# c2_messages_plexxi_l3(b1, b5, b4)


def c2cx(c2, cx, outfile=None):
    counter = 0
    make_tmp_file()
    write_file = open('/tmp/%s/c2_cx.txt' % directory, 'w+')

    with open(c2) as myFile:
        data = myFile.readlines()

        for index, line in enumerate(data, 1):
            if line[0].isdigit():
                c2_date_info = line.split()[0:1]
                c2_date_string = ' '.join(c2_date_info)
                c2_time_info = line.split()[1:2]
                c2_time_string = ' '.join(c2_time_info)
                c2_string_dt = c2_date_string + ' ' + c2_time_string
                line = CBLUE + 'c2:' + CEND + ' ' + line

                c2_tuple = (c2_string_dt, line)
                c2_cx_tuple.append(c2_tuple)

    with open(cx) as myFile:
        data = myFile.readlines()
        for index, line in enumerate(data, 1):
            if line[0].isdigit():
                cx_date_info = line.split()[0:1]
                cx_date_string = ' '.join(cx_date_info)
                cx_time_info = line.split()[1:2]
                cx_time_string = ' '.join(cx_time_info)
                cx_string_dt = cx_date_string + ' ' + cx_time_string
                line = CGREENBG2 + 'cx:' + CEND + ' ' + line

                cx_tuple = (cx_string_dt, line)
                c2_cx_tuple.append(cx_tuple)

    sorted_list = sorted(c2_cx_tuple, key=lambda x: datetime.datetime.strptime(x[0], '%Y-%m-%d %H:%M:%S.%f'))

    for date, lines in sorted_list:
        print CORANGE2 + str(counter) + ': ' + CEND + lines
        write_file.write(CORANGE2 + str(counter) + ': ' + CEND + lines + '\n')
        counter = counter + 1
    counter = 0

    if outfile:
        yes = open(outfile, 'w+')
        for date, lines in sorted_list:
            yes.write(CORANGE2 + str(counter) + ': ' + CEND + lines + '\n')
            counter = counter + 1
    del c2_cx_tuple[:]

# c2cx(a1, a2, 'HI')
# c2cx(b1, b2)


def command_line_list():
    parser = argparse.ArgumentParser()

    parser.add_argument('--all', '-a', help="Print out timeline for c2, cx, plexxi, plexxi-L3, messages", dest='all', action='store')
    parser.add_argument('--c2controlx', '-c2cx', help="Print out timeline for c2, cx", dest='c2cx', action='store')
    parser.add_argument('--c2messagesplexxiL3', '-c2mp', help="Print out timeline for c2, messages, plexxiL3", dest='c2mp', action='store')
    parser.add_argument('--list', '-l', help="List of timelines", dest='list', action='store')

    args = parser.parse_args()
    switch_word = 'c2platd'

    if args.all:
        check_switch = 'ps -ef | grep c2platd | grep -v grep'
        test = subprocess.Popen(check_switch, shell=True, stdout=subprocess.PIPE)
        text = test.stdout.read()
        #print 'Output:\n' + CRED + text + CEND

        if switch_word in text:
            auto_input_c2 = "/var/log/c2/messages.log"
            auto_input_messages = "/var/log/messages"
            auto_input_plexxi_l3 = "/var/log/plexxi-L3"
            auto_input_cx = "/var/log/cxcontrol/messages.log"
            auto_input_plexxi = "/var/log/plexxi"
            all_five(auto_input_c2, auto_input_cx, auto_input_plexxi, auto_input_plexxi_l3, auto_input_messages, args.all)
            print '\n' + CGREEN2 + 'SWITCH WAS FOUND' + CEND

        elif switch_word not in text:
            print '\n' + CRED + 'SWITCH NOT FOUND - ENTER MANUALLY' + CEND
            input_c2 = raw_input('Enter full path of c2: ')
            input_cx = raw_input('Enter full path of cx: ')
            input_plexxi = raw_input('Enter full path of plexxi: ')
            input_plexxi_l3 = raw_input('Enter full path of plexxi-L3: ')
            input_messages = raw_input('Enter full path of messages: ')
            all_five(input_c2, input_cx, input_plexxi, input_plexxi_l3, input_messages, args.all)

        print 'File Name: ', CBLUE + args.all + CEND + "  \nTo view file - 'cat %s' \n" % args.all

    if args.c2cx:
        check_switch = 'ps -ef | grep c2platd | grep -v grep'
        test = subprocess.Popen(check_switch, shell=True, stdout=subprocess.PIPE)
        text = test.stdout.read()

        #print 'Output:\n' + CRED + text + CEND
        if switch_word in text:
            auto_input_c2 = "/var/log/c2/messages.log"
            auto_input_cx = "/var/log/cxcontrol/messages.log"
            c2cx(auto_input_c2, auto_input_cx, args.c2cx)
            print CGREEN2 + 'SWITCH WAS FOUND' + CEND

        elif switch_word not in text:
            print '\n' + CRED + 'SWITCH NOT FOUND - ENTER MANUALLY' + CEND
            input_c2 = raw_input('Enter full path of c2: ')
            input_cx = raw_input('Enter full path of cx: ')
            c2cx(input_c2, input_cx, args.c2cx)

        print 'File Name: ', CBLUE + args.c2cx + CEND + "  \nTo view file - 'cat %s' \n" % args.c2cx

    if args.c2mp:
        check_switch = 'ps -ef | grep c2platd | grep -v grep'
        test = subprocess.Popen(check_switch, shell=True, stdout=subprocess.PIPE)
        text = test.stdout.read()
        #print 'Output:\n' + CRED + text + CEND

        if switch_word in text:
            auto_input_c2 = "/var/log/c2/messages.log"
            auto_input_messages = "/var/log/messages"
            auto_input_plexxi_l3 = "/var/log/plexxi-L3"
            c2_messages_plexxi_l3(auto_input_c2, auto_input_messages, auto_input_plexxi_l3, args.c2mp)
            print '\n' + CGREEN2 + 'SWITCH WAS FOUND' + CEND

        elif switch_word not in text:
            print '\n' + CRED + 'SWITCH NOT FOUND - ENTER MANUALLY' + CEND
            input_c2 = raw_input('Enter full path of c2: ')
            input_plexxi_l3 = raw_input('Enter full path of plexxi-L3: ')
            input_messages = raw_input('Enter full path of messages: ')
            c2_messages_plexxi_l3(input_c2, input_messages, input_plexxi_l3, args.c2mp)

        print 'File Name: ', CBLUE + args.c2mp + CEND + "  \nTo view file- 'cat %s' \n" % args.c2mp

    if args.list:
        check_switch = 'ps -ef | grep c2platd | grep -v grep'
        test = subprocess.Popen(check_switch, shell=True, stdout=subprocess.PIPE)
        text = test.stdout.read()

        if switch_word in text:
            print '\n' + CGREEN2 + 'SWITCH FOUND' + CEND
            input_which = input("\n Which timeline would you like to view: \n 1 - c2, cx, plexxi, plexxi-L3, messages \n 2 - c2, messages, plexxi-L3 \n 3 - c2, cx \n 4 - Exit \n\n")

            while (input_which < 5) and (input_which > 0):
                if input_which == 1:
                    auto_input_cx = "/var/log/cxcontrol/messages.log"
                    auto_input_plexxi = "/var/log/plexxi"
                    auto_input_c2 = "/var/log/c2/messages.log"
                    auto_input_messages = "/var/log/messages"
                    auto_input_plexxi_l3 = "/var/log/plexxi-L3"
                    all_five(auto_input_c2, auto_input_cx, auto_input_plexxi, auto_input_plexxi_l3, auto_input_messages, args.list)

                if input_which == 2:
                    auto_input_c2 = "/var/log/c2/messages.log"
                    auto_input_messages = "/var/log/messages"
                    auto_input_plexxi_l3 = "/var/log/plexxi-L3"
                    c2_messages_plexxi_l3(auto_input_c2, auto_input_messages, auto_input_plexxi_l3, args.list)

                if input_which == 3:
                    auto_input_c2 = "/var/log/c2/messages.log"
                    auto_input_cx = "/var/log/cxcontrol/messages.log"
                    c2cx(auto_input_c2, auto_input_cx, args.list)

                if input_which == 4:
                    exit(1)
                print '\n' + CGREEN2 + 'SWITCH WAS FOUND' + CEND + '\n'
                input_which = input("\n Which timeline would you like to view: \n 1 - c2, cx, plexxi, plexxi-L3, messages \n 2 - c2, messages, plexxi-L3 \n 3 - c2, cx \n 4 - Exit \n\n")

        elif switch_word not in text:
            print '\n' + CRED + 'SWITCH NOT FOUND - ENTER MANUALLY' + CEND
            input_which = input("\n Which timeline would you like to view: \n 1 - c2, cx, plexxi, plexxi-L3, messages \n 2 - c2, messages, plexxi-L3 \n 3 - c2, cx \n 4 - Exit \n\n")
            input_c2 = raw_input('Enter full path of c2: ')
            input_cx = raw_input('Enter full path of cx: ')
            input_plexxi = raw_input('Enter full path of plexxi: ')
            input_plexxi_l3 = raw_input('Enter full path of plexxi-L3: ')
            input_messages = raw_input('Enter full path of messages: ')

            while (input_which < 5) and (input_which > 0):
                if input_which == 1:
                    all_five(input_c2, input_cx, input_plexxi, input_plexxi_l3, input_messages, args.list)

                elif input_which == 2:
                    c2_messages_plexxi_l3(input_c2, input_messages, input_plexxi_l3, args.list)

                elif input_which == 3:
                    c2cx(input_c2, input_cx, args.list)

                elif input_which == 4:
                    exit(0)

                print '--------------------------------------------------------------------------------------------------------------------'
                input_which = input("Which timeline would you like to view: \n 1 - c2, cx, plexxi, plexxi-L3, messages \n 2 - c2, messages, plexxi-L3 \n 3 - c2, cx \n 4 - Exit \n\n")


command_line_list()
