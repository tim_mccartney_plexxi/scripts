#Import what we need. Add an ignore insecure (stops annoying prints)
import requests
import readline
from pprint import pprint
requests.packages.urllib3.disable_warnings()




#prompt for IP address of Connect
#test_ip = raw_input ('enter text')
#connect_IP = raw_input('Please enter the IP address for your Connect: ')
#base_url = ('https://' + connect_IP + '/api/v1')
#print base_url

# define the base URL for the API

base_url = "https://192.168.104.151/api/v1"
#base_url = "https://192.168.104.32/api/v1"
# Create a session to connect api
s = requests.session()
# Get an auth token via POST request to /auth/token resource. Give the headers as defined in api schema.
# Verify means dont verify ssl cert (its self signed)
auth_headers = {'X-Auth-Username': 'admin', 'X-Auth-Password': 'plexxi', 'Content-Type': 'application/json'}
resp = s.post(base_url + '/auth/token', headers=auth_headers, verify=False)

# optionally check if the request was successful
#if resp.status_code is 200:
#	...

# Extract the token from the json.
token = resp.json().get('result')

# Now add the authorization and other headers to the session. also persist not checking ssl
session_headers = {
	'Content-Type': 'application/json',
    'accept': 'application/json; version=1',
	'Authorization': token,
	'X-Auth-Refresh-Token': 'true'
}
s.headers.update(session_headers)
s.verify = False

# Now have an authenticated session to Connect API.
# Example GET /switches endpoint:
resp = s.get(base_url + '/switches')
#print resp.text
switches = resp.text
data = resp.json()
#pprint(data)
#print type(data)

print '\n'
print '****************************************\n'
#get switch model
switch_count = data['count']
#print switch_count
#print(type(switch_count))
print('There are ' + str(switch_count) + ' switches in your fabric\n')


for switch in range(switch_count):
	print '\nCurrent switch ', switch

	swdata = data['result'][switch]['model']
	print swdata
	#print type(swdata)
	#get switch mac_address
	mac_address = data['result'][switch]['mac_address']
	print mac_address
	switch_uuid = data['result'][switch]['uuid']
	print ('Switch UUID is: ' + switch_uuid)
	

	port_info = s.get(base_url + '/ports?switches=' + switch_uuid)
	port_dict= port_info.json()
	port_count = port_dict['count']
	print ('Number of ports on this switch: ' + str (port_count))
	print '\n'
	print '{:>2} {:>40} {:>35} {:>30} {:>5} {:>10} {:>10} {:>15}' .format('MAC', 'Sw UUID', 'Port UUID','Port Label', 'PortSpeed', 'Native', 'VLANS', 'Admin State')
	for port in range(port_count):
		port_label= port_dict['result'][port]['port_label']
		port_speed= port_dict['result'][port]['speed']['current']
		native_vlan= port_dict['result'][port]['native_vlan']
		port_uuid= port_dict['result'][port]['uuid']
		vlan= port_dict['result'][port]['vlans']
		#print port_speed
		#print port_dict['result'][0]
		#print port
		#print (type(port_label))
		admin_state= port_dict['result'][port]['admin_state']
		#print ('Admin state: ' + admin_state +' for ' + 'Port Label: ' + str(port_label) + ' Switch UUID' + switch_uuid)
		#print '{:>5} {:>5} {:>40} {:>20} {:>10} {:>10} {:>10} {:>40}'.format(admin_state, port_label, 
		#											switch_uuid, mac_address, port_speed, native_vlan, vlan, port_uuid)
	

		print '{:>5} {:>40} {:>40} {:>5} {:>15} {:>10} {:>10} {:>15}'.format(mac_address, switch_uuid, port_uuid, port_label, port_speed, native_vlan, vlan, admin_state)


#This is to capture the LAGs that have been explicitly provisioned, NOTE- excludes internal fabric LAGs
prov_lag_info = s.get(base_url + '/lags?type=provisioned')
lag_info_dict = prov_lag_info.json()
#Get the number of LAGs that are ACCESS and PROVISIONED
prov_acces_lag_info = s.get(base_url + '/lags?count_only=true&port_type=access&type=provisioned')
lag_acces_dict = prov_acces_lag_info.json()
lag_count = lag_acces_dict['result']
print "********************************************************\n"
print "The number of LAGs on this Fabric = " + str(lag_count)

#lag_type= lag_info_dict['result']
#pprint (lag_type)

for lag in range(lag_count):
	lag_port_desc = lag_info_dict['result'][lag]['description']
	lag_port_name = lag_info_dict['result'][lag]['name']
	lag_port_uuid = lag_info_dict['result'][lag]['port_properties'][0]['port_uuids']
	lag_speed = lag_info_dict['result'][lag]['port_properties'][0]['speed']['current']
	lag_mode = lag_info_dict['result'][lag]['port_properties'][0]['lacp']['mode']
	
	

	print "\nThis is the LAG information"
	print "LAG Description = " + lag_port_desc
	print "LAG Name = " + lag_port_name
	print "LAG speed = " + str(lag_speed)
	print "LAG mode = " + lag_mode
	print "Number of ports = " + str(len(lag_port_uuid))
	for i in range (len(lag_port_uuid)):
		print "Port UUID = " + lag_port_uuid[i]
print "********************************************************\n"

#Get PACK information

pack_info = s.get(base_url + '/packs?configurations=true')
pack_info_dict = pack_info.json()
#print pack_info_dict
pack_count = pack_info_dict['count']

print "This is the PACK information take from Connect "
print "Number of Packs = " + str(pack_count)
print '\n\n\n'
for pack in range(pack_count):
	pack_desc = pack_info_dict['result'][pack]['description']
	pack_name = pack_info_dict['result'][pack]['name']
	

#THIS BREAKS WITH KEY ERROR FOR CONFIGURATIONS IF YOU DO NOT HAVE ALL PACKS CONFIGURED
	pack_username = pack_info_dict['result'][pack]['configurations'][0]['username']
	pack_host = pack_info_dict['result'][pack]['configurations'][0]['host']
	pack_con_state = pack_info_dict['result'][pack]['configurations'][0]['connection_state']
	pack_enabled = pack_info_dict['result'][pack]['configurations'][0]['enabled']
	print pack_desc
	print pack_name.capitalize()
	print pack_con_state
	print pack_host
	print str(pack_enabled)
	print pack_username
	print "*****************************************"




