
from legacy_harvest import *
import requests
import sys
import time
from pprint import pprint as pp
from requests.packages.urllib3.exceptions import InsecureRequestWarning

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)


#print get_switch_ip(sw_response)
#print get_fabric(fab_response)
CFM_session = Session(CFM_HOST_IP)
CFM_session.authenticate()

fabric_name, fabric_description =  get_fabric(fab_response)
print fabric_name
print fabric_description
switch_for_fab = get_switch_ip(sw_response)[0]
print switch_for_fab

#TODO create a function to post to CFM

def add_fabric(switch, fabric_name, fabric_desc):
    post_body = {
      "host": switch,
      "name": fabric_name,
      "vlans": fabric_desc
    }
    #print post_body
    response = CFM_session.send_request('POST', 'fabrics', json=post_body)
