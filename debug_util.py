
import json
import httplib
import os
import requests
import re
from requests.packages.urllib3.util.retry import Retry
from requests.adapters import HTTPAdapter


httplib_send = httplib.HTTPConnection.send
httplib_read = httplib.HTTPConnection.getresponse

def rest_debug(action=None, host=None, endpoint=None, callback=None, response_headers=False):
    """
    This will patch the urllib, so the raw request will be intercepted. This
    can be used for debug purposes. A request can be filtered by action, host,
    or endpoint.

    :param str action:     Action to filter on (GET, PUT, etc)
    :param str host:       Host to filter on
    :param str endpoint:   Endpoint to filter on
    :return:               None
    """
    original_send = httplib_send
    original_read = httplib_read

    def patched_send(self, data):
        re_action = re.compile('^(%s) ' % (action), re.IGNORECASE)
        re_host = re.compile('Host:\s+(%s)' % (host), re.MULTILINE)
        re_endpoint = re.compile('^\w+ (%s) HTTP' % (endpoint))

        if action and not re_action.match(data):
            return original_send(self, data)

        if host and not re_host.search(data):
            return original_send(self, data)

        if endpoint and not re_endpoint.match(data):
            return original_send(self, data)

        if callback:
            callback(data)
        else:
            print data

        return original_send(self, data)

    def patched_response(self, buffering=False):
        response = original_read(self, buffering=buffering)
        version = '.'.join(str(response.version))
        print 'HTTP/%s %s %s' % (version, response.status, response.reason)
        print response.msg

        return response

    if response_headers:
        httplib.HTTPConnection.getresponse = patched_response

    httplib.HTTPConnection.send = patched_send
