#!/bin/bash

#sudo /opt/plexxi/bin/psh ring '/bin/px-shell -e "show hardware"' |/bin/grep "System Product Code" |/usr/bin/awk {'print $4'} > /tmp/list
/bin/px-shell -e "show hardware" |/bin/grep "System Product Code" |/usr/bin/awk {'print $4'} > /tmp/migrate_switch_type
/bin/px-shell -e "show interface summary"  > /tmp/migrate_interface_conf
/bin/px-shell -e "show vlan" > /tmp/migrate_vlan_conf
/bin/px-shell -e "show qsfp" > /tmp/migrate_qsfp_conf
/bin/px-shell -e "show vlan group" > /tmp/migrate_vlangroup_conf
/bin/px-shell -e "show lag" > /tmp/migrate_mlag_conf
/bin/px-shell -e "show transceivers" > /tmp/migrate_trans_conf
/bin/px-shell -e "show timezone" > /tmp/migrate_tz_conf
cp /plexxi/1/chassis/eeprom/mac_base /tmp/migrate_mac_base

cp /etc/hostname /tmp/migrate_hostname

cd /tmp ; /bin/ls migrate* |/usr/bin/xargs /bin/tar -cf migration_$HOSTNAME.tar 

# push to all other switches so it can be wholesale collected from any one. A design assumption elsewhere!
/opt/plexxi/bin/pcp /tmp/migration_$HOSTNAME.tar ring:/tmp/
