#!/usr/bin/python 

##########################################################################################
# Copyright 2017 Plexxi Inc. and its licensors.  All rights reserved.                    #
# Use and duplication of this software is subject to a separate license                  #
# agreement between the user and Plexxi or its licensor.                                 #
##########################################################################################


import os,sys,socket,re,datetime,subprocess,readline,argparse,shutil,time,pickle,requests,json
import logging
logging.basicConfig(filename="/var/log/migration.log",level=logging.DEBUG)
logging.basicConfig(level=logging.WARNING, format="%(asctime)s - %(levelname)s - %(message)s")
logging.debug("DEBUG Logging activated.")

#############
# scpSTUFF2switch 
#############


def scpSTUFF2switch(TARGETstring,filesToTransport):

    print("SCP'ing %s to target %s." % (filesToTransport,TARGETstring))

    userName = "admin"
    
    if (TARGETstring != ""):
        commandX = "sudo /usr/bin/scp " + " " + filesToTransport + " " + userName + "@" + TARGETstring + ":/tmp"
    else:
        return ""      #This leaves little choice but to fail, due to lack of pyschic feature. 
    
    logging.debug("CommandX \"%s\"" % commandX)
    try: 
        raw_resultX = subprocess.check_output(commandX,stderr=subprocess.STDOUT, shell=True)
        logging.debug("*****")
        logging.debug(raw_resultX)
        logging.debug("*****")
    except subprocess.CalledProcessError:
        print("Error encountered attempting to transfer files: command \"%s\" --error-- \"%s\"" % (commandX,subprocess.STDOUT))
        return "" # reflects failure to apply, not sure if we want to bail out here.            

    
    return "success"

#############
# scpSTUFF4switch END
#############

def scpSTUFF4switch(TARGETstring,filesToTransport):

    print("SCP'ing %s from target %s." % (filesToTransport,TARGETstring))

    userName = "admin"

    if (TARGETstring != ""): 
        commandX = "sudo /usr/bin/scp " + " " + userName + "@" + TARGETstring + ":/tmp/" + filesToTransport + " /tmp/"
    else:
        return ""      #This leaves little choice but to fail, due to lack of pyschic feature. 
    logging.debug("CommandX \"%s\"" % commandX)
    try: 
        raw_resultX = subprocess.check_output(commandX,stderr=subprocess.STDOUT, shell=True)
        logging.debug("*****")
        logging.debug(raw_resultX)
        logging.debug("*****")
    except subprocess.CalledProcessError:
        print("Error encountered attempting to transfer files: command \"%s\" --error-- \"%s\"" % (commandX,subprocess.STDOUT))
        return "" # reflects failure to apply, not sure if we want to bail out here.            

    
    return "success"

#############
# scpSTUFF4switch END
#############


#############
# execSTUFF2switch 
#############

def execSTUFF2switch(TARGETstring,commandY):

    print("Remotely executing collection script.")

    userName = "admin"

    if (commandY == ""):
        print("exec what!? error")
        return ""
    
    # We should be passing in TARGETstring always in this case, BUT, sometimes it looks like we don't so doublecheck we have a sensible value
    
    if (TARGETstring != ""): # if we have no TARGETstring it may have been unconfigured, so don't use that!        
        hoststring = userName + "@" + TARGETstring
    else:
        if (TARGETstringFOUND != ""):
            hoststring = userName + "@" + TARGETstringFOUND
        else:
            return ""      #This leaves little choice but to fail, due to lack of pyschic feature. 

    commandY2 = "/usr/bin/ssh -t " + hoststring + " \"sudo " + commandY + "\""
    
    logging.debug("CommandY2 \"%s\"" % commandY2)
    
    #ssh = subprocess.Popen([commandY2],shell=False)

    #    ssh = subprocess.check_output(commandY2,stderr=subprocess.STDOUT,shell=True)
    #ssh = subprocess.call(["/usr/bin/ssh -t ", hoststring, "sudo ", commandY])

    #ssh = subprocess.check_output([commandY2],stderr=subprocess.STDOUT,shell=True)
    ssh = subprocess.check_call([commandY2],stderr=subprocess.STDOUT,shell=True)
    
    logging.debug("Result is %s" % ssh)
    
    result = ssh
    if (result == []):
        error = ssh.stderr.readlines()
        print("ERROR detected: %s" % error)
        return ""

    # if we get here result is good, return that.
    return result

#############
# execSTUFF2switch END
#############

#parse the command line args

# while defaults are provided in argparse, if there is no arg, no default seems to be applied so define here.


parser = argparse.ArgumentParser(description='Simplexxi, the Plexxi switch fabric configuration engine.')
parser.add_argument('-c','--configure', dest='theChosenOnes', default="TBD", help='Too early in the morning for specifics.')
parser.add_argument('-t','--target', dest = 'theTargets', help='A switch IP address to go talk to and gain information about fabric.')
parser.add_argument('-o','--override', dest='theOverrides', default="TBD", help='Too early in the morning for specifics.')
args = parser.parse_args()


theTargets = args.theTargets

scpRES = scpSTUFF2switch(theTargets,"fabricscan2.sh collect_switches_conf.sh")
execRES = execSTUFF2switch(theTargets,"/tmp/collect_switches_conf.sh")
scpRES = scpSTUFF4switch(theTargets,"all_migrations.tar")
