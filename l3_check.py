#!/usr/bin/python

"""This is a simple script to test for slowpath on 3eq and 3ex"""

import os

print "Testing for L3 slowpath"
path = "/home/admin"
os.chdir(path)
os.system("/opt/plexxi/bin/psh ring 'snoopsh l3 egress show | grep 8191' > output.txt")
