#!/usr/bin/env python
import datetime
import os
import subprocess
import argparse

"""Colour codes"""
CRED = '\033[91m'
CEND = '\033[0m'
CGREEN2 = '\33[92m'
CGREENBG2 = '\33[42m'
CBEIGE = '\33[36m'
CBLUE = '\33[104m'
CWHITE = '\33[107m'
CRED2 = '\33[101m'
CBLUE2 = '\33[94m'
CORANGE2 = '\33[93m'
CORANGE = '\33[45m'
CTORQ = '\33[46m'

"""
examples of the file paths 
tims c2     "/Users/tmccartney/Downloads/tmp/c2/messages.log"
tims cx     "/Users/tmccartney/Downloads/tmp/cxcontrol/messages.log"
tims plexxi "/Users/tmccartney/Downloads/tmp/plexxi"
tims plexxi-L3  "/Users/tmccartney/Downloads/tmp/plexxi-L3"
tims messages   "/Users/tmccartney/Downloads/tmp/messages"

"""

time = datetime.datetime.now()
directory = "CombineSwitchesLogs_" + time.strftime("%d-%m-%y_%H:%M:%S")
parent_dir = "/tmp"
tempPath = os.path.join(parent_dir, directory)

# Tuples which the chosen elements are added to before being sorting chronologically
# Each element contains 2 parts - DateTime, and the line
all_tuple = []
c2_cx_tuple = []
c2_messages_plexxi_l3_tuple = []


def make_tmp_file():
    """Makes the temporary file located in /tmp"""
    try:
        os.mkdir(tempPath)
    except OSError as error:
        print(error)
    print ("\n\n\nDirectory '%s' created" % directory)


def all_five(c2, cx, plexxi, plexxi_l3, messages, outfile=None):
    """Ordered c2,cx,plexxi,plexxi_L3,messages.
        'outfile' is the name of the file which is to be created and written to"""

    counter = 0
    # Creates /tmp file
    make_tmp_file()
    write_file = open('/tmp/%s/ALL-TIMELINE.txt' % directory, 'w+')

    #Read c2 file
    with open(c2) as myFile:
        data = myFile.readlines()
        for index, line in enumerate(data, 1):
            if line[0].isdigit():
                # Format date/time
                c2_date_info = line.split()[0:1]
                c2_date_string = ' '.join(c2_date_info)
                c2_time_info = line.split()[1:2]
                c2_time_string = ' '.join(c2_time_info)
                c2_string_dt = c2_date_string + ' ' + c2_time_string
                line = CBLUE + 'c2:' + CEND + ' ' + line

                # c2_tuple contains both c2 dateTime, and the log line
                c2_tuple = (c2_string_dt, line)
                # Added to all_tuple
                all_tuple.append(c2_tuple)

    #Read cx file
    with open(cx) as myFile:
        data = myFile.readlines()
        for index, line in enumerate(data, 1):
            if line[0].isdigit():
                # Format date/time
                cx_date_info = line.split()[0:1]
                cx_date_string = ' '.join(cx_date_info)
                cx_time_info = line.split()[1:2]
                cx_time_string = ' '.join(cx_time_info)
                cx_string_dt = cx_date_string + ' ' + cx_time_string
                line = CGREENBG2 + 'cx:' + CEND + ' ' + line

                # cx_tuple contains both cx dateTime, and the log line
                cx_tuple = (cx_string_dt, line)
                # Added to all_tuple
                all_tuple.append(cx_tuple)

    #Read plexxi file
    with open(plexxi) as myFile:
        data = myFile.readlines()
        for index, line in enumerate(data, 1):
            # Format date/time
            year = '2020'
            plexxi_date_info = line.split()[:2]
            plexxi_date_info.insert(0, year)
            str_date_string = ' '.join(plexxi_date_info)
            plexxi_time_info = line.split()[2:3]
            plexxi_time_string = ' '.join(plexxi_time_info)
            plexxi_time_string = plexxi_time_string.replace(' ', '')

            #Format date/time
            if len(plexxi_time_string) == 8:
                date_time = str_date_string + ' ' + plexxi_time_string + '.000'
                plexxi_date_time = datetime.datetime.strptime(date_time, "%Y %b %d %H:%M:%S.%f")
                plexxi_string_dt = str(plexxi_date_time) + '.000'
                line = CRED2 + 'plexxi:' + CEND + ' ' + line

                # plexxi_tuple contains both plexxi dateTime, and the log line
                plexxi_tuple = (plexxi_string_dt, line)
                # Added to all_tuple
                all_tuple.append(plexxi_tuple)

            #Format date/time
            elif len(plexxi_time_string) == 12:
                date_time = str_date_string + ' ' + plexxi_time_string
                if plexxi_time_string.endswith('000'):
                    plexxi_date_time = datetime.datetime.strptime(date_time, "%Y %b %d %H:%M:%S.%f")
                    plexxi_string_dt = str(plexxi_date_time) + '.000'
                    line = CRED2 + 'plexxi:' + CEND + ' ' + line

                    # plexxi_tuple contains both plexxi dateTime, and the log line
                    plexxi_tuple = (plexxi_string_dt, line)
                    # Added to all_tuple
                    all_tuple.append(plexxi_tuple)

                else:
                    plexxi_date_time = datetime.datetime.strptime(date_time, "%Y %b %d %H:%M:%S.%f")
                    plexxi_string_dt = str(plexxi_date_time)
                    line = CRED2 + 'plexxi:' + CEND + ' ' + line

                    # plexxi_tuple contains both plexxi dateTime, and the log line
                    plexxi_tuple = (plexxi_string_dt, line)
                    # Added to all_tuple
                    all_tuple.append(plexxi_tuple)

    #Read plexxi_L3 file
    with open(plexxi_l3) as myFile:
        data = myFile.readlines()
        for index, line in enumerate(data, 1):
            # Format date/time
            year = '2020'
            plexxi_l3_date_info = line.split()[:2]
            plexxi_l3_date_info.insert(0, year)
            str_date_string = ' '.join(plexxi_l3_date_info)
            plexxi_l3_time_info = line.split()[2:3]
            plexxi_l3_time_string = ' '.join(plexxi_l3_time_info)
            plexxi_l3_time_string = plexxi_l3_time_string.replace(' ', '')
            date_time = str_date_string + ' ' + plexxi_l3_time_string + '.000'
            plexxi_l3_date_time = datetime.datetime.strptime(date_time, "%Y %b %d %H:%M:%S.%f")
            plexxi_l3_string_dt = str(plexxi_l3_date_time) + '.000'
            line = CORANGE + 'plexxiL3:' + CEND + ' ' + line

            # plexxi_L3_ tuple contains both plexxi_L3 dateTime, and the log line
            plexxi_l3_tuple = (plexxi_l3_string_dt, line)
            # Added to all_tuple
            all_tuple.append(plexxi_l3_tuple)

    with open(messages) as myFile:
        data = myFile.readlines()
        for index, line in enumerate(data, 1):
            # Format date/time
            year = '2020'
            messages_date_info = line.split()[:2]
            messages_date_info.insert(0, year)
            str_date_string = ' '.join(messages_date_info)
            messages_time_info = line.split()[2:3]
            messages_time_string = ' '.join(messages_time_info)
            messages_time_string = messages_time_string.replace(' ', '')
            date_time = str_date_string + ' ' + messages_time_string + '.000'
            messages_date_time = datetime.datetime.strptime(date_time, "%Y %b %d %H:%M:%S.%f")
            messages_string_dt = str(messages_date_time) + '.000'
            line = CTORQ + 'Messages:' + CEND + ' ' + line

            # messages_ tuple contains both messages dateTime, and the log line
            messages_tuple = (messages_string_dt, line)
            # Added to all_tuple
            all_tuple.append(messages_tuple)

    # List which sorts all_tuple in chronological order by the date format shown below
    sorted_list = sorted(all_tuple, key=lambda x: datetime.datetime.strptime(x[0], '%Y-%m-%d %H:%M:%S.%f'))

    # Prints sorted_lines
    for date, lines in sorted_list:
        print CORANGE2 + str(counter) + ': ' + CEND + lines
        write_file.write(CORANGE2 + str(counter) + ': ' + CEND + lines + '\n')
        counter = counter + 1

    counter = 0

    # if outfile argument is given - Write sorted_list to file
    if outfile:
        yes = open(outfile, 'w+')
        for date, lines in sorted_list:
            yes.write(CORANGE2 + str(counter) + ': ' + CEND + lines + '\n')
            counter = counter + 1

    # all_tuple is deleted
    del all_tuple[:]


def c2_messages_plexxi_l3(c2, messages, plexxi_l3, outfile=None):
    """Ordered c2, messages, plexxi_L3.
        'outfile' is the name of the file which is to be created and written to"""
    counter = 0
    # Creates /tmp file
    make_tmp_file()
    write_file = open('/tmp/%s/c2_messages_plexxi.txt' % directory, 'w+')

    # Read c2 file
    with open(c2) as myFile:
        data = myFile.readlines()
        for index, line in enumerate(data, 1):
            if line[0].isdigit():
                # Format date/time
                c2_date_info = line.split()[0:1]
                c2_date_string = ' '.join(c2_date_info)
                c2_time_info = line.split()[1:2]
                c2_time_string = ' '.join(c2_time_info)
                c2_string_dt = c2_date_string + ' ' + c2_time_string
                line = CBLUE + 'c2:' + CEND + ' ' + line

                # c2_tuple contains both c2 dateTime, and the log line
                c2_tuple = (c2_string_dt, line)
                # Added to c2_messages_plexxi_l3_tuple
                c2_messages_plexxi_l3_tuple.append(c2_tuple)

    # Read messages file
    with open(messages) as myFile:
        data = myFile.readlines()
        for index, line in enumerate(data, 1):
            # Format date/time
            year = '2020'
            messages_date_info = line.split()[:2]
            messages_date_info.insert(0, year)
            str_date_string = ' '.join(messages_date_info)
            messages_time_info = line.split()[2:3]
            messages_time_string = ' '.join(messages_time_info)
            messages_time_string = messages_time_string.replace(' ', '')
            date_time = str_date_string + ' ' + messages_time_string + '.000'
            messages_date_time = datetime.datetime.strptime(date_time, "%Y %b %d %H:%M:%S.%f")
            messages_string_dt = str(messages_date_time) + '.000'
            line = CTORQ + 'Messages:' + CEND + ' ' + line

            # messages_tuple contains both messages dateTime, and the log line
            messages_tuple = (messages_string_dt, line)
            # Added to c2_messages_plexxi_l3_tuple
            c2_messages_plexxi_l3_tuple.append(messages_tuple)

    # Read plexxi_L3 file
    with open(plexxi_l3) as myFile:
        data = myFile.readlines()
        for index, line in enumerate(data, 1):
            # Format date/time
            year = '2020'
            plexxi_l3_date_info = line.split()[:2]
            plexxi_l3_date_info.insert(0, year)
            str_date_string = ' '.join(plexxi_l3_date_info)
            plexxi_l3_time_info = line.split()[2:3]
            plexxi_l3_time_string = ' '.join(plexxi_l3_time_info)
            plexxi_l3_time_string = plexxi_l3_time_string.replace(' ', '')
            date_time = str_date_string + ' ' + plexxi_l3_time_string + '.000'
            plexxi_l3_date_time = datetime.datetime.strptime(date_time, "%Y %b %d %H:%M:%S.%f")
            plexxi_l3_string_dt = str(plexxi_l3_date_time) + '.000'
            line = CORANGE + 'plexxiL3:' + CEND + ' ' + line

            # plexxi_l3_tuple contains both plexxi_l3 dateTime, and the log line
            plexxi_l3_tuple = (plexxi_l3_string_dt, line)
            # Added to c2_messages_plexxi_l3_tuple
            c2_messages_plexxi_l3_tuple.append(plexxi_l3_tuple)

    # List which sorts all_tuple in chronological order by the date format shown below
    sorted_list = sorted(c2_messages_plexxi_l3_tuple, key=lambda x: datetime.datetime.strptime(x[0], '%Y-%m-%d %H:%M:%S.%f'))

    # Prints sorted_lines
    for date, lines in sorted_list:
        print CORANGE2 + str(counter) + ': ' + CEND + lines
        write_file.write(CORANGE2 + str(counter) + ': ' + CEND + lines + '\n')
        counter = counter + 1

    counter = 0

    # if outfile argument is given - Write sorted_list to file
    if outfile:
        yes = open(outfile, 'w+')
        for date, lines in sorted_list:
            yes.write(CORANGE2 + str(counter) + ': ' + CEND + lines + '\n')
            counter = counter + 1

    # c2_messages_plexxi_l3_tuple is deleted
    del c2_messages_plexxi_l3_tuple[:]


def c2cx(c2, cx, outfile=None):
    """Ordered c2, cx.
            'outfile' is the name of the file which is to be created and written to"""
    counter = 0
    # Creates /tmp file
    make_tmp_file()
    write_file = open('/tmp/%s/c2_cx.txt' % directory, 'w+')

    # Read c2 file
    with open(c2) as myFile:
        data = myFile.readlines()

        for index, line in enumerate(data, 1):
            if line[0].isdigit():
                # Format date/time
                c2_date_info = line.split()[0:1]
                c2_date_string = ' '.join(c2_date_info)
                c2_time_info = line.split()[1:2]
                c2_time_string = ' '.join(c2_time_info)
                c2_string_dt = c2_date_string + ' ' + c2_time_string
                line = CBLUE + 'c2:' + CEND + ' ' + line

                # c2_tuple contains both c2 dateTime, and the log line
                c2_tuple = (c2_string_dt, line)
                # Added to c2_cx_tuple
                c2_cx_tuple.append(c2_tuple)

    # Read cX file
    with open(cx) as myFile:
        data = myFile.readlines()
        for index, line in enumerate(data, 1):
            if line[0].isdigit():
                # Format date/time
                cx_date_info = line.split()[0:1]
                cx_date_string = ' '.join(cx_date_info)
                cx_time_info = line.split()[1:2]
                cx_time_string = ' '.join(cx_time_info)
                cx_string_dt = cx_date_string + ' ' + cx_time_string
                line = CGREENBG2 + 'cx:' + CEND + ' ' + line

                # cx_tuple contains both cx dateTime, and the log line
                cx_tuple = (cx_string_dt, line)
                # Added to c2_cx_tuple
                c2_cx_tuple.append(cx_tuple)

    # List which sorts all_tuple in chronological order by the date format shown below
    sorted_list = sorted(c2_cx_tuple, key=lambda x: datetime.datetime.strptime(x[0], '%Y-%m-%d %H:%M:%S.%f'))

    # Prints sorted_lines
    for date, lines in sorted_list:
        print CORANGE2 + str(counter) + ': ' + CEND + lines
        write_file.write(CORANGE2 + str(counter) + ': ' + CEND + lines + '\n')
        counter = counter + 1
    counter = 0

    # if outfile argument is given - Write sorted_list to file
    if outfile:
        yes = open(outfile, 'w+')
        for date, lines in sorted_list:
            yes.write(CORANGE2 + str(counter) + ': ' + CEND + lines + '\n')
            counter = counter + 1

    # c2_cx is deleted
    del c2_cx_tuple[:]


def command_line_list():

    print " CombinePLexxi Switch, run with option followed by a filename, use -h for more"
    parser = argparse.ArgumentParser()

    # python CombinePlexxiSwitchLogs.py --all/-a FileNameHere
    parser.add_argument('-a', help="Print out timeline for c2, cx, plexxi, plexxi-L3, messages", dest='all', action='store')

    # python CombinePlexxiSwitchLogs.py --c2controlx/-c2cx FileNameHere
    parser.add_argument('-c2cx', help="Print out timeline for c2, cx", dest='c2cx', action='store')

    # python CombinePlexxiSwitchLogs.py --c2messagesplexxiL3/-c2mp FileNameHere
    parser.add_argument('-c2mp', help="Print out timeline for c2, messages, plexxiL3", dest='c2mp', action='store')

    # python CombinePlexxiSwitchLogs.py --list/-l FileNameHere
    parser.add_argument('-l', help="List of timelines", dest='list', action='store')

    args = parser.parse_args()

    # switch_word only exists when on a switch
    switch_word = 'c2platd'

    if args.all:
        # Checks if you are on a switch or not
        check_switch = 'ps -ef | grep c2platd | grep -v grep'
        test = subprocess.Popen(check_switch, shell=True, stdout=subprocess.PIPE)
        text = test.stdout.read()

        # You are on a switch - file paths entered automatically
        if switch_word in text:
            auto_input_c2 = "/var/log/c2/messages.log"
            auto_input_messages = "/var/log/messages"
            auto_input_plexxi_l3 = "/var/log/plexxi-L3"
            auto_input_cx = "/var/log/cxcontrol/messages.log"
            auto_input_plexxi = "/var/log/plexxi"
            all_five(auto_input_c2, auto_input_cx, auto_input_plexxi, auto_input_plexxi_l3, auto_input_messages, args.all)
            print '\n' + CGREEN2 + 'SWITCH WAS FOUND' + CEND

        # You are not on a switch - Must enter file paths manually
        elif switch_word not in text:
            print '\n' + CRED + 'SWITCH NOT FOUND - ENTER MANUALLY' + CEND
            input_c2 = raw_input('Enter full path of c2: ')
            input_cx = raw_input('Enter full path of cx: ')
            input_plexxi = raw_input('Enter full path of plexxi: ')
            input_plexxi_l3 = raw_input('Enter full path of plexxi-L3: ')
            input_messages = raw_input('Enter full path of messages: ')
            all_five(input_c2, input_cx, input_plexxi, input_plexxi_l3, input_messages, args.all)

        print 'File Name: ', CBLUE + args.all + CEND + "  \nTo view file - 'cat %s' \n" % args.all

    if args.c2cx:
        # Checks if you are on a switch or not
        check_switch = 'ps -ef | grep c2platd | grep -v grep'
        test = subprocess.Popen(check_switch, shell=True, stdout=subprocess.PIPE)
        text = test.stdout.read()

        # You are on a switch - file paths entered automatically
        if switch_word in text:
            auto_input_c2 = "/var/log/c2/messages.log"
            auto_input_cx = "/var/log/cxcontrol/messages.log"
            c2cx(auto_input_c2, auto_input_cx, args.c2cx)
            print CGREEN2 + 'SWITCH WAS FOUND' + CEND

        # You are not on a switch - Must enter file paths manually
        elif switch_word not in text:
            print '\n' + CRED + 'SWITCH NOT FOUND - ENTER MANUALLY' + CEND
            input_c2 = raw_input('Enter full path of c2: ')
            input_cx = raw_input('Enter full path of cx: ')
            c2cx(input_c2, input_cx, args.c2cx)

        print 'File Name: ', CBLUE + args.c2cx + CEND + "  \nTo view file - 'cat %s' \n" % args.c2cx

    if args.c2mp:
        # Checks if you are on a switch or not
        check_switch = 'ps -ef | grep c2platd | grep -v grep'
        test = subprocess.Popen(check_switch, shell=True, stdout=subprocess.PIPE)
        text = test.stdout.read()

        # You are on a switch - file paths entered automatically
        if switch_word in text:
            auto_input_c2 = "/var/log/c2/messages.log"
            auto_input_messages = "/var/log/messages"
            auto_input_plexxi_l3 = "/var/log/plexxi-L3"
            c2_messages_plexxi_l3(auto_input_c2, auto_input_messages, auto_input_plexxi_l3, args.c2mp)
            print '\n' + CGREEN2 + 'SWITCH WAS FOUND' + CEND

        # You are not on a switch - Must enter file paths manually
        elif switch_word not in text:
            print '\n' + CRED + 'SWITCH NOT FOUND - ENTER MANUALLY' + CEND
            input_c2 = raw_input('Enter full path of c2: ')
            input_plexxi_l3 = raw_input('Enter full path of plexxi-L3: ')
            input_messages = raw_input('Enter full path of messages: ')
            c2_messages_plexxi_l3(input_c2, input_messages, input_plexxi_l3, args.c2mp)

        print 'File Name: ', CBLUE + args.c2mp + CEND + "  \nTo view file- 'cat %s' \n" % args.c2mp

    if args.list:
        # Checks if you are on a switch or not
        check_switch = 'ps -ef | grep c2platd | grep -v grep'
        test = subprocess.Popen(check_switch, shell=True, stdout=subprocess.PIPE)
        text = test.stdout.read()

        # You are on a switch - file paths entered automatically
        if switch_word in text:
            print '\n' + CGREEN2 + 'SWITCH FOUND' + CEND
            # Provides with user with a list of options
            input_which = input("\n Which timeline would you like to view: \n 1 - c2, cx, plexxi, plexxi-L3, messages \n 2 - c2, messages, plexxi-L3 \n 3 - c2, cx \n 4 - Exit \n\n")

            while (input_which < 5) and (input_which > 0):
                # User entered '1'
                if input_which == 1:
                    auto_input_cx = "/var/log/cxcontrol/messages.log"
                    auto_input_plexxi = "/var/log/plexxi"
                    auto_input_c2 = "/var/log/c2/messages.log"
                    auto_input_messages = "/var/log/messages"
                    auto_input_plexxi_l3 = "/var/log/plexxi-L3"
                    all_five(auto_input_c2, auto_input_cx, auto_input_plexxi, auto_input_plexxi_l3, auto_input_messages, args.list)

                # User entered '2'
                if input_which == 2:
                    auto_input_c2 = "/var/log/c2/messages.log"
                    auto_input_messages = "/var/log/messages"
                    auto_input_plexxi_l3 = "/var/log/plexxi-L3"
                    c2_messages_plexxi_l3(auto_input_c2, auto_input_messages, auto_input_plexxi_l3, args.list)

                # User entered '3'
                if input_which == 3:
                    auto_input_c2 = "/var/log/c2/messages.log"
                    auto_input_cx = "/var/log/cxcontrol/messages.log"
                    c2cx(auto_input_c2, auto_input_cx, args.list)

                # User entered '4'
                if input_which == 4:
                    exit(1)

                print '\n' + CGREEN2 + 'SWITCH WAS FOUND' + CEND + '\n'

                # The list is presented to the user again
                input_which = input("\n Which timeline would you like to view: \n 1 - c2, cx, plexxi, plexxi-L3, messages \n 2 - c2, messages, plexxi-L3 \n 3 - c2, cx \n 4 - Exit \n\n")

        # You are not on a switch - Must enter file paths manually
        elif switch_word not in text:
            print '\n' + CRED + 'SWITCH NOT FOUND - ENTER MANUALLY' + CEND
            # Provides with user with a list of options
            input_which = input("\n Which timeline would you like to view: \n 1 - c2, cx, plexxi, plexxi-L3, messages \n 2 - c2, messages, plexxi-L3 \n 3 - c2, cx \n 4 - Exit \n\n")

            # User will enter paths
            input_c2 = raw_input('Enter full path of c2: ')
            input_cx = raw_input('Enter full path of cx: ')
            input_plexxi = raw_input('Enter full path of plexxi: ')
            input_plexxi_l3 = raw_input('Enter full path of plexxi-L3: ')
            input_messages = raw_input('Enter full path of messages: ')

            while (input_which < 5) and (input_which > 0):
                # User entered '1'
                if input_which == 1:
                    all_five(input_c2, input_cx, input_plexxi, input_plexxi_l3, input_messages, args.list)

                # User entered '2'
                elif input_which == 2:
                    c2_messages_plexxi_l3(input_c2, input_messages, input_plexxi_l3, args.list)

                # User entered '3'
                elif input_which == 3:
                    c2cx(input_c2, input_cx, args.list)

                # User entered '4'
                elif input_which == 4:
                    exit(0)

                print '--------------------------------------------------------------------------------------------------------------------'

                # The list is presented to the user again
                input_which = input("Which timeline would you like to view: \n 1 - c2, cx, plexxi, plexxi-L3, messages \n 2 - c2, messages, plexxi-L3 \n 3 - c2, cx \n 4 - Exit \n\n")

command_line_list()
