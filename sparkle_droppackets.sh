#!/bin/bash

runtime="120 minute"
endtime=$(date -ud "$runtime" +%s)


while [[ $(date -u +%s) -le $endtime ]]
do
    echo "Time Now: `date +%H:%M:%S`"
#ring command for Sparkle rx error
    /opt/plexxi/bin/psh ring "snoopsh show c | grep -e TBCA -e RXCF -e TXCF -e TUCA -e DROP -e RBCA -e RUCA " 
    sleep 5
    /opt/plexxi/bin/psh ring "fabricInfo | grep BUM"
    echo "Sleeping for 10 minutes"
    sleep 600
done


