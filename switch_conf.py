# an object to contain configuration elements for  switch (starting with 5.0)

###############
# switch_conf
###############

class switch_conf(object):
    
    def __init__(self,hostname = ""):
        # Modification here should be avoided.
        self.hostname = hostname
        self.macaddr = ""
        self.switchtype = ""
        self.old2new = {}   # map to contain old interface names to new interface name translations
        self.oldvlanmap = {}   # a map of interface ids to vlans, native and tagged xp format
        self.vlanmap = {}   # a map of interface ids to vlans, native and tagged ##.# format
        self.vlangroupmap = {}   # a map of interface ids to vlangroups
        self.mlag_vlanmap = {}   # a map of interface ids to vlans, native and tagged ##.# format
        self.mlag_list = []  # a list of dict/maps of mlag containing local name, shared name and list of interfaces (port_list)
        self.qsfpmap = {}   # indicates the ports which are in what qsfp mode
        self.speedmap = {}   # indicates the ports which are in what speed (affects qsfp modes) ##.# format
        self.mlag_speedmap = {}   # indicates the ports which are in what speed (affects qsfp modes) ##.# format
        self.oldspeedmap = {}   # indicates the ports which are in what speed (affects qsfp modes) xp format 
        self.adminstatemap = {}   # indicates the ports which are enabled/disabled ##.# format
        self.mlag_adminstatemap = {}   # indicates the ports which are enabled/disabled ##.# format
        self.oldadminstatemap = {}   # indicates the ports which are enabled/disabled xp format
        self.result = -42    # just to put some non-zero clearly invalid value as seed. 
        self.falsehood = False   # By default don't lie.

    def setFALSE(self):
        # For testing, falsify results such that "failure" is triggered, for alert testing etc.
        self.falsehood = True

    def getHOSTNAME(self):
        # Modification here should be avoided.
        return self.hostname

    def setHOSTNAME(self,new_hostname):
        # Modification here should be avoided.
        self.hostname = new_hostname
        return self.hostname

    def getSWITCHTYPE(self):
        # Modification here should be avoided.
        return self.switchtype

    def setSWITCHTYPE(self,new_switchtype):
        # Modification here should be avoided.
        self.switchtype = new_switchtype
        return self.switchtype

    
    def getMACADDR(self):
        # Modification here should be avoided.
        return self.macaddr

    def setMACADDR(self,new_macaddr):
        # Modification here should be avoided.
        self.macaddr = new_macaddr
        return self.macaddr

    def getVLANMAP(self):
        # Modification here should be avoided.
        return self.vlanmap

    def setVLANMAP(self,new_vlanmap):
        # Modification here should be avoided.
        self.vlanmap = new_vlanmap
        return self.vlanmap

    def getVLANGROUPMAP(self):
        # Modification here should be avoided.
        return self.vlangroupmap

    def setVLANGROUPMAP(self,new_vlangroupmap):
        # Modification here should be avoided.
        self.vlangroupmap = new_vlangroupmap
        return self.vlangroupmap

    def getMLAGLIST(self):
        # Modification here should be avoided.
        return self.mlag_list

    def setMLAGLIST(self,new_mlag_list):
        # Modification here should be avoided.
        self.mlag_list = new_mlag_list
        return self.mlag_list

    
    def getMLAGVLANMAP(self):
        # Modification here should be avoided.
        return self.mlag_vlanmap

    def setMLAGVLANMAP(self,new_vlanmap):
        # Modification here should be avoided.
        self.mlag_vlanmap = new_vlanmap
        return self.mlag_vlanmap

    def getOLDVLANMAP(self):
        # Modification here should be avoided.
        return self.oldvlanmap

    def setOLDVLANMAP(self,new_oldvlanmap):
        # Modification here should be avoided.
        self.oldvlanmap = new_oldvlanmap
        return self.oldvlanmap

    def getOLDSPEEDMAP(self):
        # Modification here should be avoided.
        return self.oldspeedmap

    def setOLDSPEEDMAP(self,new_oldspeedmap):
        # Modification here should be avoided.
        self.oldspeedmap = new_oldspeedmap
        return self.oldspeedmap

    def getOLDADMINSTATEMAP(self):
        # Modification here should be avoided.
        return self.oldadminstatemap

    def setOLDADMINSTATEMAP(self,new_oldadminstatemap):
        # Modification here should be avoided.
        self.oldadminstatemap = new_oldadminstatemap
        return self.oldadminstatemap

    
    def getQSFPMAP(self):
        # Modification here should be avoided.
        return self.qsfpmap

    def setQSFPMAP(self,new_qsfpmap):
        # Modification here should be avoided.
        self.qsfpmap = new_qsfpmap
        return self.qsfpmap

    def getSPEEDMAP(self):
        # Modification here should be avoided.
        return self.speedmap

    def setSPEEDMAP(self,new_speedmap):
        # Modification here should be avoided.
        self.speedmap = new_speedmap
        return self.speedmap

    def getADMINSTATEMAP(self):
        # Modification here should be avoided.
        return self.adminstatemap

    def setADMINSTATEMAP(self,new_adminstatemap):
        # Modification here should be avoided.
        self.adminstatemap = new_adminstatemap
        return self.adminstatemap

    def getMLAGSPEEDMAP(self):
        # Modification here should be avoided.
        return self.mlag_speedmap

    def setMLAGSPEEDMAP(self,new_mlag_speedmap):
        # Modification here should be avoided.
        self.mlag_speedmap = new_mlag_speedmap
        return self.mlag_speedmap

    def getMLAGADMINSTATEMAP(self):
        # Modification here should be avoided.
        return self.mlag_adminstatemap

    def setMLAGADMINSTATEMAP(self,new_mlag_adminstatemap):
        # Modification here should be avoided.
        self.mlag_adminstatemap = new_mlag_adminstatemap
        return self.mlag_adminstatemap

    
    def getINT2UUIDMAP(self):
        # Modification here should be avoided.
        return self.INT2UUIDmap

    def setINT2UUIDMAP(self,new_INT2UUIDmap):
        # Modification here should be avoided.
        self.INT2UUIDmap = new_INT2UUIDmap
        return self.INT2UUIDmap

    def getOLD2NEW(self):
        # Modification here should be avoided.
        return self.old2new

    def setOLD2NEW(self,new_old2new):
        # Modification here should be avoided.
        self.old2new = new_old2new
        return self.old2new

    def remap4newint(self):

        for interface in self.oldvlanmap:
            try:
                new_interfacename = self.old2new[interface]
                new_interfacedata = self.oldvlanmap[interface]
                self.vlanmap[new_interfacename] = new_interfacedata
            except KeyError:
                mlag_interfacename = interface
                mlag_interfacedata = self.oldvlanmap[interface]
                self.mlag_vlanmap[mlag_interfacename] = mlag_interfacedata
        #print("vlanmap inside: %s" % self.vlanmap)
        #print("mlagvlanmap inside: %s" % self.mlag_vlanmap)
                
        for interface in self.oldspeedmap:
            try:
                new_interfacename = self.old2new[interface]
                new_interfacedata = self.oldspeedmap[interface]
                self.speedmap[new_interfacename] = new_interfacedata
            except KeyError:
                mlag_interfacename = interface
                mlag_interfacedata = self.oldspeedmap[interface]
                self.mlag_vlanmap[mlag_interfacename] = mlag_interfacedata
                

        for interface in self.oldadminstatemap:
            try:
                new_interfacename = self.old2new[interface]
                new_interfacedata = self.oldadminstatemap[interface]
                self.adminstatemap[new_interfacename] = new_interfacedata
            except KeyError:
                mlag_interfacename = interface
                mlag_interfacedata = self.oldadminstatemap[interface]
                self.mlag_vlanmap[mlag_interfacename] = mlag_interfacedata

        old_mlag_list = self.getMLAGLIST()
        #print("old_mlag_list %s" % old_mlag_list)
        new_mlag_list = []        
        new_mlag_set = {}
        new_port_list = []
        for mlag_set in old_mlag_list:
            new_mlag_set["local_lag_name"] = mlag_set["local_lag_name"]
            new_mlag_set["shared_lag_name"] = mlag_set["shared_lag_name"]
            for port_name in mlag_set["port_list"]:
                new_port_list.append(self.old2new[port_name])
            #print("new_port_list %s" % new_port_list)
            new_mlag_set["port_list"] = new_port_list
            new_mlag_list.append(new_mlag_set)
            new_port_list = []
            new_mlag_set = {}

            
        self.setMLAGLIST(new_mlag_list)
                
        return self.vlanmap

    

    
###############
# END switch_conf
###############
