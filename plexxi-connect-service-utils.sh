#!/usr/bin/env bash
##########################################################################
#
# Copyright (c) 2018, Plexxi Inc. and its licensors.
#
# All rights reserved.
#
# Use and duplication of this software is subject to a separate license
# agreement between the user and Plexxi or its licensor.
#
##########################################################################

# Library of systemd service utility functions.

# List of Plexxi Connect-owned services (including StackStorm) managed by systemd.
CONNECT_SERVICES=(nginx.service plexxiconnectapi.service expressjs.service \
                  st2actionrunner.service st2api.service st2auth.service \
                  st2garbagecollector.service st2notifier.service st2resultstracker.service \
                  st2rulesengine.service st2sensorcontainer.service mistral.service \
                  mistral-server.service mistral-api.service)

# List of services that StackStorm depends on, managed by systemd.
ST2_DEPENDENT_SERVICES=(mongod.service postgresql-9.6.service rabbitmq-server.service redis.service)

# List of Plexxi Connect processes managed under systemd
SERVICES=("${CONNECT_SERVICES[@]}" "${ST2_DEPENDENT_SERVICES[@]}")

function plexxi_connect_services_are_running()
{
    service_stopped=false
    for service in ${SERVICES[@]}
    do
        status=$(systemctl is-active ${service})
        if [ "${status}" != "active" ]; then
            if [[ $1 == "warn" ]]; then
                echo "Detected that service $service is not running. You can attempt to manually"
                echo "start the process with 'sudo systemctl start $service'. If it does not start"
                echo "please contact Plexxi Support."
                service_stopped=true
            elif [[ "$status" == "" ]]; then
                # Noop - status is empty if Unit is not found.
                :
            else
                service_stopped=true
            fi
        fi
    done

    if [ "$service_stopped" = true ] ; then
        return 1
    fi
    return 0
}

function plexxi_connect_services_are_stopped()
{
    service_running=false
    for service in ${SERVICES[@]}
    do
        status=$(systemctl is-active ${service})
        if [ "${status}" == "active" ] ; then
            if [[ $1 == "warn" ]]; then
                LOG_FILE=$2
                if [ -f ${LOG_FILE} ]; then
                    echo "Detected that service $service is still running. You can attempt to manually" >> ${LOG_FILE}
                    echo "stop the process with 'sudo systemctl stop $service'. If it does not stop" >> ${LOG_FILE}
                    echo "please contact Plexxi Support." >> ${LOG_FILE}
                else
                    echo "Detected that service $service is still running. You can attempt to manually"
                    echo "stop the process with 'sudo systemctl stop $service'. If it does not stop"
                    echo "please contact Plexxi Support."
                fi
            fi
            service_running=true
        fi
    done

    if [ "$service_running" = true ] ; then
        return 1
    fi
    return 0
}

function disable_and_kill_plexxi_connect_service()
{
    LOG_FILE=$1
    service=$2
    status=$(systemctl is-active ${service})
    if [ "${status}" == "active" ] ; then
        if [ -f ${LOG_FILE} ]; then
            echo "Detected that service $service is still running. Stopping with SIGKILL/SIGINT.."  >> ${LOG_FILE}
        else
            echo "Detected that service $service is still running. Stopping with SIGKILL/SIGINT..."
        fi
        systemctl disable ${service}
        if [ ${service} == "mongod.service" ];then
            # Special case for mongod - don't use SIGKILL use SIGINT instead
            systemctl kill -s SIGINT ${service}
        else
            systemctl kill -s SIGKILL ${service}
        fi
        if [ $? -ne 0 ]; then
            if [ -f ${LOG_FILE} ]; then
                echo "Kill on ${service} failed." >> ${LOG_FILE}
            else
                echo "Kill on ${service} failed."
            fi
            return 1
        fi
    fi
    return 0
}

function disable_and_kill_all_plexxi_connect_services()
{
    LOG_FILE=$1
    return_value=0
    if [ -f ${LOG_FILE} ]; then
        echo "Kill all connect services if they are running."  >> ${LOG_FILE}
    else
        echo "Kill all connect services if they are running."
    fi
    for service in ${SERVICES[@]}
    do
        disable_and_kill_plexxi_connect_service ${LOG_FILE} ${service}
        if [ $? -ne 0 ]; then
            return_value=1
        fi
    done

    return ${return_value}
}

function shutdown_plexxi_connect_services()
{
    REQUESTED_SERVICES=("$@")
    return_value=0

    for service in ${REQUESTED_SERVICES[@]}
    do
        echo "Shutdown service ( ${service} ).."
        systemctl stop ${service} >> /dev/null 2>&1
        if [ $? -ne 0 ]; then
            echo "Failed to shutdown ${service}"
            return_value=1
        fi
    done
    return ${return_value}
}

function start_plexxi_connect_services()
{
    if [ "$(id -u)" != "0" ]; then
        echo "This script must be run as root"
        exit 1
    fi

    echo "Starting all Plexxi Connect services"
    /opt/plexxi/python/bin/salt-call state.highstate >> /var/log/plexxiconnect-salt-highstate.log 2>&1
    return 0
}

function stop_plexxi_connect_services()
{
    if [ "$(id -u)" != "0" ]; then
        echo "This script must be run as root"
        exit 1
    fi

    echo "Stopping Plexxi Connect services"

    # Permanently stop services that aren't needed after the upgrade
    for service in postgresql-9.5.service
    do
        systemctl status ${service} > /dev/null 2>&1
        if [ $? -eq 0 ] ; then
            echo "Permanently stopping ${service}"
            systemctl disable --now ${service}
            if [ $? -ne 0 ] ; then
                echo "Failed to stop ${service}"
                return 1
            fi
        fi
    done

    # Special case: Attempt to stop mistral-server gracefully by sending 2 SIGTERM's in order to
    # cancel both of the executor_server instances.  If for some reason this is not successful
    # then it will be canceled forcefully with a SIGINT signal from the service stop command.
    # (PXPSE-1882)
    MISTRAL_SERVER_PID=`pgrep mistral-server`
    if [ -n "${MISTRAL_SERVER_PID}" ]; then
        kill -SIGTERM ${MISTRAL_SERVER_PID}
        sleep 1
        kill -SIGTERM ${MISTRAL_SERVER_PID}
    fi

    # Stop all connect and stack storm services.
    shutdown_plexxi_connect_services "${CONNECT_SERVICES[@]}"
    if [ $? -ne 0 ]; then
        echo "Failed to stop connect services"
        return 1
    fi

    # Stop all thirdparty services used by stackstorm
    shutdown_plexxi_connect_services "${ST2_DEPENDENT_SERVICES[@]}"
    if [ $? -ne 0 ]; then
        echo "Failed to stop st2 services"
        return 1
    fi

    return 0
}

function kill_plexxi_connect_services()
{
    if [ "$(id -u)" != "0" ]; then
        echo "This script must be run as root"
        exit 1
    fi

    echo "Killing Plexxi Connect services"
    # Stop all services running under the plexxiconnect.service
    systemctl stop plexxiconnect.service

    for service in ${SERVICES[@]}
    do
        status=$(systemctl is-active ${service})
        if [ "${status}" == "active" ] ; then
            echo "Detected that service $service is still running. Stopping with SIGKILL..."
            systemctl disable ${service}
            systemctl kill -s SIGKILL ${service}
        fi
    done
    return 0
}

disable_plexxi_connect_services()
{
    DISABLE_SERVICES=("${SERVICES[@]}")
    DISABLE_SERVICES+=("cancelst2actions.service")
    echo "Disabling Plexxi Connect services for first boot configuration"
    for service in ${DISABLE_SERVICES[@]}
    do
        systemctl disable ${service} > /dev/null 2>&1
        if [ $? -ne 0 ]; then
            echo "Failed to disable ${service}"
            return 1
        fi
    done
    return 0
}
