#!/usr/bin/env python
import logging
import sys

# Create a logger for this file/module, this does not need to be any relevant fie name, just call it the same as your script
logger = logging.getLogger('tims_script')
# Set the root logging level. Verbosity.
logger.setLevel(logging.DEBUG) # this is the lowest, and means you shoudl capture everything


# Now to log to file: we create a file handler
# 'custom_script.log' is the filename. mode 'w' means create new and overwrite. If you wanted to append use 'a' or remove 'mode='
fh = logging.FileHandler('custom_script.log', mode='w')
fh.setLevel(logging.DEBUG)


# create console handler (stdout) with a higher log level
# so debug messages dont get sent to stdout
ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.INFO)

# Formatter. See logging cookbook etc for formats
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
# Optionally set the same format (or different)
#ch.setFormatter(formatter)
fh.setFormatter(formatter)

logger.addHandler(fh)
logger.addHandler(ch)


def run():
    logger.info("Start of script.") #What you should do is replace your script 'print' with this or debug
    logger.debug("Some extra debug info that wont show up in the console")
    logger.error("Something went wrong.") 


if __name__ == '__main__':
    run()