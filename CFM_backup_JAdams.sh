export PYTHONIOENCODING=utf8

# Get Auth Token

AUTH=$(curl --insecure -X POST "https://172.26.242.200/api/auth/token" -H "accept: application/json; version=1.0" -H "X-Auth-Username: admin" -H "X-Auth-Password: plexxi" -H "Content-Type: application/json" | python -c "import sys, json; print json.load(sys.stdin)['result']")

 

# Execute Backup and get Backup filename from URL

BACKUPFILE=$(curl --insecure -X POST "https://172.26.242.200/api/backups" -H "accept: application/json; version=1.0" -H "Content-Type: application/json" -H "Authorization: $AUTH" | python -c "import sys, json; print json.load(sys.stdin)['result']['url']")

 

# Strip last part of URL out (past ?)

BACKUPFILE=$(echo $BACKUPFILE | cut -f1 -d"?")

 

# Change Filename path to real path

BACKUPFILE=${BACKUPFILE/'/api/v1/files'/'/var'}

 

# Output SFTP commands to a Batch File

echo "cd cfm_backups" > backup_batch.txt

echo "put $BACKUPFILE" >> backup_batch.txt

 

# execute the SFTP

sftp -b backup_batch.txt administrator@172.26.242.199 > backup.out

# Check backup OK

if [[ $? != 0 ]]; then

  echo "Backup of CFM has failed!"

  exit 1

fi