#!/usr/bin/python 

##########################################################################################
#
# A module to make REST API calls easier.
#
##########################################################################################


import os,sys,socket,re,datetime,subprocess,shutil,time,requests,json,debug_util
# not sure if I need this
#import debug_util

# otherwise we get unncessary warnings due to insecure/unverfied/unsigned certs.
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

def pnln(args):
    print(args)
    #logging.debug(args)
    return True

def pnl(args):
    sys.stdout.write(args)
    #logging.debug(args)
    return True




################
# ctrlCharStrip
################

def escape_ansi(line):
    ansi_escape = re.compile(r'(\x9B|\x1B\[)[0-?]*[ -\/]*[@-~]')
    return ansi_escape.sub('', line)

################
# ctrlCharStrip END
################



################
# collectPortMap_CX
################

def collectPortMap_CX(MAChost):

        
    targetURL = "https://localhost:443/cxcontrol/rest/api/1/controllers/1/switches/" + MAChost + "/ports"

    rawportMAP = getRESTdata(targetURL)
        
    portMAP = rawportMAP['ports']
    
    return portMAP
    
################
# collectPortMap_CX END
################


#############
# writeVLANS_CX
#############

# Lets put the valid date finally obtained into the file

def writeVLANS_CX(portLIST,vlanIDs):

    interfacelist = []
    for port in portLIST:
        interfacelist.append( {"id": port["id"] , "interface_type" : "port"} )

    vlanIDlist = []
    for vlanID in vlanIDs:
        vlanIDlist.append(int(vlanID))
        
    VLANS_CONF_map =  { "vlans":
                        [
                         {
                             "interfaces": interfacelist,
                             "s_vlan": 0,
                             "user_label": "px-setup",
                             "vlan_group_id": "px-setup",
                             "vlan_tags": vlanIDlist
                         }
                        ]
    }


    
    return VLANS_CONF_map


#############
# writeVLANS_CX END
#############

                            
#############
# writeNATIVEVLAN_CX
#############

# Lets put the valid date finally obtained into the file

def writeNATIVEVLAN_CX(portLIST,nativeVLAN):

    interfaceLIST = []    
    for port in portLIST:
        interface = {}
        interface["id"] =  port
        interface["interface_type"] =  "port"
        interfaceLIST.append(interface)


    
    NATIVEVLAN_CONF_map = { "native_vlans":
            [
                {
                    "native_vlan" : nativeVLAN,
                    "interfaces" : interfaceLIST 
                }
            ]
    }
    # {"native_vlans": [{"native_vlan": 6, "interfaces": [{"interface_type": "port", "id": "102"}, {"interface_type": "port", "id": "3"}  ]}]}
    
#    NATIVEVLAN_CONF_map = { "native_vlans": [ {"native_vlan" : nativeVLAN, { "ports" : portLIST } ] }
                            
    
    return NATIVEVLAN_CONF_map


#############
# writeNATIVEVLAN_CX END
#############



###########
# patchRESTdata, handles the boiler plate rest stuff
###########


def patchRESTdata(targetURL,json_data,token = ""):

    #Need to add stuff to URL for PUT
    targetURL = targetURL + "?request_id=idontmatter"
    custheaders = {"Authorization" : token, "Content-Type" : "application/json", "accept":"application/json; version=1.0"}
    #pnl("CXjson = %s" % CXjson)
    #custheaders = {"Authorization" : token, "accept":"application/json; version=1.0"}
    try:
        currentdata = requests.patch(targetURL, json=json_data, headers = custheaders , verify = False)
        if (currentdata.status_code != 200):
            pnln("Got undesired response %s" % currentdata.status_code)
            pnln("content %s" % currentdata.content)
    except:
        pnln(custheaders)
        pnln("got exception from put for %s of %s" % (targetURL,currentdata))

    try:
        json_result = json.loads(currentdata.content)
        result = json_result["result"]
    except:
        result = ""

    return result


###########
# patchRESTdata END
###########




###########
# setRESTdata, handles the boiler plate rest stuff
###########


def setRESTdata(targetURL,json_data,token = ""):

    #Need to add stuff to URL for PUT
    targetURL = targetURL + "?request_id=idontmatter"
    custheaders = {"Authorization" : token, "Content-Type" : "application/json", "accept":"application/json; version=1.0"}
    #pnl("CXjson = %s" % CXjson)
    #custheaders = {"Authorization" : token, "accept":"application/json; version=1.0"}
    try:
        currentdata = requests.put(targetURL, json=json_data, headers = custheaders , verify = False)
        if (currentdata.status_code != 200):
            pnln("Got undesired response %s" % currentdata.status_code)            
    except:
        pnln(custheaders)
        pnln("got exception from put for %s of %s" % (targetURL,currentdata))

    try:
        json_result = json.loads(currentdata.content)
        result = json_result["result"]
    except:
        result = ""

    return result


###########
# setRESTdata END
###########

###########
# postRESTdata, handles the boiler plate rest stuff
###########


def postRESTdata(targetURL,json_data,token = ""):

    #Need to add stuff to URL for PUT
    targetURL = targetURL + "?request_id=idontmatter"
    custheaders = {"Authorization" : token, "Content-Type" : "application/json", "accept":"application/json; version=1.0"}
    #pnl("CXjson = %s" % CXjson)
    #custheaders = {"Authorization" : token, "accept":"application/json; version=1.0"}
    try:
        currentdata = requests.post(targetURL, json=json_data, headers = custheaders , verify = False)
        if (currentdata.status_code != 200):
            pnln("Got undesired response %s" % currentdata.status_code)            
    except:
        pnln(custheaders)
        pnln("got exception from put for %s of %s" % (targetURL,currentdata))

    try:
        json_result = json.loads(currentdata.content)
        result = json_result["result"]
    except:
        result = ""

    return result


###########
# postRESTdata END
###########


###########
# getRESTdata, handles the boiler plate rest stuff
###########


def getRESTdata(targetURL,token = ""):
    currentdata_set = {}
    try:
        currentdata = requests.get(targetURL, verify = False)
        if (currentdata.status_code != 200):
            pnln("Got undesired response %s" % currentdata.status_code)            
        currentdata_set = currentdata.json()
    except:
        pnln("got exception from get for %s\nresults: %s" % (targetURL,currentdata_set))        
        
    return currentdata_set

###########
# getRESTdata END
###########


###########
# deleteRESTdata, handles the boiler plate rest stuff
###########


def deleteRESTdata(targetURL, token = ""):
    currentdata = {}
    try:
        currentdata = requests.delete(targetURL, verify = False)
        #pnl("got json data %s" % (currentdata))
    except:
        pnln("got exception from get for %s" % targetURL)
        
    return currentdata

###########
# deleteRESTdata END
###########


###########
# getFABRIC, handles the boiler plate rest stuff
###########


def getFABRIC(targetADDR,token = ""):
    targetURL = "https://" + targetADDR + "/api/fabrics?count_only=false&only_with_switches=true&switches=true&tags=true&settings=true"
    currentdata = {}
    custheaders = {"Authorization" : token, "accept":"application/json; version=1.0"}
    try:
        currentdata = requests.get(targetURL, headers=custheaders, verify=False)
        #pnl("got json data %s" % (currentdata))
    except:
        pnln(custheaders)
        pnln("got exception from get for %s of %s" % (targetURL,currentdata))

    try:
        json_result = json.loads(currentdata.content)
        result = json_result["result"]
    except:
        result = ""

    return result

###########
# getFABRIC END
###########

###########
# getLAGs
###########


def getLAGs(targetADDR,token = "",fabric_uuid = ""):

    targetURL = "https://" + targetADDR + "/api/lags?count_only=false&fabric=" + str(fabric_uuid) + "&mac_attachments=false&mac_learning=false&ports=false&port_type=access&tags=false&type=provisioned&vlan_groups=true"
    currentdata = {}
    custheaders = {"Authorization" : token, "accept":"application/json; version=1.0"}
    try:
        currentdata = requests.get(targetURL, headers=custheaders, verify=False)
        #pnl("got json data %s" % (currentdata))
    except:
        pnln(custheaders)
        pnln("got exception from get for %s of %s" % (targetURL,currentdata))

    try:
        json_result = json.loads(currentdata.content)
        result = json_result["result"]
    except:
        result = ""

    return result

###########
# getLAGs END
###########



###########
# getACCESSPORTS, handles the boiler plate rest stuff
###########


def getACCESSPORTS(targetADDR,token = "",switch_uuid = ""):
    targetURL = "https://" + targetADDR + "/api/ports?count_only=false&is_uplink=false&lag=true&port_security_enabled=false&switches=" + switch_uuid + ",&tags=true&vlan_groups=true&type=access&neighbors=true" 
    currentdata = {}
    custheaders = {"Authorization" : token, "accept":"application/json; version=1.0"}
    try:
        currentdata = requests.get(targetURL, headers=custheaders, verify=False)
        print("%s" % targetURL)
    except:
        pnln(custheaders)
        pnln("got exception from get for %s of %s" % (targetURL,currentdata))

    try:
        json_result = json.loads(currentdata.content)
        result = json_result["result"]
    except:
        result = ""

    return result

###########
# getACCESSPORTS END
###########

###########
# getACCESSPORTSsec, handles the boiler plate rest stuff
###########


def getACCESSPORTSsec(targetADDR,token = "",switch_uuid = ""):
    targetURL = "https://" + targetADDR + "/api/ports?count_only=false&is_uplink=false&lag=true&port_security_enabled=true&switches=" + switch_uuid + ",&tags=true&vlan_groups=true&type=access&neighbors=true" 
    currentdata = {}
    custheaders = {"Authorization" : token, "accept":"application/json; version=1.0"}
    try:
        currentdata = requests.get(targetURL, headers=custheaders, verify=False)
        print("%s" % targetURL)
    except:
        pnln(custheaders)
        pnln("got exception from get for %s of %s" % (targetURL,currentdata))

    try:
        json_result = json.loads(currentdata.content)
        result = json_result["result"]
    except:
        result = ""

    return result

###########
# getACCESSPORTSsec END
###########



###########
# getACCESSPORTSup, handles the boiler plate rest stuff
###########


def getACCESSPORTS_UPLINK(targetADDR,token = "",switch_uuid = ""):
    targetURL = "https://" + targetADDR + "/api/ports?count_only=false&is_uplink=true&lag=true&port_security_enabled=false&switches=" + switch_uuid + ",&tags=true&vlan_groups=true&type=access&neighbors=true" 
    currentdata = {}
    custheaders = {"Authorization" : token, "accept":"application/json; version=1.0"}
    try:
        currentdata = requests.get(targetURL, headers=custheaders, verify=False)
        print("%s" % targetURL)
    except:
        pnln(custheaders)
        pnln("got exception from get for %s of %s" % (targetURL,currentdata))

    try:
        json_result = json.loads(currentdata.content)
        result = json_result["result"]
    except:
        result = ""

    return result

###########
# getACCESSPORTSup END
###########

###########
# getFABRICPORTS 
###########


def getFABRICPORTS(targetADDR,token = "",switch_uuid = ""):
    targetURL = "https://" + targetADDR + "/api/ports?count_only=false&is_uplink=false&lag=true&port_security_enabled=false&switches=" + switch_uuid + ",&tags=true&vlan_groups=true&type=fabric&neighbors=true" 
    currentdata = {}
    custheaders = {"Authorization" : token, "accept":"application/json; version=1.0"}
    try:
        currentdata = requests.get(targetURL, headers=custheaders, verify=False)
        print("%s" % targetURL)
    except:
        pnln(custheaders)
        pnln("got exception from get for %s of %s" % (targetURL,currentdata))

    try:
        json_result = json.loads(currentdata.content)
        result = json_result["result"]
    except:
        result = ""

    return result

###########
# getFABRICPORTS END
###########


###########
# getFABRICPORTSup, handles the boiler plate rest stuff
###########


def getFABRICPORTS_UPLINK(targetADDR,token = "",switch_uuid = ""):
    targetURL = "https://" + targetADDR + "/api/ports?count_only=false&is_uplink=true&lag=true&port_security_enabled=false&switches=" + switch_uuid + ",&tags=true&vlan_groups=true&type=fabric&neighbors=true" 
    currentdata = {}
    custheaders = {"Authorization" : token, "accept":"application/json; version=1.0"}
    try:
        currentdata = requests.get(targetURL, headers=custheaders, verify=False)
        print("%s" % targetURL)
    except:
        pnln(custheaders)
        pnln("got exception from get for %s of %s" % (targetURL,currentdata))

    try:
        json_result = json.loads(currentdata.content)
        result = json_result["result"]
    except:
        result = ""

    return result

###########
# getFABRICPORTSup END
###########


###########
# getTOKEN, handles the boiler plate rest stuff
###########


def getTOKEN(targetADDR,username,password):
    targetURL = "https://" + targetADDR + "/api/auth/token"
    currentdata = {}
    custheaders = {'X-Auth-Username' : username, 'X-Auth-Password' : password, "Content-Type":"application/json", "accept":"application/json; version=1.0"}
    try:
        print("user: %s pass: %s" % (username,password))
        currentdata = requests.post(targetURL, headers=custheaders, verify = False)
        #pnl("got json data %s" % (currentdata))
    except:
        pnln("got exception from get for %s of %s" % (targetURL,currentdata))

    try:
        json_result = json.loads(currentdata.content)
        result = json_result["result"]
    except:
        result = ""

    return result

###########
# getTOKEN END
###########




def setHOSTADDRviaREST():
    # Configure hostnames and mgmt ip addresses.
    # HOSTNAME
    if (thisHostADDRstring):
        pnl("Hostname and MGMT IP Address...")
        pnl("applying configuration to ... %s/%s ..." % (thisHostADDRstring,hostname))
        
        targetURL = "https://localhost:443/cxcontrol/rest/api/1/controllers/1/switches/" + MAChost + "/hostname_mgmt"
        hostCXjson = writeHOSTADDR_CX(hostname,thisHostMgmtData)
        logging.debug("hostCXjson formed %s" % hostCXjson)
        
        newHOSTdataRES = setRESTdata(targetURL,hostCXjson)
                
        pnl("done.\n")
    else:        
        pnl("No Hosts found to configure\n")


    ## GENERAL aka HOST NON-SPECIFIC SECTION, though applied to target set only ###

    # NTP
    commandX = ""
    raw_resultX = ""
    sys.stdout.write("NTP...")
    
    if (NTPstring):
        pnl("applying configuration to ... %s/%s ..." % (thisHostADDRstring,hostname))
            
        targetURL = "https://localhost:443/cxcontrol/rest/api/1/controllers/1/switches/" + MAChost + "/ntp"
        ntpCXjson = writeNTP_CX(NTPstring)
        logging.debug("ntpCXjson formed %s" % ntpCXjson)

        newNTPdataRES = setRESTdata(targetURL,ntpCXjson)
                
        pnl("done.\n")
    else:        
        pnl("seems you are not altering NTP configuration...done.\n")



    return "Success"






###########
# setPORTqsfp 
###########


def setPORTqsfp(targetADDR,token = "",
            newport = {}):
        

    result = {}

    try:
        # This is why this is special, API demands these NOT be present
        del newport["vlan_groups"]
        del newport["lag"]
        # This is why this is special, API demands these NOT be present
        
        port_config_MAP = newport #json.loads(newport)
        # print("port config: %s" % port_config_MAP)
        targetURL = "https://" + targetADDR + "/api/ports/" + port_config_MAP["uuid"] 


        result = setRESTdata(targetURL,port_config_MAP,token)
    except:
        print("Error in setPORTqsfp")
        
    return result


    
###########
# setPORTqsfp END
###########

###########
# setPORT 
###########


def setPORT(targetADDR,token = "",
            newport = {}):
        
    # port_config_MAP = {
    #     "native_vlan": native_vlan,
    #     "speed": {
    #         "current": speed_current
    #     },
    #     "switch_name": switch_name,
    #     "vlan_group_uuids": vlan_group_uuids,
    #     "description": description,
    #     "admin_state": admin_state,
    #     "qsfp_mode": qsfp_mode,
    #     "fec": fec,
    #     "name": name,
    #     "is_uplink": is_uplink,
    #     "bridge_loop_detection": {
    #         "interval": bridge_loop_detection_interval,
    #         "mode": bridge_loop_detection_state
    #     },
    #     "ungrouped_vlans": ungrouped_vlans
    # }
    result = {}

    try:
        del newport["vlan_groups"]
        del newport["lag"]
        port_config_MAP = newport #json.loads(newport)
        #print("port config: %s" % port_config_MAP)
        targetURL = "https://" + targetADDR + "/api/ports/" + port_config_MAP["uuid"] 


        result = setRESTdata(targetURL,port_config_MAP,token)
    except:
        print("Error in setPORT")
        
    return result


    
###########
# setPORT END
###########


###########
# patchPORT 
###########


def patchPORT(targetADDR,token = "",
              uuids = [],
              state = "",
              speed = 0,
              description = "",
              name = "",
              native_vlan = 0,
              ungrouped_vlans = "",
              vlan_group_uuids = [],
              port_type = "",
              fec = "",
              qsfp_mode = "",
              is_uplink = "",
              bld_mode = "",
              bld_interval = 0):

    if (len(uuids) < 1):
        return -1
    
    patch_list = []
    if (state != ""):
        state_json =  {
            "path": "/admin_state",
            "value": state,
            "op": "replace"
        }
        patch_list.append(state_json)

    if (speed != 0):
        speed_json =  {
            "path": "/speed/current",
            "value": speed,
            "op": "replace"
        }
        patch_list.append(speed_json)
                
    if (name != ""):
        name_json =  {
            "path": "/name",
            "value": name,
            "op": "replace"
        }
        patch_list.append(name_json)

    if (description != ""):
        description_json =  {
            "path": "/description",
            "value": description,
            "op": "replace"
        }
        patch_list.append(description_json)
        
    if (native_vlan != 0):
        native_vlan_json =  {
            "path": "/native_vlan",
            "value": native_vlan,
            "op": "replace"
        }
        patch_list.append(native_vlan_json)

    if (ungrouped_vlans != ""):
        ungrouped_vlans_json =  {
            "path": "/ungrouped_vlans",
            "value": ungrouped_vlans,
            "op": "replace"
        }
        patch_list.append(ungrouped_vlans_json)

    if (len(vlan_group_uuids) != 0):
        vlan_group_uuids_json =  {
            "path": "/vlan_group_uuids",
            "value": vlan_group_uuids,
            "op": "replace"
        }
        patch_list.append(vlan_group_uuids_json)

    if (port_type != ""):
        port_type_json =  {
            "path": "/port_type",
            "value": port_type,
            "op": "replace"
        }
        patch_list.append(port_type_json)

    if (fec != ""):
        fec_json =  {
            "path": "/fec",
            "value": fec,
            "op": "replace"
        }
        patch_list.append(fec_json)

    if (qsfp_mode != ""):
        qsfp_mode_json =  {
            "path": "/qsfp_mode",
            "value": qsfp_mode,
            "op": "replace"
        }
        patch_list.append(qsfp_mode_json)

    if (is_uplink != ""):
        is_uplink_json =  {
            "path": "/is_uplink",
            "value": is_uplink,
            "op": "replace"
        }
        patch_list.append(is_uplink_json)

    if (bld_mode != ""):
        bld_mode_json =  {
            "path": "/bld_mode",
            "value": bld_mode,
            "op": "replace"
        }
        patch_list.append(bld_mode_json)

    if (bld_interval != 0):
        bld_interval_json =  {
            "path": "/bld_interval",
            "value": bld_interval,
            "op": "replace"
        }
        patch_list.append(bld_interval_json)

    print("type of uuid %s" % type(uuids))
    
    port_config_MAP = {}

    port_config_MAP["uuids"] =  uuids
    port_config_MAP["patch"] = patch_list

    #port_config_MAP = {"uuids": ["00df62bafe474aa1b80e442f205e80f0"], "patch": [{"path": "/admin_state", "value": "enabled", "op": "replace"}, {"path": "/speed", "value": 25000, "op": "replace"}, {"path": "/native_vlan", "value": 1, "op": "replace"}]}
    #port_config_MAP = [ { "uuids": [ "004719af2e534705b56d6ff5ccc8a890", "00d4d2e399a54f929de27775ef046bb0" ], "patch": [ { "path": "/admin_state", "value": "enabled", "op": "replace" }, { "path": "/speed/current", "value": 10000, "op": "replace" }, { "path": "/native_vlan", "value": 4, "op": "replace" } ] }]

    targetURL = "https://" + targetADDR + "/api/ports"

    #print("port config: %s" % port_config_MAP)
    
    result = patchRESTdata(targetURL,[port_config_MAP],token)
    
    return result


    
###########
# patchPORT END
###########

###########
# postLAG
###########


def postLAG(targetADDR,token = "",
            newlag = {}):
        
    # lag_config_MAP = 
    #     {
    #   "native_vlan": 99,
    #   "description": "",
    #   "mac_learning_configuration": {
    #     "learning_limit": 1,
    #     "aging": false,
    #     "violation_action": "restrict",
    #     "mode": "dynamic"
    #   },
    #   "mac_learning_attachments": [
    #     {
    #       "vlan": 1000,
    #       "mac_address": "00:50:56:aa:bb:dd"
    #     }
    #   ],
    #   "vlan_group_uuids": [
    #     "6c8ee2a1a0284f308b3c0f4156f6d042"
    #   ],
    #   "lacp_fallback": {
    #     "port_uuid": "1c35f4450e2c4d0ab8fddae75924567e",
    #     "timeout": 20
    #   },
    #   "mac_learning_use_default_configuration": false,
    #   "port_properties": [
    #     {
    #       "lacp": {
    #         "priority": 100,
    #         "intervals": {
    #           "slow": 30,
    #           "fast": 1
    #         },
    #         "aggregate_port_limits": {
    #           "minimum": 2,
    #           "maximum": 8
    #         },
    #         "mode": "active"
    #       },
    #       "speed": {
    #         "current": 25000
    #       },
    #       "port_uuids": [
    #         "03734100040c0000399e081dc0ca985b"
    #       ]
    #     }
    #   ],
    #   "ungrouped_vlans": "5, 10-20, 30, 40, 50-60",
    #   "name": "LinkAggregationGroup	#01"
    # }

    result = {}

    try:
        lag_config_MAP = newlag #json.loads(newlag)
        #print("lag config: %s" % lag_config_MAP)
        targetURL = "https://" + targetADDR + "/api/lags"
        
        result = postRESTdata(targetURL,lag_config_MAP,token)
    except:
        print("Error in postLAG : %s" % result)
        
    return result


    
###########
# postLAG END
###########

###########
# putLAG  # different from postLAG in that a UUID of exist lag is included.
###########


def putLAG(targetADDR,token = "",
            newlag = {},uuid = ""):
        
    # lag_config_MAP = 
    #     {
    #   "native_vlan": 99,
    #   "description": "",
    #   "mac_learning_configuration": {
    #     "learning_limit": 1,
    #     "aging": false,
    #     "violation_action": "restrict",
    #     "mode": "dynamic"
    #   },
    #   "mac_learning_attachments": [
    #     {
    #       "vlan": 1000,
    #       "mac_address": "00:50:56:aa:bb:dd"
    #     }
    #   ],
    #   "vlan_group_uuids": [
    #     "6c8ee2a1a0284f308b3c0f4156f6d042"
    #   ],
    #   "lacp_fallback": {
    #     "port_uuid": "1c35f4450e2c4d0ab8fddae75924567e",
    #     "timeout": 20
    #   },
    #   "mac_learning_use_default_configuration": false,
    #   "port_properties": [
    #     {
    #       "lacp": {
    #         "priority": 100,
    #         "intervals": {
    #           "slow": 30,
    #           "fast": 1
    #         },
    #         "aggregate_port_limits": {
    #           "minimum": 2,
    #           "maximum": 8
    #         },
    #         "mode": "active"
    #       },
    #       "speed": {
    #         "current": 25000
    #       },
    #       "port_uuids": [
    #         "03734100040c0000399e081dc0ca985b"
    #       ]
    #     }
    #   ],
    #   "ungrouped_vlans": "5, 10-20, 30, 40, 50-60",
    #   "name": "LinkAggregationGroup	#01"
    # }

    result = {}

    try:
        lag_config_MAP = newlag #json.loads(newlag)
        #print("lag config: %s" % lag_config_MAP)
        targetURL = "https://" + targetADDR + "/api/lags/" + uuid
        
        result = setRESTdata(targetURL,lag_config_MAP,token)
    except:
        print("Error in putLAG : %s" % result)
        
    return result


    
###########
# putLAG END
###########




###########
# unlockXCVR
##########

def unlockXCVR():

    sys.stdout.write("Unlocking Transceivers...")

    raw_resultX = ""
    raw_resultY = ""

    commandX = "sudo /opt/plexxi/bin/pcp /opt/plexxi/PlexxiSupport/installTools/simplexxi/allowed_xcvrs.xml ring:/boot/allowed_xcvrs.xml"
    commandY = "sudo /opt/plexxi/bin/plexxiRemoteCommand ring \'/usr/bin/px-xcvr refresh;\'"
    
    logging.debug("CommandX \"%s\"" % commandX)
    try: 
        raw_resultX = subprocess.check_output(commandX,stderr=subprocess.STDOUT, shell=True)
        logging.debug("*****")
        logging.debug(raw_resultX)
        logging.debug("*****")
    except subprocess.CalledProcessError:
        print("Error encountered attempting to copy xcvr override: \"%s\"" % subprocess.STDOUT)
        return "" # reflects failure to apply, not sure if we want to bail out here.            

    logging.debug("CommandY \"%s\"" % commandY)
    try: 
        raw_resultY = subprocess.check_output(commandY,stderr=subprocess.STDOUT, shell=True)
        logging.debug("*****")
        logging.debug(raw_resultY)
        logging.debug("*****")
    except subprocess.CalledProcessError:
        print("Error encountered attempting to allow xcvr override: \"%s\"" % subprocess.STDOUT)
        return "" # reflects failure to apply, not sure if we want to bail out here.            

    sys.stdout.write("done.\n")
        
###########
# unlockXCVR End
###########

###########
# loopDetectON
##########

def loopDetectON():

    sys.stdout.write("Enabling Bridge Loop Detection Protections...")
    
    raw_resultY = ""

    commandY = "sudo /opt/plexxi/bin/cliLoopDetection enable ring"
    
    logging.debug("CommandY \"%s\"" % commandY)
    try: 
        raw_resultY = subprocess.check_output(commandY,stderr=subprocess.STDOUT, shell=True)
        logging.debug("*****")
        logging.debug(raw_resultY)
        logging.debug("*****")
    except subprocess.CalledProcessError:
        print("Error encountered attempting to enable bridge loop detection ring/fabric wide: \"%s\"" % subprocess.STDOUT)
        return "" # reflects failure to apply, not sure if we want to bail out here.            

    sys.stdout.write("done.\n")
        
###########
# loopDetectON End
###########



###########
# setPortEnable_CX
##########

def setPortEnable_CX(MAChost,portMap):


    newPorts = []
    newPort = {}
    #print("portMap %s" % portMap)

    try: 
        for port in portMap:
            newPort = {}        
            newPort["admin_state"] = "up"
            newPort["admin_role"] = port["admin_role"]
            newPort["forward_error_correction"] = port["forward_error_correction"]
            newPort["id"] = port["id"]
            newPort["lacp_id"] = port["lacp_id"]
            newPort["qsfp_mode"] = port["qsfp_mode"]
            newPort["scout_frames"] = port["scout_frames"]
            newPort["speed_in_mbps"] = port["speed_in_mbps"]
            newPort["user_description"] = port["user_description"]
            newPort["user_label"] = port["user_label"]
            newPorts.append(newPort)
    except:
        pnl("Error forming %s" % newPort)

    if (len(newPorts) > 0):                    
        portMAPnew = {"id" : MAChost, "ports" : newPorts} 
        targetURL = "https://localhost:443/cxcontrol/rest/api/1/controllers/1/switches/" + MAChost + "/ports"    
        portEnableRES = setRESTdata(targetURL,portMAPnew)
        return portEnableRES
    else:
        return ""
    

        
    
###########
# setPortEnable_CX End
###########

###########
# setPortSpeed_CX
##########

def setPortSpeed_CX(MAChost,portMap,speed):

    newPorts = []
    newPort = {}
    #print("portMap %s" % portMap)
    try: 
        for port in portMap:
            newPort = {}            
            newPort["admin_state"] = port["admin_state"]
            newPort["admin_role"] = port["admin_role"]
            newPort["forward_error_correction"] = port["forward_error_correction"]
            newPort["id"] = port["id"]
            newPort["lacp_id"] = port["lacp_id"]
            newPort["qsfp_mode"] = port["qsfp_mode"]
            newPort["scout_frames"] = port["scout_frames"]
            if (speed in port["permitted_speeds_in_mbps"]):
                newPort["speed_in_mbps"] = speed
                #pnl("set speed %s" % speed)
            else:
                pnl("This port is limited to %s, attempting setting to %s rejected." % (port["permitted_speeds_in_mbps"], speed))
                return -1 # abort because we must protect against incorrect speed settings as CX isn't.
            newPort["user_description"] = port["user_description"]
            newPort["user_label"] = port["user_label"]
            newPorts.append(newPort)
    except:
        pnl("Error forming %s" % newPort)

    if (len(newPorts) > 0):                    
        portMAPnew = {"id" : MAChost, "ports" : newPorts} 
        targetURL = "https://localhost:443/cxcontrol/rest/api/1/controllers/1/switches/" + MAChost + "/ports"    
        portSpeedRES = setRESTdata(targetURL,portMAPnew)
        return portSpeedRES
    else:
        return ""

        
    
###########
# setPortSpeed_CX End
###########


###########
# bulkClearVlanAll_CX
###########

def bulkClearVlanAll_CX(MAChost):

    """
    CX API necessitates gathering all vlan groups and "delete"-ing them.
    This is because switches have a vlan group for every port, which is invisible to Control and Connect.
    """

    """
    remove later, example of curl to successfully set a few VLANS:
    curl -X PUT "https://127.0.0.1/cxcontrol/rest/api/1/controllers/1/switches/E039D7802380/vlans?request_id=nadad" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"vlans\": [ { \"interfaces\": [ { \"id\": \"26\", \"interface_type\": \"port\" } ], \"s_vlan\": 0, \"vlan_group_id\": \"string\", \"vlan_tags\": [ 400,500 ] } ]}" --insecure
    """
    
    targetURL = "https://localhost:443/cxcontrol/rest/api/1/controllers/1/switches/" + MAChost + "/vlans"

    rawVLANSandGROUPS = getRESTdata(targetURL)
        
    rawVLANs = rawVLANSandGROUPS['vlans']
    #doomed_vlan_groups = []
    for vlan in rawVLANs:
        logging.debug("VLAN --- %s --- vgrpid: \"%s\"" % (vlan,vlan['vlan_group_id']))
        #doomed_vlan_groups.append(vlan['vlan_group_id'])
        target4deathURL = "https://localhost:443/cxcontrol/rest/api/1/controllers/1/switches/" + MAChost + "/vlans/" + vlan['vlan_group_id']
        discard_response = deleteRESTdata(target4deathURL)

        
    return rawVLANs


    

###########
# bulkClearVlanAll_CX End
###########


###########
# bulkSetVlanNative_CX
###########

def bulkSetVlanNative_CX(MAChost,portMAP,vlanNATIVE):

    portList = []
    for port in portMAP:
        portList.append(port['id'])

    vlanNATIVEstr = vlanNATIVE.split(" ")
    vlanNUM = int(vlanNATIVEstr[1])
    
    targetURL = "https://localhost:443/cxcontrol/rest/api/1/controllers/1/switches/" + MAChost + "/native_vlan"    
    nativeVLANjson = writeNATIVEVLAN_CX(portList,vlanNUM)

    newNATIVEVLANdataRES = setRESTdata(targetURL,nativeVLANjson)


    return newNATIVEVLANdataRES
    

###########
# bulkSetVlanNative_CX End
###########


###########
# bulkSetVlan_CX 
###########

"""
We must build a vlan group (as this is the way the switch sees things).
We will name it px-setup so we can remove it later if we like, cleanly (or preserve it).
"""

def bulkSetVlan_CX(MAChost,portMAP,vlanIDs):

    portList = []
    for port in portMAP:
        portList.append(port['id'])
    
    targetURL = "https://localhost:443/cxcontrol/rest/api/1/controllers/1/switches/" + MAChost + "/vlans"    
    VLANSjson = writeVLANS_CX(portMAP, vlanIDs)

    newVLANSdataRES = setRESTdata(targetURL,VLANSjson)


    return newVLANSdataRES
    

###########
# bulkSetVlan_CX End
###########


## Does not Assume user enabled.

def setPortSpeedEnableDot(MAChost,port,speed):

    sys.stdout.write(".")
    sys.stdout.flush() # Otherwise its a whole bunch of dots at once, no fun.
    return setPortSpeedEnable(MAChost,port,speed)
 
    
###########
# setPortSpeedEnableDot End
###########


###########
# setPortSpeedDot (and prints dot)
###########

def setPortSpeedDot(MAChost,port,speed):

    sys.stdout.write(".")
    sys.stdout.flush() # Otherwise its a whole bunch of dots at once, no fun.
    return setPortSpeed(MAChost,port,speed)
    
###########
# setPortSpeedDot End
###########



        
