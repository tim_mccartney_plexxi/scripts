#!/usr/bin/python 

##########################################################################################
# Copyright 2019 HPE
# Use and duplication of this software is subject to a separate license                  #
# agreement between the user and Plexxi or its licensor.                                 #
##########################################################################################



import pickle

#############
# cache 
#############

def data2file(data2write,filename="migrate_cache.tmp"):
    try:
        cacheDataFileName = filename
        cacheDataFile = open(cacheDataFileName, 'wb')
        pickle.dump(data2write,cacheDataFile)
        cacheDataFile.close()
    except:
        print("Error writing cache data to data file.")
        return False

    return True

def file2data(filename="migrate_cache.tmp"):
    try:
        cacheDataFileName = filename
        cacheDataFile = open(cacheDataFileName, 'rb')
        localCACHEinfo = pickle.load(cacheDataFile)
        cacheDataFile.close()
    except:
        print("Error reading cache data file.")
        return ""

    return localCACHEinfo

#############
# cache END
#############
