#!/usr/bin/python 

##########################################################################################
# Copyright 2019 HPE Inc. and its licensors.  All rights reserved.                    #
# Use and duplication of this software is subject to a separate license                  #
# agreement between the user and HPE or its licensor.                                 #
##########################################################################################


import os,sys,socket,re,datetime,subprocess,readline,argparse,shutil,time,pickle,requests,json,restUTIL,debug_util
import logging
logging.basicConfig(filename="/var/log/hpeCRportsup.log",level=logging.DEBUG)
logging.basicConfig(level=logging.WARNING, format="%(asctime)s - %(levelname)s - %(message)s")
logging.debug("DEBUG Logging activated.")

#############
# scpSTUFF2switch 
#############


def scpSTUFF2switch(TARGETstring,filesToTransport):

    print("SCP'ing %s to target %s." % (filesToTransport,TARGETstring))

    userName = "admin"
    
    if (TARGETstring != ""):
        commandX = "sudo /usr/bin/scp " + " " + filesToTransport + " " + userName + "@" + TARGETstring + ":/tmp"
    else:
        return ""      #This leaves little choice but to fail, due to lack of pyschic feature. 
    
    logging.debug("CommandX \"%s\"" % commandX)
    try: 
        raw_resultX = subprocess.check_output(commandX,stderr=subprocess.STDOUT, shell=True)
        logging.debug("*****")
        logging.debug(raw_resultX)
        logging.debug("*****")
    except subprocess.CalledProcessError:
        print("Error encountered attempting to transfer files: command \"%s\" --error-- \"%s\"" % (commandX,subprocess.STDOUT))
        return "" # reflects failure to apply, not sure if we want to bail out here.            

    
    return "success"

#############
# scpSTUFF4switch END
#############

def scpSTUFF4switch(TARGETstring,filesToTransport):

    print("SCP'ing %s from target %s." % (filesToTransport,TARGETstring))

    userName = "admin"

    if (TARGETstring != ""): 
        commandX = "sudo /usr/bin/scp " + " " + userName + "@" + TARGETstring + ":/tmp/" + filesToTransport + " /tmp/"
    else:
        return ""      #This leaves little choice but to fail, due to lack of pyschic feature. 
    logging.debug("CommandX \"%s\"" % commandX)
    try: 
        raw_resultX = subprocess.check_output(commandX,stderr=subprocess.STDOUT, shell=True)
        logging.debug("*****")
        logging.debug(raw_resultX)
        logging.debug("*****")
    except subprocess.CalledProcessError:
        print("Error encountered attempting to transfer files: command \"%s\" --error-- \"%s\"" % (commandX,subprocess.STDOUT))
        return "" # reflects failure to apply, not sure if we want to bail out here.            

    
    return "success"

#############
# scpSTUFF4switch END
#############


#############
# execSTUFF2switch 
#############

def execSTUFF2switch(TARGETstring,commandY):

    print("Remotely executing collection script.")

    userName = "admin"

    if (commandY == ""):
        print("exec what!? error")
        return ""
    
    # We should be passing in TARGETstring always in this case, BUT, sometimes it looks like we don't so doublecheck we have a sensible value
    
    if (TARGETstring != ""): # if we have no TARGETstring it may have been unconfigured, so don't use that!        
        hoststring = userName + "@" + TARGETstring
    else:
        if (TARGETstringFOUND != ""):
            hoststring = userName + "@" + TARGETstringFOUND
        else:
            return ""      #This leaves little choice but to fail, due to lack of pyschic feature. 

    commandY2 = "/usr/bin/ssh -t " + hoststring + " \"sudo " + commandY + "\""
    
    logging.debug("CommandY2 \"%s\"" % commandY2)
    
    #ssh = subprocess.Popen([commandY2],shell=False)

    #    ssh = subprocess.check_output(commandY2,stderr=subprocess.STDOUT,shell=True)
    #ssh = subprocess.call(["/usr/bin/ssh -t ", hoststring, "sudo ", commandY])

    #ssh = subprocess.check_output([commandY2],stderr=subprocess.STDOUT,shell=True)
    ssh = subprocess.check_call([commandY2],stderr=subprocess.STDOUT,shell=True)
    
    logging.debug("Result is %s" % ssh)
    
    result = ssh
    if (result == []):
        error = ssh.stderr.readlines()
        print("ERROR detected: %s" % error)
        return ""

    # if we get here result is good, return that.
    return result

#############
# execSTUFF2switch END
#############

#parse the command line args

# while defaults are provided in argparse, if there is no arg, no default seems to be applied so define here.


parser = argparse.ArgumentParser(description='HPE Composable Rack Ports UP utility')
parser.add_argument('-c','--custom_file', action="store", dest="custom_file", default="", help='Use alternate file for initial fabric configuration.')
parser.add_argument('-u','--username', action="store", dest="cfm_user", default="admin", help='Username for authentication at CFM.')
parser.add_argument('-p','--password', action="store", dest="cfm_pass", default="plexxi", help='Password for authentication at CFM.')
parser.add_argument('-t','--target', dest = 'theTargets', help='A CFM (Composable Fabric Manager) IP address to send these commands.')
parser.add_argument('-v','--verbose', action="store_true", dest="verbose", default=False, help='Verbose mode outputs more to screen. Default is simple view.')
parser.add_argument('-r','--readonly', action="store_true", dest="readonly", default=False, help='Read only, i.e. do no writing')
parser.add_argument('-o','--override', dest='theOverrides', default="TBD", help='Too early in the morning for specifics.')
args = parser.parse_args()

custom_file = args.custom_file
cfm_user = args.cfm_user
cfm_pass = args.cfm_pass
theTargets = args.theTargets
verbose = args.verbose
readonly = args.readonly
# Override options
theOverrides = args.theOverrides


# Override cache means do NOT use cached entries found in backup directories.

if (theOverrides.find("restdump") >= 0):
    print("Dumping rest messages.")
    debug_util.rest_debug()
    

print("Reading %s for configuration" % custom_file)
print("Configuration Target is %s" % theTargets)


# Get a token, get the fabric, parse for switches, use switches uuid to get all ports
# This will be used to form a structure to pin intended configuration to.

TOKEN = restUTIL.getTOKEN(theTargets,cfm_user,cfm_pass)
if (verbose): print("TOKEN is %s" % TOKEN)

FABRIC = restUTIL.getFABRIC(theTargets,TOKEN)
if (verbose): print("FABRIC is %s" % FABRIC)

FABRICmap = FABRIC[0]
SWITCHES = FABRICmap["switches"]
LoS = []
LoP = []

for switch in SWITCHES:
    if (verbose):
        print("RAW Switch (from FABRIC): %s : " % switch)
        print("Switch Name %s : mac: %s uuid: %s ip: %s sw_version: %s " % (switch["name"],switch["mac_address"],switch["uuid"],switch["ip_address"],switch["sw_version"]))

    #ports = restUTIL.getFABRICPORTS_UPLINK(theTargets,TOKEN,switch["uuid"])
    #ports = restUTIL.getFABRICPORTS(theTargets,TOKEN,switch["uuid"])
    #ports = restUTIL.getACCESSPORTS_UPLINK(theTargets,TOKEN,switch["uuid"])
    ports = restUTIL.getACCESSPORTS(theTargets,TOKEN,switch["uuid"])
    
    if (verbose):
        print("\tRAW Ports: %s" % ports)

    for port in ports:
        if (verbose):
            print("Port: %s uuid: %s Admin State: %s Link State: %s Speed %s Native VLAN %s ALL VLAN %s UNGROUPED VLAN %s" % (port["port_label"],port["uuid"],port["admin_state"],port["link_state"],port["speed"]["current"],port["native_vlan"],port["vlans"],port["ungrouped_vlans"]))
        LoP.append(port)
    
    
# statically define a structure to house the configuration, this will be gotten from a file in the future.
config_temp_server_port = {}
config_temp_server_port["native_vlan"] = "1"
config_temp_server_port["vlans"] = [100,101,102]
config_temp_server_port["lag"] = ""  # would be a name

config_temp_uplink_port = {}
config_temp_uplink_port["native_vlan"] = "1"
config_temp_uplink_port["vlans"] = [200,201,202]
config_temp_uplink_port["lag"] = ""  # would be a name

LoP.sort()
LoP_sorted = sorted(LoP, key=lambda k: k['uuid'])
LoUUIDs = []
count = 0
for tmp_port in LoP_sorted:
    # What the heck, JIRA the fact that I must remove these elements from what was "gotten" just now from same API, should eat on and give 200, NOT give 400.
    #del tmp_port["lag"]
    #del tmp_port["vlan_groups"]

    ##
    ## This would set every port in list LoP_sorted (should be all ports) individually
    ##
    #tmp_port["admin_state"] = "enabled"
    #tmp_port["native_vlan"] = 55
    #restUTIL.setPORT(theTargets,TOKEN,tmp_port)

    # This is building a list to do a bulk set via patch after this loop is complete.
    # Because changing speed will not be allowed for qspf(s) we only want to do this on SFP ports.
    if (tmp_port["form_factor"] == "sfp"):
        LoUUIDs.append(tmp_port["uuid"])


    if (not readonly):
        # Currently assuming we set all ports same as a starting point; therefore assume server_port by default.
        restUTIL.patchPORT(theTargets,TOKEN,LoUUIDs,"enabled",25000,"","",1,"55,66,77-88,99") # there are more optional args that we don't need to specify          

print("exit")
sys.exit(0)
