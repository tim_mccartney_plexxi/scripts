import os
import time

#List of variables
stringToMatch = 'no  yes'
hostnamelabel = 'Output of'
timestamp = time.strftime("%Y%m%d%H%M%S.txt")

#Files being used
inputfile= '/home/admin/input.txt'
outputfile = '/home/admin/output_{}'.format(timestamp)


def grep_for_string():
    """This function takes the input file and matches for Output of and string appending to output timestamp.txt file"""

    if os.path.isfile(inputfile):
        print ("Input file found")
    else:
        print("Error: {} file not found".format(inputfile))

    try:
        with open ('/home/admin/input.txt', 'r') as file:
            for line in file:
                if stringToMatch in line or hostnamelabel in line:
                    print (line)
                    with open (outputfile, 'a+') as f:
                        f.write(line)
    except IOError:
        print "Could not find file\n", inputfile
    return outputfile

def clean_up_input(inputfile):
    """This function removes the input file from this run, ready for next run"""

    if os.path.isfile(inputfile):
        os.remove(inputfile)
    else:
        print("Error: {} file not found".format(inputfile))

def send_command(inputfile):
    """This function will send the command to the fabric and write to input.txt which is later cleaned up"""
    os.system("/opt/plexxi/bin/psh ring 'snoopsh l3 egress show | grep 8191' > {}".format(inputfile))
    

if __name__ == "__main__":
    print ("Issuing snoopsh command for interface 8191\n")
    send_command(inputfile)
    print ("Processing file for string match")
    outfilename = grep_for_string()
    print ("The output file is" + outfilename)
    print ("Now deleting input.txt file for new execution")
    clean_up_input(inputfile)







