#!/usr/bin/env python
import datetime
import os
import subprocess
import argparse

import log_conf
#import read_crash


from typing import Tuple



time = datetime.datetime.now()
directory = "CombineSwitchesLogs_" + time.strftime("%d-%m-%y_%H:%M:%S")
parent_dir = "/tmp"
tempPath = os.path.join(parent_dir, directory)

# Tuples which the chosen elements are added to before being sorting chronologically
# Each element contains 2 parts - DateTime, and the line
all_tuple = []
c2_cx_tuple = []
c2_messages_plexxi_l3_tuple = []


def make_tmp_file():
    """Makes the temporary file located in /tmp"""
    try:
        os.mkdir(tempPath)
    except OSError as error:
        print(error)
    print ("\n\n\nDirectory '%s' created" % directory)


def all_five(c2, cx, plexxi, plexxi_l3, messages, outfile=None):
    """Ordered c2,cx,plexxi,plexxi_L3,messages.
        'outfile' is the name of the file which is to be created and written to"""

    counter = 0
    # Creates /tmp file
    make_tmp_file()
    write_file = open('/tmp/%s/ALL-TIMELINE.txt' % directory, 'w+')

    # Read c2 file
    with open(c2) as myFile:
        data = myFile.readlines()
        for index, line in enumerate(data, 1):
            if line[0].isdigit():
                # Format date/time
                c2_date_info = line.split()[0:1]
                c2_date_string = ' '.join(c2_date_info)
                c2_time_info = line.split()[1:2]
                c2_time_string = ' '.join(c2_time_info)
                c2_string_dt = c2_date_string + ' ' + c2_time_string
                line = 'C2:' + ' ' + line

                # c2_tuple contains both c2 dateTime, and the log line
                c2_tuple = (c2_string_dt, line)
                # Added to all_tuple
                all_tuple.append(c2_tuple)

     # Read cx file
    with open(cx) as myFile:
        data = myFile.readlines()
        for index, line in enumerate(data, 1):
            if line[0].isdigit():
                # Format date/time
                cx_date_info = line.split()[0:1]
                cx_date_string = ' '.join(cx_date_info)
                cx_time_info = line.split()[1:2]
                cx_time_string = ' '.join(cx_time_info)
                cx_string_dt = cx_date_string + ' ' + cx_time_string
                line = 'CX:' + ' ' + line

                # cx_tuple contains both cx dateTime, and the log line
                cx_tuple = (cx_string_dt, line)  # type: Tuple[str, str]
                # Added to all_tuple
                all_tuple.append(cx_tuple)

    # Read plexxi file
    with open(plexxi) as myFile:
        data = myFile.readlines()
        for index, line in enumerate(data, 1):
            # Format date/time
            year = '2020'
            plexxi_date_info = line.split()[:2]
            plexxi_date_info.insert(0, year)
            str_date_string = ' '.join(plexxi_date_info)
            plexxi_time_info = line.split()[2:3]
            plexxi_time_string = ' '.join(plexxi_time_info)
            plexxi_time_string = plexxi_time_string.replace(' ', '')  # type: str

            #Format date/time
            if len(plexxi_time_string) == 8:
                date_time = str_date_string + ' ' + plexxi_time_string + '.000'
                plexxi_date_time = datetime.datetime.strptime(date_time, "%Y %b %d %H:%M:%S.%f")
                plexxi_string_dt = str(plexxi_date_time) + '.000'
                line = 'PLEXXI:' + ' ' + line

                # plexxi_tuple contains both plexxi dateTime, and the log line
                plexxi_tuple = (plexxi_string_dt, line)
                # Added to all_tuple
                all_tuple.append(plexxi_tuple)

            #Format date/time
            elif len(plexxi_time_string) == 12:
                date_time = str_date_string + ' ' + plexxi_time_string
                if plexxi_time_string.endswith('000'):
                    plexxi_date_time = datetime.datetime.strptime(date_time, "%Y %b %d %H:%M:%S.%f")
                    plexxi_string_dt = str(plexxi_date_time) + '.000'
                    line = 'PLEXXI:' + ' ' + line

                    # plexxi_tuple contains both plexxi dateTime, and the log line
                    plexxi_tuple = (plexxi_string_dt, line)
                    # Added to all_tuple
                    all_tuple.append(plexxi_tuple)

                else:
                    plexxi_date_time = datetime.datetime.strptime(date_time, "%Y %b %d %H:%M:%S.%f")
                    plexxi_string_dt = str(plexxi_date_time)
                    line = 'PLEXXI:' + ' ' + line

                    # plexxi_tuple contains both plexxi dateTime, and the log line
                    plexxi_tuple = (plexxi_string_dt, line)
                    # Added to all_tuple
                    all_tuple.append(plexxi_tuple)

    # # # Read plexxi_L3 file
    with open(plexxi_l3) as myFile:
        data = myFile.readlines()
        for index, line in enumerate(data, 1):
            # Format date/time
            year = '2020'
            plexxi_l3_date_info = line.split()[:2]
            plexxi_l3_date_info.insert(0, year)
            str_date_string = ' '.join(plexxi_l3_date_info)
            plexxi_l3_time_info = line.split()[2:3]
            plexxi_l3_time_string = ' '.join(plexxi_l3_time_info)
            plexxi_l3_time_string = plexxi_l3_time_string.replace(' ', '')
            date_time = str_date_string + ' ' + plexxi_l3_time_string + '.000'
            plexxi_l3_date_time = datetime.datetime.strptime(date_time, "%Y %b %d %H:%M:%S.%f")
            plexxi_l3_string_dt = str(plexxi_l3_date_time) + '.000'
            line = 'PLEXXI_L3:' + ' ' + line

            # plexxi_L3_ tuple contains both plexxi_L3 dateTime, and the log line
            plexxi_l3_tuple = (plexxi_l3_string_dt, line)
            # Added to all_tuple
            all_tuple.append(plexxi_l3_tuple)

    with open(messages) as myFile:
        data = myFile.readlines()
        for index, line in enumerate(data, 1):
            # Format date/time
            year = '2020'
            messages_date_info = line.split()[:2]
            messages_date_info.insert(0, year)
            str_date_string = ' '.join(messages_date_info)
            messages_time_info = line.split()[2:3]
            messages_time_string = ' '.join(messages_time_info)
            messages_time_string = messages_time_string.replace(' ', '')
            date_time = str_date_string + ' ' + messages_time_string + '.000'
            messages_date_time = datetime.datetime.strptime(date_time, "%Y %b %d %H:%M:%S.%f")
            messages_string_dt = str(messages_date_time) + '.000'
            line = 'MESSAGES:' +  ' ' + line

            # messages_ tuple contains both messages dateTime, and the log line
            messages_tuple = (messages_string_dt, line)
            # Added to all_tuple
            all_tuple.append(messages_tuple)

    # List which sorts all_tuple in chronological order by the date format shown below
    sorted_list = sorted(all_tuple, key=lambda x: datetime.datetime.strptime(x[0], '%Y-%m-%d %H:%M:%S.%f'))

    # Prints sorted_lines
    for date, lines in sorted_list:
        print str(counter) + ': ' + lines
        write_file.write(str(counter) + ': ' + lines + '\n')
        counter = counter + 1

    counter = 0

    # if outfile argument is given - Write sorted_list to file
    if outfile:
        yes = open(outfile, 'w+')
        for date, lines in sorted_list:
            yes.write(str(counter) + ': ' + lines + '\n')
            counter = counter + 1

    # all_tuple is deleted
    del all_tuple[:]




def command_line_list():

    print " CombinePLexxi Switch, run with option followed by a filename, use -h for more"
    parser = argparse.ArgumentParser()

    # python CombinePlexxiSwitchLogs.py config file FileNameHere
    parser.add_argument('-cfg', help="List of timelines", dest='cfg', action='store')

    args = parser.parse_args()

    # switch_word only exists when on a switch
    switch_word = 'c2platd'

    if args.cfg:
        print "Please ensure the log_conf.py file is correct"
        print "C2 messages is: " + log_conf.path4c2msg
        print "CX messages is: " + log_conf.path4cxmsg
        print "plexxi is: " + log_conf.path4plexxi
        print "Plexxi l3 is: " + log_conf.path4l3
        print "Plexxi messages is: " + log_conf.path4tmpmsgs

       # print '\n' + 'SWITCH NOT FOUND - ENTER MANUALLY'

        all_five(log_conf.path4c2msg, log_conf.path4cxmsg, log_conf.path4plexxi, log_conf.path4l3, log_conf.path4tmpmsgs, args.cfg)

        print 'File Name: ', args.cfg
        logfile = args.cfg
        return logfile


def read_c2crash(logfile):
    try:

        if os.stat(log_conf.c2crash_file_path).st_size == 0:
            print ("\n **********Crash file is empty********")
        else:
            print "\nThis is the contents of c2 crash.log:\n"
            with open(log_conf.c2crash_file_path, 'r') as file:
                with open(logfile, 'a') as file1:
                    for line in file:
                        print line.rstrip('\n')
                        file1.write(line)

    except OSError:
        print "C2 crash.log File does not exist"


def read_cxcrash(logfile):
    try:


        if os.stat(log_conf.cxcrash_file_path).st_size == 0:
            print ("\n **********Crash file is empty********")
        else:
            print "\nThis is the contents of cx crash.log:\n"
            with open(log_conf.cxcrash_file_path, 'r') as file:
                with open(logfile,'a') as file1:
                    for line in file:
                        print line.rstrip('\n')
                        file1.write(line)

    except OSError:
        print "CX crash.log does not exist"

logfile = command_line_list()
read_c2crash(logfile)
read_cxcrash(logfile)


