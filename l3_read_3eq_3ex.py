"""This script was created for MMH, as a way to run the psh ring commands
    whwich will flag the interface id when sent to CPU for 3eq this is 8191
    for 3ex this is 16383

    The script  creates two input files (the output fo snoopsh)
    It then searches these input files for stringToMacth and the Output of
    which will capture the command sent and switch name

    The results are written to two files with timestamp appended

"""




import os
import time

#List of variables
stringToMatch = 'no  yes'
hostnamelabel = 'Output of'
timestamp = time.strftime("%Y%m%d%H%M%S.txt")

#Files being used
inputfile3eq= '/Users/tmccartney/Downloads/input3eq.txt'
inputfile3ex= '/Users/tmccartney/Downloads/input3ex.txt'
outputfile3eq = '/Users/tmccartney/Downloads/output3eq_{}'.format(timestamp)
outputfile3ex = '/Users/tmccartney/Downloads/output3ex_{}'.format(timestamp)

def grep_for_string(inputfile,outputfile):
    """This function takes the input file and matches for Output of and string appending to output timestamp.txt file"""

    if os.path.isfile(inputfile):
        print ("Input file found")
    else:
        print("Error: {} file not found".format(inputfile))

    try:
        with open (inputfile, 'r') as file:
            for line in file:
                if stringToMatch in line or hostnamelabel in line:
                    print (line)
                    with open (outputfile, 'a+') as f:
                        f.write(line)
    except IOError:
        print "Could not find file\n", inputfile
    return outputfile

def clean_up_input(inputfile):
    """This function removes the input file from this run, ready for next run"""

    if os.path.isfile(inputfile):
        os.remove(inputfile)
    else:
        print("Error: {} file not found".format(inputfile))

def send_command(interfaceid,inputfile):
   
    """This function will send the command to the fabric and write to a file
        which is later cleaned up
    """
    
    os.system("/opt/plexxi/bin/psh ring 'snoopsh l3 egress show | grep {}' > {}".format(interfaceid, inputfile))
   

if __name__ == "__main__":
    #print ("Issuing snoopsh command for interface 8191\n")
    #send_command('8191',inputfile3eq)
    #send_command('16383',inputfile3ex)
    print ("Processing file for string match")
    grep_for_string(inputfile3eq,outputfile3eq)
    grep_for_string(inputfile3ex,outputfile3ex)
    print ("Now deleting input files for new execution")
    #clean_up_input(inputfile3eq)
    #clean_up_input(inputfile3ex)
    







