#!/usr/bin/python2.7
##########################################################################
#
# Copyright (c) 2016, Plexxi Inc. and its licensors.
#
# All rights reserved.
#
# Use and duplication of this software is subject to a separate license
# agreement between the user and Plexxi or its licensor.
#
##########################################################################

#sys.path.insert(0, '/root')
#import pythonrc

# Assume default credentials due to intial install assumption of this operation.

controlSecureUrl = 'https://localhost:8443/PlexxiCore/api'
controlUrl = 'http://localhost:8080/PlexxiCore/api'
controlUser = 'admin'
controlPass = 'plexxi'

# Import required stuff and create a session to localhost


# To get all plexxi goodness required
from plexxi.core.api.binding import *
from plexxi.core.api.session import *
from plexxi.core.api.util import *

# To read data file from px-setup/simplexxi.py engine
import sys
from update_names_ctrl_simplexxi_lib import Update_Names_Oninit
from enable_ports_simplexxi_lib import Enable_Ports_Oninit
import pickle

try:
    exchange = CoreSession.connect(controlSecureUrl, True, controlUser, controlPass, verifyCertificate=False)
    print("INFO: Connected to PlexxiCore on localhost %s" % controlSecureUrl)

    # update the prompt
    sys.ps1 = "plexxi-ctrl-python-api>>> "
except:
    print("FAILURE: to connect to localhost %s" % controlSecureUrl)
    try:
        exchange = CoreSession.connect(controlUrl, True, controlUser, controlPass)        
        print("INFO: Connected to PlexxiCore on localhost %s" % controlUrl)
    
        # update the prompt
        sys.ps1 = "plexxi-ctrl-python-api>>> "

    except:
        print """
        ERROR: Failed to connect to local Plexxi Control core session!
        Please check that your Plexxi Control instance (ie. tomcat server) is running.
        Ensure you provided appropriate credentials if not default.
        """
    
      

###########
# Main 
###########

# load 'em up (harvest data from python data files which should be now on CTRL)
switchDataFile = open("/tmp/switch.dat", 'rb')
sData = pickle.load(switchDataFile)
print sData

portsupDataFile = open("/tmp/portsup.dat", 'rb')
pData = pickle.load(portsupDataFile)
print pData

# get 'em in (execute reconciliation functions)
updateEm = Update_Names_Oninit(sData)
updateEm.run()

bringEmUp = Enable_Ports_Oninit(pData)
bringEmUp.run()

# raw hide!

###########
# Main END
###########



