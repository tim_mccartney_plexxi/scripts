
#!/usr/bin/python

#Importing modules
import paramiko
import sys
import time



#setting parameters like host IP, username, passwd and number of iterations to gather cmds
#HOST = "192.168.104.10"
USER = "admin"  # type: str
PASS = "plexxi"  # type: str
 #HOST = "0"  # type: str

def get_client(HOST):
    client1 = paramiko.SSHClient()
    # Add missing client key
    client1.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    # connect to switch
    client1.connect(HOST, username=USER, password=PASS)
    print "SSH connection to %s established" % HOST
    return client1

#A function to get fabric information
def fabricInfo(client1):
  stdin, stdout, stderr = client1.exec_command('/opt/plexxi/bin/fabricInfo\n')
  fabric = stdout.read()
  print fabric
  print "Logged out of device %s" % HOST
  return fabric

#A function to pull out the master id from fabric variable
def master_id(fabric):
    master = fabric.index('Master')
    print (fabric[master: master + 39])

def host_ip_address():
    HOST = raw_input("Enter IP address of switch?: ")
    return HOST

def switch_version(client1):
    stdin, stdout, stderr = client1.exec_command('/opt/plexxi/bin/plexxiSwitchVersion\n')
    version = stdout.read()
    print ("Switch Version: " + version)

def up_ports(client1):
    stdin, stdout, stderr = client1.exec_command('/opt/plexxi/bin/showPort | grep up\n')
    ports = stdout.read()
    print ('The ports that are up: ' + ports)


if __name__ == '__main__':
    HOST = host_ip_address()
    client_session = get_client(HOST)
    fabric = fabricInfo(client_session)
    master_id(fabric)
    switch_version(client_session)
    up_ports(client_session)
    client_session.close()
    print "********* Logged out of device %s *************" % HOST
