#!/bin/bash
#Script to compress /var/log folder excluding*.csv

/bin/tar --exclude='*.csv' -zcvf /var/tmp/var_log_$HOSTNAME.tar.gz /var/log



VISUAL=vi crontab -e

5 * * * *
10 * * * *
15 * * * *
20 * * * *
25 * * * *
30 * * * *
35 * * * *
40 * * * *
45 * * * *
50 * * * *
0 * * * *
2 * * * *
55 * * * *
12 * * * *
27 * * * *
42 * * * *