import datetime
import tarfile
import os
import psycopg2
import subprocess
import argparse

time = datetime.datetime.now()
directory = "FailureOutput_" + time.strftime("%d-%m-%y_%H:%M:%S")
parent_dir = "/tmp"
extract_directory = "FailureOutput_" + time.strftime("%d-%m-%y_%H:%M:%S") + "/CFM-extract"
switches_directory = "FailureOutput_" + time.strftime("%d-%m-%y_%H:%M:%S") + "/Switches"
switches_ex = "FailureOutput_" + time.strftime("%d-%m-%y_%H:%M:%S") + "/Switches/Extract"
active_controller_path = "FailureOutput_" + time.strftime("%d-%m-%y_%H:%M:%S") + "/Switches/Extract/Active-Controller-Extracted"
all_switch_path = "FailureOutput_" + time.strftime("%d-%m-%y_%H:%M:%S") + "/Switches/Extract/All-Switches-Extracted"
tempPath = os.path.join(parent_dir, directory)
extract_folder = os.path.join(parent_dir, extract_directory)
switches_folder = os.path.join(parent_dir, switches_directory)
switches_extract = os.path.join(parent_dir, switches_ex)
active_controller_folder = os.path.join(parent_dir, active_controller_path)
all_switch_folders = os.path.join(parent_dir, all_switch_path)
active_controller_pathway = None

TEST_FAILS = []
CONNECT_TUPLE = []
DIFFERENCES_TUPLE = []
TEST_TUPLE = []
TEST_TIME = []
TEST_DATE = []
CLOSEST = []
log_paths = []

PSQL_PATH = '/Applications/Postgres.app/Contents/Versions/latest/bin/psql'
POSTGRES_PATH = '/Applications/Postgres.app/Contents/Versions/latest/bin/'
db_name = 'restore_' + str(time.time())
DROP_DB = POSTGRES_PATH+'dropdb'

e1 = '/Users/shanemccaughan/Downloads/cfm_release_5.1.1_5474_switch_release_5.1.1_106-cfm_release_5.1.1_5474_switch_release_5.1.1_106-portsecurity-20190815025915-4812.txt'
e2 = '/Users/shanemccaughan/Downloads/cfm_release_5.1.1_5474_switch_release_5.1.1_106-cfm_release_5.1.1_5474_switch_release_5.1.1_106-ipv4intervlanbasicnondefault-20190815032342-8328.txt'
e3 = '/Users/shanemccaughan/Downloads/cfm_release_5.1.0_5424_switch_release_5.1.0_92-cfm_release_5.1.0_5424_switch_release_5.1.0_92-mlag-20190731212524-2850.txt'
e4 = '/Users/shanemccaughan/Downloads/shane_test2.txt'
t1 = "/Users/shanemccaughan/Downloads/rastan-connect.lab.plexxi.com-log-bundle.tar.gz"
t2 = "/Users/shanemccaughan/Downloads/loudon-connect.lab.plexxi.com-log-bundle.tar.gz"
t3 = "/Users/shanemccaughan/Downloads/denali-connect-log-bundle.tar.gz"
t4 = "/Users/shanemccaughan/Downloads/linkUpSlow-composable-fabric-manager-support-bundle-2019-07-26-161829846865.tar.gz"
d1 = "/Users/shanemccaughan/Downloads/composable-fabric-support-bundle-collection-2019-08-28-112029696208.gz"
d2 = "/Users/shanemccaughan/Downloads/d93_switch166_12thAug-composable-fabric-support-bundle-collection-2019-08-12-075206500146.tar"

CRED = '\033[91m'
CEND = '\033[0m'
CGREEN2 = '\33[92m'
CGREENBG2 = '\33[42m'
CBEIGE = '\33[36m'
CBLUE = '\33[104m'
CWHITE = '\33[107m'
CRED2 = '\33[101m'


def make_temp_file():
    try:
        os.mkdir(tempPath)
        os.mkdir(extract_folder)
        os.mkdir(switches_folder)
        os.mkdir(switches_extract)
        os.mkdir(active_controller_folder)
        os.mkdir(all_switch_folders)

    except OSError as error:
        print(error)
    print ("\n\n\nDirectory '%s' created" % directory)


def active_controller(support_path):
    ac_present = None
    make_temp_file()
    active = None
    dc = None
    tar = tarfile.open(support_path)
    tar.extractall(path=switches_folder)
    tar.close()
    for root, dirs, files in os.walk(switches_folder):
        for afile in files:
            if 'composable-fabric' in afile:
                full_path = os.path.join(root, afile)
                log_paths.append(full_path)

                tar_extract = tarfile.open(full_path)
                tar_extract.extractall(path=switches_extract)
                tar_extract.close()

                for root_b, dirs_b, files_b in os.walk(switches_extract):
                    for bfile in files_b:
                        if 'pgdump-plexxi' in bfile:
                            path_join = os.path.join(root, switches_extract)
                            path_join_2 = os.path.join(root, path_join, bfile)
                            pg_dump_path = path_join_2
                            print 'pgdump: ', pg_dump_path

                            create_db_cmd = '{}/createdb'.format(POSTGRES_PATH)
                            subprocess.call([create_db_cmd, db_name])
                            print CRED + 'Database Created:', create_db_cmd + CEND

                            pg_restore_cmd = '{}pg_restore -d {} --no-owner --no-privileges {}'.format(POSTGRES_PATH, db_name, pg_dump_path)
                            subprocess.call(pg_restore_cmd.split())
                            print CRED + 'Database Restored:', pg_restore_cmd + CEND + '\n'

                            conn = psycopg2.connect("dbname=%s user=postgres" % db_name)
                            cur = conn.cursor()

                            cur.execute("select controller_uuid from fabric_segments;")
                            controller_uuid_fabric_segments = cur.fetchone()

                            print ('---------------------------------------------------------------------' + '\n' + CBLUE + 'ACTIVE CONTROLLER:' + CEND)
                            cur.execute("SELECT name, uuid, fabric_uuid, mac_address from switches where uuid = '%s';" % controller_uuid_fabric_segments)
                            name_and_mac_addresses = cur.fetchall()
                            active = name_and_mac_addresses[0][0]
                            print 'Name:', name_and_mac_addresses[0][0], '\nUUID:', name_and_mac_addresses[0][1], '\nFabric UUID:', name_and_mac_addresses[0][2], '\nMac Address', name_and_mac_addresses[0][3]

                            print '---------------------------------------------------------------------'

                            cur.execute("select name, uuid, fabric_uuid, mac_address from switches;")
                            dc = cur.fetchall()
                            cur.close()
                            conn.close()

    for root, dirs, files in os.walk(switches_folder):
        for afile in files:
            if 'sw' in afile and 'tar.gz' in afile and 'composable' not in afile:
                full_path = os.path.join(root, afile)
                log_paths.append(full_path)
                print 'Switches: ', afile

            if 'sw' in afile and 'tar.gz' in afile and '%s' % active in afile:
                full_path = os.path.join(root, afile)
                log_paths.append(full_path)
                tar_extract = tarfile.open(full_path)
                tar_extract.extractall(path=active_controller_folder)
                tar_extract.close()
                active_controller_pathway_ = full_path
                print "\n" + CGREENBG2 + "Active Controller Found: %s" % active + CEND
                print "File Path: %s" % active_controller_pathway_
                ac_present = True

    if ac_present is None:
        print CRED2 + 'There is no active controller in the files. The active controller is %s. LIMITED ANALYSIS.' % active + CEND

    print '\n' + CWHITE + 'All Switches Information:' + CEND
    for item in dc:
        print 'Name:', str(item[0]), '\n', 'UUID:', str(item[1]), '\n', 'Fabric UUID:', str(item[2]), '\n', 'Mac Address:', str(item[3]), '\n'
    print '---------------------------------------------------------------------'

    drop_db_cmd = DROP_DB + ' %s' % db_name
    subprocess.call(drop_db_cmd.split())
    print '\n' + CRED + 'Database Dropped:', drop_db_cmd + CEND

    for root, dirs, files in os.walk(switches_folder):
        for cfile in files:
            if 'sw' in cfile and 'tar.gz' in cfile:
                full_path = os.path.join(root, cfile)
                log_paths.append(full_path)
                tar_extract = tarfile.open(full_path)
                tar_extract.extractall(path=all_switch_folders)
                tar_extract.close()


# support_database(d1)


def error_log(file_path):
    make_temp_file()
    counter = 1
    with open(file_path) as myFile:
        f = open('/tmp/%s/TestLog.txt' % directory, 'w+')
        f.write('\n\n\n' + CBLUE + 'File: ' + str(file_path) + CEND)
        print('\n\n\n' + CBLUE + 'File: ' + str(file_path) + CEND)
        data = myFile.readlines()
        for index, line in enumerate(data, 1):
            ten_lines = index - 10
            if '::FAILED' in line:
                f.write('\nCounter: ' + str(counter) + '\n')
                print '\nCounter:', counter

                f.write(CRED + 'Failure Info: ' + line + CEND + '\n')
                print(CRED + 'Failure Info: ' + line + CEND)

                date_string = line.split()[1:2][0]
                time_string = line.split()[2:3][0][:8]
                test_datetime_time = datetime.time(int(time_string[:2]), int(time_string[3:5]), int(time_string[6:8]))

                both_datetime = datetime.datetime(int(date_string[0:4]), int(date_string[5:7]), int(date_string[8:10]), int(time_string[:2]), int(time_string[3:5]), int(time_string[6:8]))

                f.write(CBEIGE + 'Line Number: ' + CEND + str(index) + '\n')
                print (CBEIGE + 'Line Number: ' + CEND + str(index))

                both_connect_datetime = datetime.datetime(int(date_string[0:4]),
                                                          int(date_string[5:7]),
                                                          int(date_string[8:10]),
                                                          int(time_string[:2]),
                                                          int(time_string[3:5]),
                                                          int(time_string[6:8]))
                print 'Date Time: ', both_connect_datetime, '\n'


                TEST_TUPLE.append((both_datetime, line))
                TEST_TIME.append((test_datetime_time, line))

                f.write('\n' + CGREEN2 + "Previous 10 Lines Before '::FAILED':" + CEND + '\n')
                print(CGREEN2 + "Previous 10 Lines Before '::FAILED':" + CEND)

                counter = counter + 1

                for info in data[ten_lines:index]:
                    info = info.replace('\n', '')
                    f.write(info + '\n')
                    print(info + '\n')
                f.write('\n')
                print '\n----------------------------------------------------------------------------------------'
        f.write('\n-------------------------------------------------------------------------------------------------------\n\n')
        f.close()


# error_log(e1)


def tuple_has_data(any_structure):
    if any_structure:
        return True

    else:
        return False


def tar_files(tar_file_path):
    make_temp_file()
    counter = 1
    tar = tarfile.open(tar_file_path)
    tar.extractall(path=extract_folder)
    tar.close()
    f = open('/tmp/%s/ConnectApiLog.txt' % directory, 'w+')

    for root, dirs, files in os.walk(extract_folder):
        for afile in files:

            if 'connectapi' in afile:
                full_path = os.path.join(root, afile)
                log_paths.append(full_path)

                f.write('\n\n' + CBLUE + 'File: ' + afile + CEND)
                print('\n' + CBLUE + 'File: ' + afile + CEND)

                with open(full_path) as myFile:
                    data = myFile.readlines()

                    for index, line in enumerate(data, 1):
                        twenty_lines_before = index + 20

                        if 'Traceback' in line:

                            f.write('\nCounter: ' + str(counter) + '\n')
                            print '\n\nCounter:', counter
                            f.write(CRED + 'Traceback Error: ' + line + '\n' + CEND)
                            print CRED + 'Traceback Error: ', line + CEND

                            f.write(CBEIGE + 'Line Number: ' + CEND + str(index) + '\n')
                            print CBEIGE + 'Line Number: ' + CEND + str(index)

                            first_char = line.split()[0][:1]
                            if first_char.isdigit():
                                date_info = line.split()[:1]
                                date_string = ' '.join(date_info)

                                connect_datetime_date = datetime.datetime.strptime(date_string, "%Y-%m-%d")

                                time_info = line.split()[1:2]
                                time_string = ' '.join(time_info)[:8]

                                both_connect_datetime = datetime.datetime(int(date_string[0:4]), int(date_string[5:7]), int(date_string[8:10]), int(time_string[:2]), int(time_string[3:5]), int(time_string[6:8]))
                                print 'Date + Time: ', both_connect_datetime

                                CONNECT_TUPLE.append((both_connect_datetime, line))

                            else:
                                year = '2019'
                                str_date_info = line.split()[:2]
                                str_date_info.insert(0, year)

                                formatted_date = connect_datetime_date.strftime("%Y-%m-%d")

                                connect_datetime_date = datetime.datetime.strptime(formatted_date, "%Y-%m-%d")

                                str_time_info = line.split()[2:3]
                                str_time_string = ' '.join(str_time_info)

                                both_connect_datetime = datetime.datetime(int(formatted_date[0:4]), int(formatted_date[5:7]), int(formatted_date[8:10]), int(str_time_string[:2]), int(str_time_string[3:5]), int(str_time_string[6:8]))

                                print 'Date + Time:', both_connect_datetime
                                CONNECT_TUPLE.append((both_connect_datetime, line))

                            f.write(CGREEN2 + "20 Lines After 'Traceback': " + '\n' + CEND)
                            print CGREEN2 + "\n20 Lines After 'Traceback': " + CEND

                            counter = counter + 1

                            for info in data[index: twenty_lines_before]:
                                info = info.replace('\n', '')
                                f.write(info + '\n')
                                print info
                            f.write('\n')
                    print "No 'Traceback' found in %s" % afile

                f.write('----------------------------------------------------------------------------------------------------------\n\n')
                del CONNECT_TUPLE[:]


# tar_files(t4)


def compare(error_logfile_path, tar_file_path):

    def compare_error_log(error_log_file_path):
        with open(error_log_file_path) as myFile:
            data = myFile.readlines()
            for index, line in enumerate(data, 1):

                if '::FAILED' in line:
                    date_string = line.split()[1:2][0]
                    time_string = line.split()[2:3][0][:8]
                    test_datetime_time = datetime.time(int(time_string[:2]), int(time_string[3:5]), int(time_string[6:8]))

                    both_test_datetime = datetime.datetime(int(date_string[0:4]), int(date_string[5:7]), int(date_string[8:10]), int(time_string[:2]), int(time_string[3:5]), int(time_string[6:8]))
                    TEST_TUPLE.append((both_test_datetime, line))
                    TEST_TIME.append((test_datetime_time, line))

    def compare_tar_files(tarfile_path):
        tar = tarfile.open(tarfile_path)
        tar.extractall(path=extract_folder)
        tar.close()

        for root, dirs, files in os.walk(extract_folder):
            for afile in files:

                if 'connectapi' in afile:
                    full_path = os.path.join(root, afile)
                    log_paths.append(full_path)

                    with open(full_path) as myFile:
                        data = myFile.readlines()

                        for index, line in enumerate(data, 1):
                            if 'Traceback' in line:
                                first_char = line.split()[0][:1]
                                if first_char.isdigit():
                                    date_info = line.split()[:1]
                                    date_string = ' '.join(date_info)
                                    connect_datetime_date = datetime.datetime.strptime(
                                        date_string, "%Y-%m-%d")

                                    time_info = line.split()[1:2]
                                    time_string = ' '.join(time_info)[:8]

                                    both_connect_datetime = datetime.datetime(int(date_string[0:4]), int(date_string[5:7]), int(date_string[8:10]), int(time_string[:2]), int(time_string[3:5]), int(time_string[6:8]))

                                    CONNECT_TUPLE.append((both_connect_datetime, line))

                                else:
                                    year = '2019'
                                    str_date_info = line.split()[:2]
                                    str_date_info.insert(0, year)
                                    formatted_date = connect_datetime_date.strftime("%Y-%m-%d")
                                    connect_datetime_date = datetime.datetime.strptime(formatted_date, "%Y-%m-%d")

                                    str_time_info = line.split()[2:3]
                                    str_time_string = ' '.join(str_time_info)

                                    both_connect_datetime = datetime.datetime(int(formatted_date[0:4]), int(formatted_date[5:7]), int(formatted_date[8:10]), int(str_time_string[:2]), int(str_time_string[3:5]), int(str_time_string[6:8]))
                                    CONNECT_TUPLE.append((both_connect_datetime, line))

    def compare_closest_times():
        for testData in TEST_TUPLE:
            del DIFFERENCES_TUPLE[:]
            if tuple_has_data(CONNECT_TUPLE):
                for connectData in CONNECT_TUPLE:

                    difference = testData[0] - connectData[0]
                    difference = difference.total_seconds()

                    if difference < 0:
                        difference = difference * -1
                    DIFFERENCES_TUPLE.append((difference, testData, connectData))

                closest = min(DIFFERENCES_TUPLE)[0]
                time_smh = str(datetime.timedelta(seconds=closest))
                for i, x, y in DIFFERENCES_TUPLE:

                    if closest == i:
                        print CRED, "Closest 'connectapi' failure to each 'test' failure:", CEND
                        print CBLUE + 'Test' + CEND + CGREEN2 + '     Timestamp: ' + CEND + str(x[0]), CGREEN2 + '   Line: ' + CEND + str(x[1])
                        print CBLUE + 'Connect' + CEND + CGREEN2 + '  Timestamp: ' + CEND + str(y[0]) + CGREEN2 + '  Line: ' + CEND  + str(y[1])

                print 'Time difference = ', CBEIGE, time_smh, '(', closest, 'seconds)\n', CEND
                print '------------------------------------------------------------------------------------------------------------------'
            else:
                print "No data in CONNECT_TUPLE (Possibly no instances of 'Traceback' in logs)"
    del TEST_TUPLE[:]

    compare_error_log(error_logfile_path)
    compare_tar_files(tar_file_path)
    compare_closest_times()
    del CONNECT_TUPLE[:]


def command_line_list():
    parser = argparse.ArgumentParser()
    parser.add_argument('--list', '-l', help="List all function", dest='error_log_path', action='store')

    input_choice = input(
        "\nWhat action would you like to perform next: \n Type the Number: \n 1 - View 'FAILED::' in PSQA Regression Log \n 2 - View 'Tracebacks' in Connectapi Log \n 3 - View each PSQA regression log 'FAILED' to closest Connectapi 'Traceback' \n 4 - View Active Controller + Switch Information \n 5 - Exit \n")

    while (input_choice < 8) and (input_choice > 0):
        if input_choice == 1:
            input_error_log = input('Enter full path of PSQA Regression log ')
            error_log(input_error_log)

        elif input_choice == 2:
            input_tar_file_path = input('Enter full path of Connectapi Log ')
            tar_files(input_tar_file_path)

        elif input_choice == 3:
            input_error_log = input('Enter full path of PSQA Regression Log ')
            input_tar_file_path = input('Enter full path of Connectapi Log')
            compare(input_error_log, input_tar_file_path)

        elif input_choice == 4:
            input_support_path = input('Enter full path of support bundle: ')
            active_controller(input_support_path)

        elif input_choice == 5:
            exit(0)

        print '--------------------------------------------------------------------------------------------------------------------'
        input_choice = input(
             "\nWhat action would you like to perform next: \n Type the Number: \n 1 - View 'FAILED::' in PSQA Regression Log \n 2 - View 'Tracebacks' in Connectapi Log \n 3 - View each PSQA regression log 'FAILED' to closest Connectapi 'Traceback' \n 4 - View Active Controller + Switch Information \n 5 - Exit \n")







command_line_list()
