#!/usr/bin/env python

"""This script is to be used to read the crash file of c2"""
import log_conf
import os


def read_c2crash():
    try:


        if os.stat(log_conf.c2crash_file_path).st_size == 0:
            print ("\n **********Crash file is empty********")
        else:
            print "\nThis is the contents of c2 crash.log:\n"
            with open(log_conf.c2crash_file_path, 'r') as file:
                for line in file:
                    print line.rstrip('\n')

    except OSError:
        print "C2 crash.log File does not exist"

read_c2crash()